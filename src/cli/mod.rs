//! Module for the CLI tool
//!
//! Contains all the necessary components for the CLI tool, its help and the logic necessary to call
//! the interpreter with the correct contents.

use std::{
    fs::File,
    io::Read,
};

use clap::{app_from_crate, Arg, crate_authors, crate_description, crate_name, crate_version, SubCommand};
use nom::{InputLength, types::CompleteStr};

use crate::{
    domains::expressions::stmt::prelude::*,
    interpreter::{
        concretes::{
            denotational,
            operational,
        },
        traits::ConcreteInterpreter,
    },
    parser::*,
    utilities::logging::*,
};
use crate::domains::abstracts::constants::Constant;
use crate::domains::abstracts::intervals::Interval;
use crate::domains::abstracts::signs::Sign;
use crate::interpreter::abstracts;
use crate::interpreter::traits::AbstractInterpreter;

/// Method that manages the cli interface.
///
/// This method parses the application arguments; parses the source of a program;
/// converts it to an abstract representation, if necessary, and executes it.
pub fn cli() {
    init_log();

    let mut a = app_from_crate!()
        .about("Interpreter and static analyzer for While (Stm) language")
        .subcommand(SubCommand::with_name("interpret")
            .about("Interpret the source code")
            .arg(Arg::with_name("operational")
                .short("o")
                .help("Execute the program in an operational mode"))
            .arg(Arg::with_name("INPUT")
                .help("Source file to execute")
                .required(true)
                .takes_value(true)))
        .subcommand(SubCommand::with_name("abstract")
            .about("Use an abstract interpretation to run the source code")
            .arg(Arg::with_name("abstraction")
                .help("Choice of the abstract domain")
                .short("a")
                .required(true)
                .takes_value(true)
                .possible_values(&["signs", "constants", "intervals"]))
            .arg(Arg::with_name("INPUT")
                .help("Source file to execute")
                .required(true)
                .takes_value(true)));

    let m = a.clone().get_matches();

    match m.subcommand() {
        ("interpret", Some(m)) => {
            let input = m.value_of("INPUT").unwrap();
            let mut source = String::new();
            File::open(input)
                .map_err(|e| error!("Error while opening the file: {}", e))
                .and_then(|mut f| {
                    f.read_to_string(&mut source)
                        .map_err(|e| error!("Error while reading the file: {}", e))
                })
                .and_then(|_| {
                    debug!("Code in input:\n{}", &source);
                    parse_s_expr(CompleteStr(&source))
                        .map_err(|e|
                            error!("An error has occurred while parsing the program: {:?}", e))
                })
                .and_then(|(extra, exp)| {
                    info!("Parsed code:\n{}", exp);
                    if extra.input_len() > 0 {
                        warn!("Code not parsed:\n{}", extra)
                    }
                    Ok(exp)
                })
                .and_then(|exp| {
                    let p = &program(exp, State::new());
                    if m.is_present("operational") {
                        operational::Interpreter::run(&p)
                    } else {
                        denotational::Interpreter::run(&p)
                    }.map_err(|e| error!("Error during execution: {}", e))
                })
                .and_then(|c| {
                    info!("Final state: {}", c.state());
                    Ok(())
                })
                .unwrap_or(())
        }
        ("abstract", Some(m)) => {
            let input = m.value_of("INPUT").unwrap();
            let mut source = String::new();
            File::open(input)
                .map_err(|e| error!("Error while opening the file: {}", e))
                .and_then(|mut f| {
                    f.read_to_string(&mut source)
                        .map_err(|e| error!("Error while reading the file: {}", e))
                })
                .and_then(|_| {
                    debug!("Code in input:\n{}", &source);
                    parse_s_expr(CompleteStr(&source))
                        .map_err(|e|
                            error!("An error has occurred while parsing the program: {:?}", e))
                })
                .and_then(|(extra, exp)| {
                    info!("Parsed code:\n{}", exp);
                    if extra.input_len() > 0 {
                        warn!("Code not parsed:\n{}", extra)
                    }
                    Ok(exp)
                })
                .and_then(|exp| {
                    match m.value_of("abstraction").unwrap() {
                        "signs" => {
                            let p: AbstractConfig<Sign> = program(exp, State::new()).into();
                            abstracts::Interpreter::run(&p)
                                .map(|c| info!("Final state: {}", c.state()))
                                .map_err(|e| error!("Error during execution: {}", e))
                        }
                        "constants" => {
                            let p: AbstractConfig<Constant<i64>> = program(exp, State::new()).into();
                            abstracts::Interpreter::run(&p)
                                .map(|c| info!("Final state: {}", c.state()))
                                .map_err(|e| error!("Error during execution: {}", e))
                        }
                        "intervals" => {
                            let p: AbstractConfig<Interval<i64>> = program(exp, State::new()).into();
                            abstracts::Interpreter::run(&p)
                                .map(|c| info!("Final state: {}", c.state()))
                                .map_err(|e| error!("Error during execution: {}", e))
                        }
                        a => {
                            error!("Invalid abstraction \"{}\"", a);
                            Ok(())
                        }
                    }
                })
                .unwrap_or(())
        }

        _ => { a.print_help().unwrap_or(()) }
    }
}