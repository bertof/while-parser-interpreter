//! Concrete domains module
use rand::{Rng, thread_rng};

use crate::data::value::Value;

impl Value for i64 {
    fn rnd_range(min: Self, max: Self) -> Self {
        debug_assert!(min < max);
        thread_rng().gen_range(min, max)
    }
}