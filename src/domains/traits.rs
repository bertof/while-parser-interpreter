//! Domain traits module

use std::fmt;

use crate::data::value::{Value, AbstractValue};
use crate::domains::traits::Concretization::*;

/// Generic concretization of an abstract value.
pub enum Concretization<V> where V: Value {
    Top,
    Val(Box<dyn Iterator<Item=V>>),
    Bottom,
}

impl<V> fmt::Debug for Concretization<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Top => "Top",
            Val(_) => "Val",
            Bottom => "Bottom",
        })
    }
}

/// A value is Concretizable if it can be made into a concretization.
pub trait Concretizable<V>: Into<Concretization<V>> where V: Value {}

/// A value is Abstractable if it can be made into an abstract value.
pub trait Abstractable<A>: Into<A> where A: AbstractValue {}

/// Galois connection.
///
/// Grants that `to_concrete(to_abstract(c)) = c` for all c in C concretes.
pub trait GaloisConnection<A, C> where A: AbstractValue + Concretizable<C>, C: Value + Abstractable<A> {}

/// Galois embedding.
///
/// Galois connection where each element of the abstract world is necessary.
/// There are no multiple abstract representations of the same concretes element.
pub trait GaloisEmbedding<A, C>: GaloisConnection<A, C>
    where A: AbstractValue + Concretizable<C>, C: Value + Abstractable<A> {}


