//! Constants abstract domain module

use std::{fmt, ops};
use std::cmp::Ordering;

use crate::data::value::{AbstractValue, Value};

/// Constant abstract domain.
///
/// This domain represents values as constants. When a value is first assigned to a variable it will
/// be defined. If a value in a state is changed it will reach the top representation.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Constant<V> where V: Value + Ord {
    Top,
    Value(V),
    Bottom,
}

impl<V> PartialOrd for Constant<V> where V: Value + Ord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Constant::Top, Constant::Top) => Some(Ordering::Equal),
            (Constant::Value(v1), Constant::Value(v2)) => Some(v1.cmp(v2)),
            (Constant::Bottom, Constant::Bottom) => Some(Ordering::Equal),
            _ => None
        }
    }
}

impl<V> fmt::Display for Constant<V> where V: Value + Ord {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Constant::Top => write!(f, "⊤"),
            Constant::Value(v) => write!(f, "{}", v),
            Constant::Bottom => write!(f, "⊥")
        }
    }
}

impl<V> Value for Constant<V> where V: Value + Ord {
    fn rnd_range(min: Self, max: Self) -> Self {
        if min == max {
            min
        } else {
            Constant::Top
        }
    }
}

impl<V> AbstractValue for Constant<V> where V: Value + Ord {
    fn top() -> Self {
        Constant::Top
    }

    fn bottom() -> Self {
        Constant::Bottom
    }

    fn lub(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (s1, s2) {
            (Some(x), Some(y)) => match (&x, &y) {
                _ if *x == *y => x.clone(),
                _ if *x == Constant::Bottom || *y == Constant::Bottom => Constant::Bottom,
                _ => Self::top()
            }
            (Some(x), None) | (None, Some(x)) => x.clone(),
            (None, None) => panic!("Invalid input")
        }
    }

    fn glb(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (s1, s2) {
            (Some(x), Some(y)) => match (&x, &y) {
                _ if *x == *y => x.clone(),
                _ if *y == Constant::Top => x.clone(),
                _ if *x == Constant::Top => y.clone(),
                _ => Constant::Bottom,
            }
            _ => Constant::Bottom
        }
    }

    fn widen(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        Self::lub(s1, s2)
    }

    fn update_state(old_val: &Option<Self>, new_val: &Self) -> Self {
        Self::widen(&old_val, &Some(new_val.clone()))
    }
}

impl<V> ops::Neg for Constant<V> where V: Value + Ord {
    type Output = Self;

    fn neg(self) -> Self::Output {
        match &self {
            Constant::Value(v) => Constant::Value(-v.clone()),
            c => c.clone(),
        }
    }
}

impl<V> ops::Add for Constant<V> where V: Value + Ord {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Constant::Bottom, _) => Constant::Bottom,
            (_, Constant::Bottom) => Constant::Bottom,
            (Constant::Top, _) => Constant::Top,
            (_, Constant::Top) => Constant::Top,
            (Constant::Value(v1), Constant::Value(v2)) =>
                Constant::Value(v1.clone() + v2.clone()),
        }
    }
}

impl<V> ops::Sub for Constant<V> where V: Value + Ord {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Constant::Bottom, _) => Constant::Bottom,
            (_, Constant::Bottom) => Constant::Bottom,
            (Constant::Top, _) => Constant::Top,
            (_, Constant::Top) => Constant::Top,
            (Constant::Value(v1), Constant::Value(v2)) =>
                Constant::Value(v1.clone() - v2.clone()),
        }
    }
}

impl<V> ops::Mul for Constant<V> where V: Value + Ord {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Constant::Bottom, _) => Constant::Bottom,
            (_, Constant::Bottom) => Constant::Bottom,
            (Constant::Top, _) => Constant::Top,
            (_, Constant::Top) => Constant::Top,
            (Constant::Value(v1), Constant::Value(v2)) =>
                Constant::Value(v1.clone() * v2.clone()),
        }
    }
}

impl<V> ops::Div for Constant<V> where V: Value + Ord {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Constant::Bottom, _) => Constant::Bottom,
            (_, Constant::Bottom) => Constant::Bottom,
            (Constant::Top, _) => Constant::Top,
            (_, Constant::Top) => Constant::Top,
            (Constant::Value(v1), Constant::Value(v2)) =>
                Constant::Value(v1.clone() / v2.clone()),
        }
    }
}

pub mod concretization;
pub mod interpretation;

#[cfg(test)]
mod tests;