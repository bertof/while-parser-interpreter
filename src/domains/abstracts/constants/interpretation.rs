//! Constants interpretation module
use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::traits::{AbstractArithmeticInterpreter, AbstractBooleanInterpreter, AbstractInterpreter};

use super::*;

impl<V> AbstractArithmeticInterpreter<Constant<V>> for Interpreter where V: Value + Ord {}

impl<V> AbstractBooleanInterpreter<Constant<V>> for Interpreter where V: Value + Ord {}

impl<V> AbstractInterpreter<Constant<V>> for Interpreter where V: Value + Ord {}