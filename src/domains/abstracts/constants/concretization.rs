//! Constants concretization module
use std::iter;

use itertools::Itertools;

use crate::data::config::{AbstractConfig, Config};
use crate::data::state::State;
use crate::data::value::Value;
use crate::domains::abstracts::constants::Constant;
use crate::domains::expressions::stmt::arithmetic::AExpr;
use crate::domains::expressions::stmt::boolean::BExpr;
use crate::domains::expressions::stmt::semantic::SExpr;
use crate::domains::traits::{Abstractable, Concretizable, Concretization, GaloisConnection};

impl<V> Concretizable<V> for Constant<V> where V: 'static + Value + Ord {}

impl<V> Abstractable<Constant<V>> for V where V: Value + Ord {}

impl<V> GaloisConnection<Constant<V>, V> for Constant<V> where V: 'static + Value + Ord {}

impl<V> Into<Concretization<V>> for Constant<V> where V: 'static + Value + Ord {
    fn into(self) -> Concretization<V> {
        match self {
            Constant::Top => Concretization::Top,
            Constant::Value(v) => {
                let it = iter::once(v);
                Concretization::Val(Box::new(it))
            }
            Constant::Bottom => Concretization::Bottom,
        }
    }
}

impl<V> From<V> for Constant<V> where V: Value + Ord {
    fn from(val: V) -> Self {
        Constant::Value(val)
    }
}

impl<V> Into<AExpr<Constant<V>>> for AExpr<V> where V: Value + Ord {
    fn into(self) -> AExpr<Constant<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<BExpr<Constant<V>>> for BExpr<V> where V: Value + Ord {
    fn into(self) -> BExpr<Constant<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<SExpr<Constant<V>>> for SExpr<V> where V: Value + Ord {
    fn into(self) -> SExpr<Constant<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<State<Constant<V>>> for State<V> where V: Value + Ord {
    fn into(self) -> State<Constant<V>> {
        self.to_abstract(&|v| v.clone().into())
    }
}

impl<V> Into<AbstractConfig<Constant<V>>> for Config<V> where V: Value + Ord {
    fn into(self) -> AbstractConfig<Constant<V>> {
        let e = self.expression().cloned().map(|e| e.into());
        let s = self.state().keys()
            .iter()
            .cloned()
            .filter_map(|k| self.state().get(&k).cloned().map(|v| (k, v.into())))
            .collect_vec();
        if let Some(e) = e {
            AbstractConfig::Program(e, State::from_value_vec(s))
        } else {
            AbstractConfig::Ts(State::from_value_vec(s))
        }
    }
}