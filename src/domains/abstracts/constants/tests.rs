use crate::domains::abstracts::constants::Constant;
use crate::domains::expressions::stmt::prelude::*;
use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::traits::{
    AbstractArithmeticInterpreter,
    AbstractBooleanInterpreter,
    AbstractInterpreter,
};
use crate::utilities::logging::*;
use crate::utilities::table::{generate_ab_table, generate_f_table};

const VALUES: [Constant<i64>; 5] = [
    Constant::Top, Constant::Value(-1), Constant::Value(0),
    Constant::Value(1), Constant::Bottom
];

#[test]
/// Test Display and Debug implementation of Sign
fn test_constant_print() {
    init_log_test();

    for (k, v) in vec![
        ("Value(-1)", Constant::Value(-1)), ("Value(0)", Constant::Value(0)),
        ("Value(1)", Constant::Value(1)), ("Top", Constant::Top), ("Bottom", Constant::Bottom)] {
        debug!("Print {:<10}  Display: {:<5}  Debug: {:<10}", k, format!("{}", v), format!("{:?}", v));
    }
}

#[cfg(test)]
mod tables {
    use itertools::Itertools;
    use prettytable::{Row, Table};

    use super::*;

    #[test]
    fn test_constant_partial_order() {
        init_log_test();

        let table = generate_ab_table(&VALUES, &|a, b| a < b, "<");
        debug!("PartialOrder <\n{}", table);

        let table = generate_ab_table(&VALUES, &|a, b| a <= b, "<=");
        debug!("PartialOrder <=\n{}", table)
    }

    #[test]
    fn test_constant_neg() {
        init_log_test();

        let table = generate_f_table(&VALUES, &|a| -a.clone(), "-");
        debug!("ops::Neg neg\n{}", table);
    }

    #[test]
    fn test_constant_add() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() + b.clone(),
            "+");
        debug!("ops::Add add\n{}", table);
    }

    #[test]
    fn test_constant_sub() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() - b.clone(),
            "-");
        debug!("ops::Sub sub\n{}", table);
    }

    #[test]
    fn test_constant_mul() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() * b.clone(),
            "*");
        debug!("ops::Mul mul\n{}", table);
    }

    #[test]
    fn test_constant_div() {
        init_log_test();

        let mut table = Table::new();

        let mut r = vec!["/".to_string()];
        r.extend(VALUES.iter().map(|c| format!("{}", c)).collect_vec());
        table.set_titles(Row::from(r));

        for a in &VALUES {
            let mut r = vec![format!("{}", a)];
            r.extend(VALUES.iter().map(|b|
                if b == &Constant::Value(0) {
                    "Undef".to_string()
                } else {
                    format!("{}", a.clone() / b.clone())
                }).collect_vec());
            table.add_row(Row::from(r));
        }

        debug!("ops::Div div\n{}", table);
    }

    #[test]
    fn test_constant_unite() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Constant::lub(&Some(a.clone()), &Some(b.clone())),
            "∪");
        debug!("AbstractValue unite\n{}", table);
    }

    #[test]
    fn test_constant_intersect() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Constant::glb(&Some(a.clone()), &Some(b.clone())),
            "∩");
        debug!("AbstractValue intersect\n{}", table);
    }

    #[test]
    fn test_constant_widen() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Constant::widen(&Some(a.clone()), &Some(b.clone())),
            "∇");
        debug!("AbstractValue widen\n{}", table);
    }
}

#[cfg(test)]
mod a_eval {
    use super::*;

    #[test]
    fn test_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = val(15).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(15));
        Ok(())
    }

    #[test]
    fn test_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 12).into();
        let e: AExpr<Constant<i64>> = var("x");
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(12));
        Ok(())
    }

    #[test]
    fn test_eval_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = sum(val(1), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = dif(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(-1));
        Ok(())
    }

    #[test]
    fn test_eval_mul() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = mul(val(2), val(3)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(6));
        Ok(())
    }

    #[test]
    fn test_eval_div() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = div(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Value(0));
        Ok(())
    }

    #[test]
    /// Test range valber generation
    fn test_range() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: AExpr<Constant<i64>> = range(val(15), val(30)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Constant::Top);
        Ok(())
    }
}

#[cfg(test)]
mod b_expr {
    use super::*;

    #[test]
    fn test_eval_tru() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = tru();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_fal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = fal();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(tru());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(not(tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_not_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(not(not(fal())));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_and() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = and(tru(), fal());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_and() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(and(tru(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_or() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e = or(tru(), fal());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(or(tru(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(or(tru(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(or(fal(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(or(fal(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(val(1), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(val(1), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = eq(var("x"), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = eq(var("x"), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = eq(val(1), var("x")).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = eq(val(2), var("x")).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(val(2), sum(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(sum(val(1), val(1)), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(val(0), dif(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_mul_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(mul(val(1), val(0)), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_div_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = eq(div(val(10), val(2)), val(5)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            val(1),
            val(1),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            val(1),
            val(2),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = not(eq(
            var("x"),
            val(1),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = not(eq(
            var("x"),
            val(2),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 1)
            .into();
        let e: BExpr<Constant<i64>> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        assert_eq!(r.get("y").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 2)
            .into();
        let e: BExpr<Constant<i64>> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        assert_eq!(r.get("y").unwrap(), &Constant::Value(2));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 2)
            .into();
        let e: BExpr<Constant<i64>> = not(eq(
            var("x"),
            var("y"),
        ));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        assert_eq!(r.get("y").unwrap(), &Constant::Value(2));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 1)
            .into();
        let e: BExpr<Constant<i64>> = not(eq(
            var("x"),
            var("y"),
        ));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        assert_eq!(r.get("y").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = not(eq(
            val(1),
            var("x"),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Constant<i64>> = not(eq(
            val(2),
            var("x"),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            val(2),
            sum(val(1), val(1)),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            sum(val(1), val(1)),
            val(2),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            val(0),
            dif(val(1), val(1)),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(eq(
            dif(val(1), val(1)),
            val(0),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }


    #[test]
    fn test_eval_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = leq(val(1), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_val_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 2).into();
        let e: BExpr<Constant<i64>> = leq(val(1), var("x")).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", 0).into();
        let e: BExpr<Constant<i64>> = leq(var("x"), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 0)
            .set("y", 1)
            .into();
        let e: BExpr<Constant<i64>> = leq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = leq(val(2), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = leq(val(3), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(1), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(2), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(3), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(1), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new();
        let e: BExpr<Constant<i64>> = not(leq(val(2), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 2).into();
        let e: BExpr<Constant<i64>> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 1)
            .set("y", 1).into();
        let e: BExpr<Constant<i64>> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new()
            .set("x", 2)
            .set("y", 1).into();
        let e: BExpr<Constant<i64>> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }
}

#[cfg(test)]
mod s_expr {
    use super::*;

    #[test]
    fn test_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ass("x", val(3));
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(3));
        Ok(())
    }

    #[test]
    fn test_double_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 3);
        let e = ass("x", val(5));
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }

    #[test]
    fn test_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1).set("y", 5);
        let e = skip();
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = conc(skip(), skip());
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(2)),
            ass("y", val(3)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(2));
        assert_eq!(r.state().get("y").unwrap(), &Constant::Value(3));
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(2)),
            ass("x", val(3)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_same() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(2)),
            ass("x", var("x")),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(2));
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(0)),
            ass("x", sum(var("x"), val(1))),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }

    #[test]
    fn test_ite_true_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            tru(),
            ass("x", val(1)),
            ass("x", val(2)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_ite_false_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            fal(),
            ass("x", val(1)),
            ass("x", val(2)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(2));
        Ok(())
    }

    #[test]
    fn test_ite_top_condition() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Constant<i64>> = State::new().set("x", Constant::Top);
        let e: SExpr<Constant<i64>> = ite(
            eq(var("x"), val(2)),
            ass("y", val(1)),
            ass("y", val(2)),
        ).into();
        let p = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("y").unwrap(), &Constant::Top);
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }

    #[test]
    fn test_while_loop_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(fal(), ass("x", val(1)));
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x"), None);
        Ok(())
    }

    #[test]
    fn test_while_loop_true_same_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(tru(), ass("x", val(1)));
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_while_loop_true_different_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0);
        let e = wd(tru(), ass("x", sum(var("x"), val(1))));
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }

    #[test]
    fn test_while_loop_constant() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = wd(
            eq(var("x"), val(1)),
            ass("x", val(1)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(1));
        Ok(())
    }

    #[test]
    fn test_while_loop_var_change() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1).set("y", 5);
        let e = wd(
            eq(var("x"), val(1)),
            ass("x", val(2)),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        assert_eq!(r.state().get("y").unwrap(), &Constant::Value(5));
        Ok(())
    }

    #[test]
    fn test_while_loop_until() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            not(eq(var("x"), var("y"))),
            ass("x", sum(val(1), var("x"))),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        assert_eq!(r.state().get("y").unwrap(), &Constant::Value(5));
        Ok(())
    }

    #[test]
    fn test_while_nested_infinite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            wd(
                eq(var("x"), val(1)),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Value(0));
        assert_eq!(r.state().get("y").unwrap(), &Constant::Value(5));
        Ok(())
    }

    #[test]
    fn test_while_nested_finite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            conc(
                wd(
                    eq(var("x"), val(1)),
                    ass("x", var("y"))),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Constant<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        assert_eq!(r.state().get("y").unwrap(), &Constant::Value(5));
        Ok(())
    }
}

#[cfg(test)]
mod programs {
    use crate::interpreter::examples::*;

    use super::*;

    #[test]
    fn test_factorial() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Constant<i64>> = program_factorial_5().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        assert_eq!(r.state().get("y").unwrap(), &Constant::Top);
        Ok(())
    }


    #[test]
    /// "while true do skip" test. It's run only if the environment variable "REPEAT" is set and it will
    /// loop forever.
    fn test_infinite_loop() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Constant<i64>> = infinite_loop().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        Ok(())
    }

    #[test]
    /// "while true ++i" test. It's run only if the environment variable "REPEAT" is set and it will
    /// loop forever.
    fn test_infinite_increment() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Constant<i64>> = infinite_increment().into();
        debug!("{}\n", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Constant::Top);
        Ok(())
    }
}
