//! Signs concretization module
use itertools::{interleave, Itertools};

use crate::domains::traits::GaloisConnection;

use super::*;

impl Concretizable<i64> for Sign {}

impl Abstractable<Sign> for i64 {}

impl GaloisConnection<Sign, i64> for Sign {}

impl Into<Concretization<i64>> for Sign {
    fn into(self) -> Concretization<i64> {
        match self {
            Sign::Top => Concretization::Top,
            Sign::LZero => Concretization::Val(Box::new((1 as i64..).map(|v| -v))),
            Sign::LEqZero => Concretization::Val(Box::new((0 as i64..).map(|v| -v))),
            Sign::Zero => Concretization::Val(Box::new(0 as i64..=0)),
            Sign::NotZero => Concretization::Val(Box::new(interleave(1 as i64.., (1 as i64..).map(|v| -v)))),
            Sign::GEqZero => Concretization::Val(Box::new(0 as i64..)),
            Sign::GZero => Concretization::Val(Box::new(1 as i64..)),
            Sign::Bottom => Concretization::Bottom,
        }
    }
}

impl From<i64> for Sign {
    fn from(v: i64) -> Self {
        match v.cmp(&0) {
            Ordering::Greater => Sign::GZero,
            Ordering::Equal => Sign::Zero,
            Ordering::Less => Sign::LZero,
        }
    }
}

impl Into<AExpr<Sign>> for AExpr<i64> {
    fn into(self) -> AExpr<Sign> {
        self.apply(&|v| v.clone().into())
    }
}

impl Into<BExpr<Sign>> for BExpr<i64> {
    fn into(self) -> BExpr<Sign> {
        self.apply(&|v| v.clone().into())
    }
}

impl Into<SExpr<Sign>> for SExpr<i64> {
    fn into(self) -> SExpr<Sign> {
        self.apply(&|v| v.clone().into())
    }
}

impl Into<State<Sign>> for State<i64> {
    fn into(self) -> State<Sign> {
        self.to_abstract(&|v| v.clone().into())
    }
}

impl Into<AbstractConfig<Sign>> for Config<i64> {
    fn into(self) -> AbstractConfig<Sign> {
        let e = self.expression().cloned().map(|e| e.into());
        let s = self.state().keys()
            .iter()
            .cloned()
            .filter_map(|k| self.state().get(&k).cloned().map(|v| (k, v.into())))
            .collect_vec();
        if let Some(e) = e {
            AbstractConfig::Program(e, State::from_value_vec(s))
        } else {
            AbstractConfig::Ts(State::from_value_vec(s))
        }
    }
}