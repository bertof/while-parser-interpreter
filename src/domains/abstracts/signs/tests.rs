use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::examples::*;
use crate::interpreter::traits::{
    AbstractArithmeticInterpreter,
    AbstractBooleanInterpreter,
    AbstractInterpreter,
};
use crate::utilities::logging::*;
use crate::utilities::table::{generate_ab_table, generate_f_table};

use super::*;

const VALUES: [Sign; 8] = [Sign::Top, Sign::LZero, Sign::LEqZero, Sign::Zero, Sign::NotZero, Sign::GEqZero, Sign::GZero, Sign::Bottom];


#[test]
fn test_sign_print() {
    init_log_test();

    for (k, v) in vec![
        ("LZero", Sign::LZero), ("LEqZero", Sign::LEqZero), ("Zero", Sign::Zero),
        ("NotZero", Sign::NotZero), ("GEqZero", Sign::GEqZero), ("GZero", Sign::GZero),
        ("Top", Sign::Top), ("Bottom", Sign::Bottom)] {
        debug!("Print {:<10}  Display: {:<5}  Debug: {:<10}", k, format!("{}", v), format!("{:?}", v));
    }
}

mod tables {
    use super::*;

    #[test]
    fn test_sign_partial_order() {
        init_log_test();

        let table = generate_ab_table(&VALUES, &|a, b| a < b, "<");
        debug!("PartialOrder <\n{}", table);

        let table = generate_ab_table(&VALUES, &|a, b| a <= b, "<=");
        debug!("PartialOrder <=\n{}", table)
    }


    #[test]
    fn test_sign_rand_range() {
        init_log_test();

        let table = generate_ab_table(&VALUES, &|a, b| Sign::rnd_range(a.clone(), b.clone()), "rand");
        debug!("Value rang_range\n{}", table);
    }

    #[test]
    fn test_sign_neg() {
        init_log_test();

        let table = generate_f_table(&VALUES, &|a| -a.clone(), "-");
        debug!("ops::Neg neg\n{}", table);
    }

    #[test]
    fn test_sign_not() {
        init_log_test();

        let table = generate_f_table(&VALUES, &|a| !a.clone(), "!");
        debug!("ops::Not not\n{}", table);
    }

    #[test]
    fn test_sign_add() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() + b.clone(),
            "+");
        debug!("ops::Add add\n{}", table);
    }

    #[test]
    fn test_sign_sub() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() - b.clone(),
            "-");
        debug!("ops::Sub sub\n{}", table);
    }

    #[test]
    fn test_sign_mul() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() * b.clone(),
            "*");
        debug!("ops::Mul mul\n{}", table);
    }

    #[test]
    fn test_sign_div() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| a.clone() / b.clone(),
            "/");
        debug!("ops::Div div\n{}", table);
    }

    #[test]
    fn test_sign_unite() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Sign::lub(&Some(a.clone()), &Some(b.clone())),
            "∪");
        debug!("AbstractValue unite\n{}", table);
    }

    #[test]
    fn test_sign_intersect() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Sign::glb(&Some(a.clone()), &Some(b.clone())),
            "∩");
        debug!("AbstractValue intersect\n{}", table);
    }

    #[test]
    fn test_sign_widen() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES,
            &|a, b| Sign::widen(&Some(a.clone()), &Some(b.clone())),
            "∇");
        debug!("AbstractValue widen\n{}", table);
    }
}

mod concretization {
    use itertools::Itertools;

    use super::*;

    #[test]
    fn test_sign_to_concrete() {
        init_log_test();
        match Sign::Top.into() {
            Concretization::Top => {}
            _ => panic!("Incorrect concretization")
        };
        match Sign::GZero.into() {
            Concretization::Val(i) =>
                assert_eq!(i.take(5).collect_vec(), vec![1, 2, 3, 4, 5]),
            _ => panic!("Incorrect concretization")
        }
        match Sign::GEqZero.into() {
            Concretization::Val(i) =>
                assert_eq!(i.take(5).collect_vec(), vec![0, 1, 2, 3, 4]),
            _ => panic!("Incorrect concretization")
        }
        match Sign::Zero.into() {
            Concretization::Val(i) =>
                assert_eq!(i.take(5).collect_vec(), vec![0]),
            _ => panic!("Incorrect concretization")
        }
        match Sign::LEqZero.into() {
            Concretization::Val(i) =>
                assert_eq!(i.take(5).collect_vec(), vec![0, -1, -2, -3, -4]),
            _ => panic!("Incorrect concretization")
        }
        match Sign::LZero.into() {
            Concretization::Val(i) =>
                assert_eq!(i.take(5).collect_vec(), vec![-1, -2, -3, -4, -5]),
            _ => panic!("Incorrect concretization")
        }
        match Sign::Bottom.into() {
            Concretization::Bottom => {}
            _ => panic!("Incorrect concretization")
        };
    }

    #[test]
    fn test_i64_to_sign() {
        init_log_test();
        assert_eq!(Sign::Zero, (0 as i64).into());
        assert_eq!(Sign::GZero, (12 as i64).into());
        assert_eq!(Sign::LZero, (-12 as i64).into());
    }

    #[test]
    fn test_aexpr_i64_to_aexpr_sign() {
        init_log_test();
        assert_eq!(val(Sign::Zero), val(0).into());
        assert_eq!(sum(val(Sign::GZero), val(Sign::LZero)), sum(val(1), val(-1)).into());
    }

    #[test]
    fn test_bexpr_i64_to_bexpr_sign() {
        init_log_test();
        assert_eq!(leq(val(Sign::Zero), val(Sign::GZero)), leq(val(0), val(1)).into());
        assert_eq!(eq(val(Sign::Zero), val(Sign::GZero)), eq(val(0), val(1)).into());
    }

    #[test]
    fn test_sexpr_i64_to_sexpr_sign() {
        init_log_test();
        assert_eq!(ass("x", val(Sign::Zero)), ass("x", val(0)).into());
        assert_eq!(wd(leq(val(Sign::Zero), val(Sign::GZero)), skip()), wd(leq(val(0), val(1)), skip()).into());
    }
}

#[cfg(test)]
mod a_eval {
    use super::*;

    #[test]
    fn test_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = val(15).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 12).into();
        let e: AExpr<Sign> = var("x");
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = sum(val(1), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = sum(val(0), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_sum_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = sum(val(1), val(-1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        let e: AExpr<Sign> = sum(val(-1), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        Ok(())
    }

    #[test]
    fn test_eval_sum_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = sum(val(1), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = sum(val(0), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = dif(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        let e: AExpr<Sign> = dif(val(2), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        Ok(())
    }

    #[test]
    fn test_eval_dif_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = dif(val(1), val(-2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = dif(val(-2), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_eval_dif_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = dif(val(1), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = dif(val(0), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_eval_mul() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = mul(val(2), val(3)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = mul(val(3), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_mul_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = mul(val(2), val(-3)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        let e: AExpr<Sign> = mul(val(-3), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_eval_mul_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = mul(val(2), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Zero);
        let e: AExpr<Sign> = mul(val(0), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Zero);
        Ok(())
    }

    #[test]
    fn test_eval_div() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = div(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = div(val(2), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_div_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = div(val(1), val(-2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        let e: AExpr<Sign> = div(val(-2), val(1)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_range() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = range(val(15), val(30)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = range(val(30), val(15)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GZero);
        let e: AExpr<Sign> = range(val(-15), val(-30)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        let e: AExpr<Sign> = range(val(-30), val(-15)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = range(val(15), val(-30)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        let e: AExpr<Sign> = range(val(-30), val(15)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::Top);
        Ok(())
    }

    #[test]
    /// Test range valber generation
    fn test_range_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: AExpr<Sign> = range(val(15), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GEqZero);
        let e: AExpr<Sign> = range(val(0), val(15)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::GEqZero);
        let e: AExpr<Sign> = range(val(-15), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LEqZero);
        let e: AExpr<Sign> = range(val(0), val(-15)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Sign::LEqZero);
        Ok(())
    }
}

#[cfg(test)]
mod b_expr {
    use super::*;

    #[test]
    fn test_eval_tru() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = tru();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_fal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = fal();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(tru());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_not_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(not(tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_not_not_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(not(not(fal())));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_and_true_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = and(tru(), fal());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_and_false_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = and(tru(), fal());
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_and_t_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(and(tru(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_and_f_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(and(fal(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_and_t_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(and(tru(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_and_f_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(and(fal(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(or(tru(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(or(tru(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(or(fal(), tru()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(or(fal(), fal()));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_true_unknown() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(1), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_true_zeros() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(0), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(-1), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let e: BExpr<Sign> = eq(val(0), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let e: BExpr<Sign> = eq(val(Sign::GEqZero), val(Sign::LZero));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let e: BExpr<Sign> = eq(val(Sign::LEqZero), val(Sign::GZero));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }


    #[test]
    fn test_eval_eq_var_true_unknown() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Sign> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_true_zeros() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 0).set("y", 0).into();
        let e: BExpr<Sign> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let e: BExpr<Sign> = eq(var("x"), var("y"));

        let s: State<Sign> = State::new().set("x", -1).set("y", 0).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let s: State<Sign> = State::new().set("x", 0).set("y", 1).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let s: State<Sign> = State::new().set("x", Sign::LEqZero).set("y", Sign::GZero);
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let s: State<Sign> = State::new().set("x", Sign::LZero).set("y", Sign::GEqZero);
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }


    #[test]
    fn test_eval_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(2), sum(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(sum(val(1), val(1)), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(dif(val(1), val(1)), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(10), dif(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_mul_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(mul(val(1), val(0)), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_mul() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(val(0), mul(val(1), val(0))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_div_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = eq(div(val(10), val(2)), val(5)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(val(1), val(2),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).into();
        let e: BExpr<Sign> = not(eq(var("x"), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).into();
        let e: BExpr<Sign> = not(eq(var("x"), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).set("y", 1).into();
        let e: BExpr<Sign> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        assert_eq!(r.get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Sign> = eq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        assert_eq!(r.get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Sign> = not(eq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        assert_eq!(r.get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).set("y", 1).into();
        let e: BExpr<Sign> = not(eq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        assert_eq!(r.get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).into();
        let e: BExpr<Sign> = not(eq(val(1), var("x"))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 1).into();
        let e: BExpr<Sign> = not(eq(val(2), var("x"))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(val(2), sum(val(1), val(1)),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(sum(val(1), val(1)), val(-2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(val(0), dif(val(1), val(-1)),
        )).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(eq(dif(val(1), val(-1)), val(0))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = leq(val(1), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_val_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 2).into();
        let e: BExpr<Sign> = leq(val(1), var("x")).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 0).into();
        let e: BExpr<Sign> = leq(var("x"), val(1)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 0).set("y", 1).into();
        let e: BExpr<Sign> = leq(var("x"), var("y"));
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = leq(val(2), val(2)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = leq(val(3), val(0)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        let e: BExpr<Sign> = leq(val(3), val(-7)).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(0), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());

        let e: BExpr<Sign> = not(leq(val(-1), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(0), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(3), val(0))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(0), val(2))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(1), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new();
        let e: BExpr<Sign> = not(leq(val(2), val(1))).into();
        let r = Interpreter::b_filter(&e, &s)?;
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 0).set("y", 2).into();
        let e: BExpr<Sign> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 0).set("y", 0).into();
        let e: BExpr<Sign> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", 2).set("y", -1).into();
        let e: BExpr<Sign> = not(leq(var("x"), var("y")));
        let r = Interpreter::b_filter(&e, &s)?;
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }
}

#[cfg(test)]
mod s_expr {
    use super::*;

    #[test]
    fn test_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ass("x", val(3));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_double_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 3);
        let e = ass("x", val(5));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_double_ass_neg() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 3);
        let e = ass("x", val(-5));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1).set("y", 5);
        let e = skip();
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = conc(skip(), skip());
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(ass("x", val(2)), ass("y", val(3)));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        assert_eq!(r.state().get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(ass("x", val(2)), ass("x", val(3)));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_same() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(2)),
            ass("x", var("x")),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(0)),
            ass("x", sum(var("x"), val(1))),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_ite_true_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            tru(),
            ass("x", val(1)),
            ass("x", val(-2)),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_ite_false_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            fal(),
            ass("x", val(1)),
            ass("x", val(-2)),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::LZero);
        Ok(())
    }

    #[test]
    fn test_ite_top_condition() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Sign> = State::new().set("x", Sign::Top);
        let e: SExpr<Sign> = ite(
            eq(var("x"), val(2)),
            ass("y", val(1)),
            ass("y", val(-2)),
        ).into();
        let p = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("y").unwrap(), &Sign::NotZero);
        assert_eq!(r.state().get("x").unwrap(), &Sign::Top);
        Ok(())
    }

    #[test]
    fn test_while_loop_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(fal(), ass("x", val(1)));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x"), None);
        Ok(())
    }

    #[test]
    fn test_while_loop_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(tru(), ass("x", val(1)));
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_while_loop_constant() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = wd(
            eq(var("x"), val(1)),
            ass("x", val(1)),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_while_loop_var_change() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = wd(
            eq(var("x"), val(0)),
            ass("x", val(2)),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_while_loop_until() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            not(eq(var("x"), var("y"))),
            ass("x", sum(val(1), var("x"))),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GEqZero);
        assert_eq!(r.state().get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_while_nested_infinite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            wd(
                eq(var("x"), val(1)),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::Zero);
        assert_eq!(r.state().get("y").unwrap(), &Sign::GZero);
        Ok(())
    }

    #[test]
    fn test_while_nested_finite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            conc(
                wd(
                    eq(var("x"), val(1)),
                    ass("x", var("y"))),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Sign> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GEqZero);
        assert_eq!(r.state().get("y").unwrap(), &Sign::GZero);
        Ok(())
    }
}

mod programs {
    use super::*;

    #[test]
    fn test_factorial() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Sign> = program_factorial_5().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::Top);
        assert_eq!(r.state().get("y").unwrap(), &Sign::Top);
        Ok(())
    }


    #[test]
    fn test_infinite_loop() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Sign> = infinite_loop().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        Ok(())
    }

    #[test]
    fn test_infinite_increment() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Sign> = infinite_increment().into();
        debug!("{}\n", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Sign::GZero);
        Ok(())
    }
}




