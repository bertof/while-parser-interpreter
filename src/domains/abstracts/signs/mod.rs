//! Sign abstract domain module

use std::{fmt, ops};
use std::cmp::Ordering;

use crate::domains::expressions::stmt::prelude::*;
use crate::domains::traits::{Abstractable, Concretizable, Concretization};

/// Sign abstract domain
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Sign {
    Top,
    GEqZero,
    LEqZero,
    LZero,
    GZero,
    NotZero,
    Zero,
    Bottom,
}

impl fmt::Display for Sign {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match &self {
            Sign::Top => "⊤",
            Sign::GEqZero => "≥0",
            Sign::Zero => "0",
            Sign::LEqZero => "≤0",
            Sign::Bottom => "⊥",
            Sign::LZero => "<0",
            Sign::GZero => ">0",
            Sign::NotZero => "!=0"
        })
    }
}

impl PartialOrd for Sign {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Sign::Zero, Sign::Zero) | (Sign::Bottom, Sign::Bottom) =>
                Some(Ordering::Equal),

            (Sign::LZero, Sign::Zero) | (Sign::LZero, Sign::GZero) |
            (Sign::Zero, Sign::GZero) | (Sign::LZero, Sign::GEqZero) |
            (Sign::LEqZero, Sign::GZero) => Some(Ordering::Less),

            (Sign::Zero, Sign::LZero) | (Sign::GZero, Sign::LZero) |
            (Sign::GEqZero, Sign::LZero) | (Sign::GZero, Sign::LEqZero) |
            (Sign::GZero, Sign::Zero) => Some(Ordering::Greater),

            _ => None
        }
    }
}

impl Value for Sign {
    fn rnd_range(min: Self, max: Self) -> Self {
        match (&min, &max) {
            (Sign::Bottom, _) | (_, Sign::Bottom) => Sign::Bottom,

            (Sign::LZero, Sign::LZero) => Sign::LZero,

            (Sign::LEqZero, Sign::LZero) | (Sign::LZero, Sign::LEqZero) |
            (Sign::LEqZero, Sign::Zero) | (Sign::Zero, Sign::LEqZero) |
            (Sign::LZero, Sign::Zero) | (Sign::Zero, Sign::LZero) |
            (Sign::LEqZero, Sign::LEqZero) => Sign::LEqZero,

            (Sign::Zero, Sign::Zero) => Sign::Zero,

            (Sign::GEqZero, Sign::GZero) | (Sign::GZero, Sign::GEqZero) |
            (Sign::GEqZero, Sign::Zero) | (Sign::Zero, Sign::GEqZero) |
            (Sign::GZero, Sign::Zero) | (Sign::Zero, Sign::GZero) |
            (Sign::GEqZero, Sign::GEqZero) => Sign::GEqZero,

            (Sign::GZero, Sign::GZero) => Sign::GZero,

            _ => Sign::Top,
        }
    }
}

impl AbstractValue for Sign {
    fn top() -> Self {
        Sign::Top
    }

    fn bottom() -> Self {
        Sign::Bottom
    }

    fn lub(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (s1, s2) {
            (Some(x), Some(y)) => match (&x, &y) {
                (Sign::Bottom, Sign::Bottom) => Sign::Bottom,
                (Sign::Bottom, &x) | (&x, Sign::Bottom) => x.clone(),
                (Sign::Top, _) | (_, Sign::Top) => Sign::Top,

                (Sign::LZero, Sign::LZero) => Sign::LZero,
                (Sign::LZero, Sign::LEqZero) | (Sign::LEqZero, Sign::LZero) |
                (Sign::LZero, Sign::Zero) | (Sign::Zero, Sign::LZero) |
                (Sign::LEqZero, Sign::Zero) | (Sign::Zero, Sign::LEqZero) |
                (Sign::LEqZero, Sign::LEqZero) => Sign::LEqZero,
                (Sign::Zero, Sign::Zero) => Sign::Zero,
                (Sign::LZero, Sign::GZero) | (Sign::GZero, Sign::LZero) |
                (Sign::LZero, Sign::NotZero) | (Sign::NotZero, Sign::LZero) |
                (Sign::NotZero, Sign::GZero) | (Sign::GZero, Sign::NotZero) |
                (Sign::NotZero, Sign::NotZero) => Sign::NotZero,
                (Sign::Zero, Sign::GEqZero) | (Sign::GEqZero, Sign::Zero) |
                (Sign::Zero, Sign::GZero) | (Sign::GZero, Sign::Zero) |
                (Sign::GEqZero, Sign::GZero) | (Sign::GZero, Sign::GEqZero) |
                (Sign::GEqZero, Sign::GEqZero) => Sign::GEqZero,

                (Sign::GZero, Sign::GZero) => Sign::GZero,
                (Sign::LZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LZero) |
                (Sign::LEqZero, Sign::NotZero) | (Sign::NotZero, Sign::LEqZero) |
                (Sign::LEqZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LEqZero) |
                (Sign::LEqZero, Sign::GZero) | (Sign::GZero, Sign::LEqZero) |
                (Sign::Zero, Sign::NotZero) | (Sign::NotZero, Sign::Zero) |
                (Sign::NotZero, Sign::GEqZero) | (Sign::GEqZero, Sign::NotZero) => Sign::Top,
            }
            (Some(x), None) | (None, Some(x)) => x.clone(),
            _ => Self::top()
        }
    }

    fn glb(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (s1, s2) {
            (Some(x), Some(y)) => match (&x, &y) {
                (Sign::Bottom, _) | (_, Sign::Bottom) => Sign::Bottom,
                (Sign::Top, &x) | (&x, Sign::Top) => x.clone(),
                (Sign::LZero, Sign::LEqZero) | (Sign::LEqZero, Sign::LZero) |
                (Sign::LZero, Sign::NotZero) | (Sign::NotZero, Sign::LZero) |
                (Sign::LEqZero, Sign::NotZero) | (Sign::NotZero, Sign::LEqZero) |
                (Sign::LZero, Sign::LZero) => Sign::LZero,
                (Sign::LEqZero, Sign::LEqZero) => Sign::LEqZero,
                (Sign::LEqZero, Sign::Zero) | (Sign::Zero, Sign::LEqZero) |
                (Sign::LEqZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LEqZero) |
                (Sign::Zero, Sign::GEqZero) | (Sign::GEqZero, Sign::Zero) |
                (Sign::Zero, Sign::Zero) => Sign::Zero,
                (Sign::GEqZero, Sign::GEqZero) => Sign::GEqZero,

                (Sign::NotZero, Sign::NotZero) => Sign::NotZero,
                (Sign::NotZero, Sign::GEqZero) | (Sign::GEqZero, Sign::NotZero) |
                (Sign::NotZero, Sign::GZero) | (Sign::GZero, Sign::NotZero) |
                (Sign::GZero, Sign::GEqZero) | (Sign::GEqZero, Sign::GZero) |
                (Sign::GZero, Sign::GZero) => Sign::GZero,
                (Sign::LZero, Sign::Zero) | (Sign::Zero, Sign::LZero) |
                (Sign::GZero, Sign::Zero) | (Sign::Zero, Sign::GZero) |
                (Sign::LZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LZero) |
                (Sign::GZero, Sign::LEqZero) | (Sign::LEqZero, Sign::GZero) |
                (Sign::LZero, Sign::GZero) | (Sign::GZero, Sign::LZero) |
                (Sign::Zero, Sign::NotZero) | (Sign::NotZero, Sign::Zero) => Sign::Bottom,
            }
            (None, None) => panic!("Invalid input"),
            _ => Sign::Bottom
        }
    }

    fn widen(s1: &Option<Self>, s2: &Option<Self>) -> Self { Self::lub(s1, s2) }
}

impl ops::Neg for Sign {
    type Output = Sign;

    fn neg(self) -> Self::Output {
        match self {
            Sign::GEqZero => Sign::LEqZero,
            Sign::LEqZero => Sign::GEqZero,
            Sign::GZero => Sign::LZero,
            Sign::LZero => Sign::GZero,
            _ => self,
        }
    }
}

impl ops::Not for Sign {
    type Output = Sign;

    fn not(self) -> Self::Output {
        match self {
            Sign::GEqZero => Sign::LZero,
            Sign::LEqZero => Sign::GZero,
            Sign::GZero => Sign::LEqZero,
            Sign::LZero => Sign::GEqZero,
            Sign::Zero => Sign::NotZero,
            Sign::NotZero => Sign::Zero,
            Sign::Top => Sign::Bottom,
            Sign::Bottom => Sign::Top,
        }
    }
}

impl ops::Add for Sign {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Sign::Bottom, _) | (_, Sign::Bottom) => Sign::Bottom,
            (Sign::Top, _) | (_, Sign::Top) => Sign::Top,

            (Sign::LZero, Sign::LEqZero) | (Sign::LEqZero, Sign::LZero) |
            (Sign::LZero, Sign::LZero) => Sign::LZero,
            (Sign::LEqZero, Sign::LEqZero) => Sign::LEqZero,
            (Sign::Zero, x) | (x, Sign::Zero) => x.clone(),

            (Sign::GEqZero, Sign::GEqZero) => Sign::GEqZero,
            (Sign::GZero, Sign::GEqZero) | (Sign::GEqZero, Sign::GZero) |
            (Sign::GZero, Sign::GZero) => Sign::GZero,
            (Sign::LZero, Sign::NotZero) | (Sign::NotZero, Sign::LZero) |
            (Sign::LZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LZero) |
            (Sign::LZero, Sign::GZero) | (Sign::GZero, Sign::LZero) |
            (Sign::LEqZero, Sign::NotZero) | (Sign::NotZero, Sign::LEqZero) |
            (Sign::LEqZero, Sign::GEqZero) | (Sign::GEqZero, Sign::LEqZero) |
            (Sign::LEqZero, Sign::GZero) | (Sign::GZero, Sign::LEqZero) |
            (Sign::NotZero, Sign::GEqZero) | (Sign::GEqZero, Sign::NotZero) |
            (Sign::NotZero, Sign::GZero) | (Sign::GZero, Sign::NotZero) |
            (Sign::NotZero, Sign::NotZero) => Sign::Top,
        }
    }
}

impl ops::Sub for Sign {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        self + (-rhs)
    }
}

impl ops::Mul for Sign {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Sign::Bottom, _) | (_, Sign::Bottom) => Sign::Bottom,
            (Sign::Zero, _) | (_, Sign::Zero) => Sign::Zero,

            (Sign::Top, _) | (_, Sign::Top) => Sign::Top,

            (Sign::LEqZero, Sign::LEqZero) |
            (Sign::LZero, Sign::LEqZero) |
            (Sign::LEqZero, Sign::LZero) |
            (Sign::GEqZero, Sign::GEqZero) |
            (Sign::GEqZero, Sign::GZero) |
            (Sign::GZero, Sign::GEqZero) => Sign::GEqZero,
            (Sign::LZero, Sign::LZero) |
            (Sign::GZero, Sign::GZero) => Sign::GZero,
            (Sign::LEqZero, Sign::GEqZero) |
            (Sign::LEqZero, Sign::GZero) |
            (Sign::GZero, Sign::LEqZero) |
            (Sign::GEqZero, Sign::LZero) |
            (Sign::GEqZero, Sign::LEqZero) |
            (Sign::LZero, Sign::GEqZero) => Sign::LEqZero,
            (Sign::GZero, Sign::LZero) |
            (Sign::LZero, Sign::GZero) => Sign::LZero,
            (Sign::GZero, Sign::NotZero) |
            (Sign::NotZero, Sign::GZero) |
            (Sign::LZero, Sign::NotZero) |
            (Sign::NotZero, Sign::LZero) |
            (Sign::NotZero, Sign::NotZero) => Sign::NotZero,

            (Sign::LEqZero, Sign::NotZero) |
            (Sign::NotZero, Sign::LEqZero) |
            (Sign::NotZero, Sign::GEqZero) |
            (Sign::GEqZero, Sign::NotZero) => Sign::Top,
        }
    }
}

impl ops::Div for Sign {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Sign::Bottom, _) | (_, Sign::Bottom) => Sign::Bottom,

            (_, Sign::Zero) => Sign::Bottom,
            (Sign::Zero, _) => Sign::Zero,
            (Sign::GZero, Sign::Top) | (Sign::LZero, Sign::Top) |
            (Sign::GZero, Sign::NotZero) | (Sign::LZero, Sign::NotZero) |
            (Sign::NotZero, Sign::GEqZero) | (Sign::NotZero, Sign::GZero) |
            (Sign::NotZero, Sign::LZero) | (Sign::NotZero, Sign::LEqZero) |
            (Sign::NotZero, Sign::Top) | (Sign::NotZero, Sign::NotZero) => Sign::NotZero,
            (Sign::Top, _) | (_, Sign::Top) |
            (Sign::LEqZero, Sign::NotZero) | (Sign::GEqZero, Sign::NotZero) => Sign::Top,
            (Sign::LZero, Sign::GEqZero) | (Sign::GZero, Sign::LEqZero) |
            (Sign::LZero, Sign::GZero) | (Sign::GZero, Sign::LZero) => Sign::LZero,
            (Sign::GEqZero, Sign::LZero) | (Sign::LEqZero, Sign::GEqZero) |
            (Sign::GEqZero, Sign::LEqZero) | (Sign::LEqZero, Sign::GZero) => Sign::LEqZero,
            (Sign::GEqZero, Sign::GEqZero) | (Sign::GEqZero, Sign::GZero) |
            (Sign::LEqZero, Sign::LZero) | (Sign::LEqZero, Sign::LEqZero) => Sign::GEqZero,

            (Sign::LZero, Sign::LEqZero) | (Sign::LZero, Sign::LZero) |
            (Sign::GZero, Sign::GEqZero) | (Sign::GZero, Sign::GZero) => Sign::GZero,
        }
    }
}

pub mod concretization;
pub mod interpretation;

#[cfg(test)]
mod tests;