//! Signs interpretation module
use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::traits::{AbstractArithmeticInterpreter, AbstractBooleanInterpreter, AbstractInterpreter};

use super::*;

impl AbstractArithmeticInterpreter<Sign> for Interpreter {}

impl AbstractBooleanInterpreter<Sign> for Interpreter {}

impl AbstractInterpreter<Sign> for Interpreter {}