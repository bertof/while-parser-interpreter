use crate::domains::expressions::stmt::prelude::*;
use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::traits::{AbstractArithmeticInterpreter, AbstractBooleanInterpreter, AbstractInterpreter};

use super::*;

#[cfg(test)]
mod prints {
    use super::*;

    #[test]
    fn test_inf_limit_print() {
        use super::*;
        init_log_test();

        for (k, v) in vec![
            ("Inf", InfLimit::Inf),
            ("Val(0)", InfLimit::Val(0))] {
            debug!("Print {:<20}  Display: {:<6}  Debug: {:<10}", k, format!("{}", v), format!("{:?}", v));
        }
    }

    #[test]
    fn test_sup_limit_print() {
        use super::*;
        init_log_test();

        for (k, v) in vec![
            ("Inf", SupLimit::Inf),
            ("Val(0)", SupLimit::Val(0))] {
            debug!("Print {:<20}  Display: {:<6}  Debug: {:<10}", k, format!("{}", v), format!("{:?}", v));
        }
    }

    #[test]
    fn test_interval_print() {
        use super::*;

        init_log_test();

        for (k, v) in vec![
            ("Int(Inf, Val(5))", Interval::Int(InfLimit::Inf, SupLimit::Val(5))),
            ("Int(Val(0), Val(0))", Interval::Int(InfLimit::Val(0), SupLimit::Val(0))),
            ("Int(Val(-5), Inf)", Interval::Int(InfLimit::Val(-5), SupLimit::Inf)),
            ("Int(Inf, Inf)", Interval::Int(InfLimit::Inf, SupLimit::Inf)),
            ("Bottom", Interval::Bottom)] {
            debug!("Print {:<20}  Display: {:<6}  Debug: {:<10}", k, format!("{}", v), format!("{:?}", v));
        }
    }
}

#[cfg(test)]
mod tables {
    use std::ops::Not;

    use crate::utilities::table::{generate_ab_table, generate_f_table};

    use super::*;

    const VALUES: [Interval<i64>; 11] = [
        Interval::Bottom,
        Interval::Int(InfLimit::Inf, SupLimit::Val(5)),
        Interval::Int(InfLimit::Val(-5), SupLimit::Val(-1)),
        Interval::Int(InfLimit::Val(-5), SupLimit::Val(1)),
        Interval::Int(InfLimit::Val(-4), SupLimit::Val(1)),
        Interval::Int(InfLimit::Val(0), SupLimit::Val(0)),
        Interval::Int(InfLimit::Val(-1), SupLimit::Val(5)),
        Interval::Int(InfLimit::Val(1), SupLimit::Val(5)),
        Interval::Int(InfLimit::Val(1), SupLimit::Val(4)),
        Interval::Int(InfLimit::Val(5), SupLimit::Inf),
        Interval::Int(InfLimit::Inf, SupLimit::Inf)
    ];

    #[test]
    fn test_inf_limit_partial_order() {
        init_log_test();

        debug!("PartialOrder <\n{}", generate_ab_table(
            &[InfLimit::Inf, InfLimit::Val(-5), InfLimit::Val(0), InfLimit::Val(5)],
            &|a, b| format!("{:?}", a.partial_cmp(&b)), "partial_cmp"));
    }

    #[test]
    fn test_inf_limit_neg() {
        init_log_test();

        debug!("ops::Neg neg\n{}", generate_f_table(
            &[InfLimit::Inf, InfLimit::Val(-5), InfLimit::Val(0), InfLimit::Val(5)],
            &|a| -a.clone(), "neg"));
    }

    #[test]
    fn test_sup_limit_partial_order() {
        init_log_test();

        debug!("PartialOrder partial_cmp\n{}", generate_ab_table(
            &[SupLimit::Val(-5), SupLimit::Val(0), SupLimit::Val(5), SupLimit::Inf],
            &|a, b| format!("{:?}", a.partial_cmp(&b)), "partial_cmp"));
    }

    #[test]
    fn test_sup_limit_neg() {
        init_log_test();

        debug!("ops::Neg neg\n{}", generate_f_table(
            &[SupLimit::Val(-5), SupLimit::Val(0), SupLimit::Val(5), SupLimit::Inf],
            &|a| -a.clone(), "neg"));
    }

    #[test]
    fn test_interval_partial_order() {
        init_log_test();


        debug!("PartialOrder\n{}", generate_ab_table(
            &VALUES, &|a, b| format!("{:?}", a.partial_cmp(b)), "part_cmp"));
    }

    #[test]
    fn test_interval_neg() {
        init_log_test();

        debug!("ops::Neg neg\n{}", generate_f_table(
            &VALUES, &|a| -a.clone(), "neg"));
    }

    #[test]
    fn test_interval_not() {
        init_log_test();

        debug!("ops::Neg neg\n{}", generate_f_table(
            &VALUES, &|a| a.clone().not(), "not"));
    }

    #[test]
    fn test_interval_add() {
        init_log_test();

        debug!("ops::Add add\n{}", generate_ab_table(
            &VALUES, &|a, b|
                a.clone() + b.clone(), "+"));
    }

    #[test]
    fn test_interval_sub() {
        init_log_test();

        debug!("ops::Sub sub\n{}", generate_ab_table(
            &VALUES, &|a, b|
                a.clone() - b.clone(), "-"));
    }

    #[test]
    fn test_interval_mul() {
        init_log_test();

        debug!("ops::Mul mul\n{}", generate_ab_table(
            &VALUES, &|a, b|
                a.clone() * b.clone(), "*"));
    }

    #[test]
    fn test_interval_div() {
        init_log_test();

        debug!("ops::Div div\n{}", generate_ab_table(
            &VALUES, &|a, b|
                a.clone() / b.clone(), "/"));
    }

    #[test]
    fn test_constant_unite() {
        init_log_test();

        debug!("AbstractValue unite\n{}", generate_ab_table(
            &VALUES, &|a, b|
                Interval::lub(&Some(a.clone()), &Some(b.clone())), "∪"));
    }

    #[test]
    fn test_constant_intersect() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES, &|a, b|
                Interval::glb(&Some(a.clone()), &Some(b.clone())), "∩");
        debug!("AbstractValue intersect\n{}", table);
    }

    #[test]
    fn test_constant_widen() {
        init_log_test();

        let table = generate_ab_table(
            &VALUES, &|a, b|
                Interval::widen(&Some(a.clone()), &Some(b.clone())), "∇");
        debug!("AbstractValue widen\n{}", table);
    }
}

#[cfg(test)]
mod a_eval {
    use super::*;

    #[test]
    fn test_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = val(15).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(15));
        Ok(())
    }

    #[test]
    fn test_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 12).into();
        let e: AExpr<Interval<i64>> = var("x");
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(12));
        Ok(())
    }

    #[test]
    fn test_eval_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = sum(val(1), val(0)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = dif(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(-1));
        Ok(())
    }

    #[test]
    fn test_eval_mul() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = mul(val(2), val(3)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(6));
        Ok(())
    }

    #[test]
    fn test_eval_div() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = div(val(1), val(2)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from(0));
        Ok(())
    }

    #[test]
    /// Test range valber generation.
    fn test_range() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: AExpr<Interval<i64>> = range(val(15), val(30)).into();
        assert_eq!(Interpreter::a_eval(&e, &s)?, Interval::from((15, 30)));
        Ok(())
    }

    #[test]
    fn test_range_extra() {
        init_log_test();

        let s: AExpr<i64> = range(val(-5), val(1));
        let s1: AExpr<Interval<i64>> = s.into();
        let r = Interpreter::a_eval(&s1, &State::new()).unwrap();
        assert_eq!(r, Interval::from((-5, 1)))
    }
}

#[cfg(test)]
mod b_expr {
    use super::*;

    #[test]
    fn test_eval_tru() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = tru();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_fal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = fal();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(tru());
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(not(tru()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_not_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(not(not(fal())));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_and() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = and(tru(), fal());
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_and() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(and(tru(), fal()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_and_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(and(tru(), tru()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_or() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e = or(tru(), fal());
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(or(tru(), fal()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_t_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(or(tru(), tru()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_t() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(or(fal(), tru()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_or_f_f() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(or(fal(), fal()));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(val(1), val(1)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_range_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(range(val(-5), val(5)), range(val(0), val(10))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_range_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-5, 5)));
        let e: BExpr<Interval<i64>> = eq(var("x"), range(val(0), val(10))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 5)));
        Ok(())
    }

    #[test]
    fn test_eval_eq_range_var_true_edge() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-5, 0)));
        let e: BExpr<Interval<i64>> = eq(var("x"), range(val(0), val(10))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 0)));
        Ok(())
    }

    #[test]
    fn test_eval_eq_range_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-5, -1)));
        let e: BExpr<Interval<i64>> = eq(var("x"), range(val(0), val(10))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(val(1), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_range_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(range(val(-5), val(-1)), range(val(1), val(5))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = eq(var("x"), val(1)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = eq(var("x"), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = eq(val(1), var("x")).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = eq(val(2), var("x")).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(val(2), sum(val(1), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(sum(val(1), val(1)), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(val(0), dif(val(1), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_mul_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(mul(val(1), val(0)), val(0)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_eq_div_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = eq(div(val(10), val(2)), val(5)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(val(1), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_true_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((0, 1)));
        let e: BExpr<Interval<i64>> = not(eq(var("x"), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(0));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_true_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((0, 1)));
        let e: BExpr<Interval<i64>> = not(eq(val(1), var("x"))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(0));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_false_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(var("x"), val(Interval::from((0, 1)))));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_false_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(val(Interval::from((0, 1))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_partial_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((0, 1)));
        let e: BExpr<Interval<i64>> = not(eq(var("x"), val(Interval::from((-1, 0)))));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_partial_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-1, 0)));
        let e: BExpr<Interval<i64>> = not(eq(val(Interval::from((0, 1))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(-1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_partial_range_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-1, 1)));
        let e: BExpr<Interval<i64>> = not(eq(val(Interval::from((0, 0))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((-1, 1)));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_range_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(range(val(-5), val(-1)), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_range_partial_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(range(val(-5), val(1)), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_range_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(range(val(-5), val(1)), range(val(-5), val(1)))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(val(1), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(var("x"), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_val_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(var("x"), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 1).into();
        let e: BExpr<Interval<i64>> = eq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        assert_eq!(r.get("y").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Interval<i64>> = eq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        assert_eq!(r.get("y").unwrap(), &Interval::from(2));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Interval<i64>> = not(eq(var("x"), var("y")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        assert_eq!(r.get("y").unwrap(), &Interval::from(2));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_var_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(var("x"), var("y")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        assert_eq!(r.get("y").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(val(1), var("x"))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_var_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(eq(val(2), var("x"))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(val(2), sum(val(1), val(1)))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_sum_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(sum(val(1), val(1)), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_val_dif() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(val(0), dif(val(1), val(1)))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_eq_dif_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(eq(dif(val(1), val(1)), val(0))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = leq(val(1), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 2).into();
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_var_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((0, 1))).set("y", Interval::from((2, 3)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_var_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((0, 2))).set("y", Interval::from((2, 3)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_var_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((0, 3))).set("y", Interval::from((2, 3)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 3)));
        assert_eq!(r.get("y").unwrap(), &Interval::from((2, 3)));
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_var_4() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((0, 1))).set("y", Interval::from((0, 3)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 1)));
        assert_eq!(r.get("y").unwrap(), &Interval::from((0, 3)));
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_var_5() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((0, 1))).set("y", Interval::from((-1, 3)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 1)));
        assert_eq!(r.get("y").unwrap(), &Interval::from((0, 3)));
        Ok(())
    }


    #[test]
    fn test_eval_leq_less_range_true() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", Interval::from((1, 2))).set("y", Interval::from((3, 4)));
        let e: BExpr<Interval<i64>> = leq(var("x"), var("y"));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_partial() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = leq(range(val(1), val(5)), range(val(3), val(4))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_range_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = leq(range(val(1), val(5)), range(val(-5), val(0))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }


    #[test]
    fn test_eval_leq_less_val_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 2).into();
        let e: BExpr<Interval<i64>> = leq(val(1), var("x")).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_less_var_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 0).into();
        let e: BExpr<Interval<i64>> = leq(var("x"), val(1)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = leq(val(2), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = leq(val(3), val(2)).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(1), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(2), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(3), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(1), val(2))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(1), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_val_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new();
        let e: BExpr<Interval<i64>> = not(leq(val(2), val(1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_less_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new()
            .set("x", 1)
            .set("y", 2).into();
        let e: BExpr<Interval<i64>> = not(leq(var("x"), var("y")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_equal_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).set("y", 1).into();
        let e: BExpr<Interval<i64>> = not(leq(var("x"), var("y")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_greater_var_var() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 2).set("y", 1).into();
        let e: BExpr<Interval<i64>> = not(leq(var("x"), var("y")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r, s);
        Ok(())
    }


    #[test]
    fn test_eval_not_leq_var_val_true_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((0, 1)));
        let e: BExpr<Interval<i64>> = not(leq(var("x"), val(-1))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from((0, 1)));
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_true_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-1, 1)));
        let e: BExpr<Interval<i64>> = not(leq(val(0), var("x"))).into();
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(-1));
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_false_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(leq(var("x"), val(Interval::from((0, 1)))));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_false_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", 1).into();
        let e: BExpr<Interval<i64>> = not(leq(val(Interval::from((0, 1))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(r.is_bottom());
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_partial_range_1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((0, 1)));
        let e: BExpr<Interval<i64>> = not(leq(var("x"), val(Interval::from((-1, 0)))));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_partial_range_2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-1, 0)));
        let e: BExpr<Interval<i64>> = not(leq(val(Interval::from((0, 1))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(-1));
        Ok(())
    }

    #[test]
    fn test_eval_not_leq_var_val_partial_range_3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::from((-1, 1)));
        let e: BExpr<Interval<i64>> = not(leq(val(Interval::from((0, 0))), var("x")));
        debug!("{} {}", e, s);
        let r = Interpreter::b_filter(&e, &s)?;
        debug!("{}", r);
        assert!(!r.is_bottom());
        assert_eq!(r.get("x").unwrap(), &Interval::from(-1));
        Ok(())
    }
}

#[cfg(test)]
mod s_expr {
    use super::*;

    #[test]
    fn test_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ass("x", val(Interval::from((-5, 5))));
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((-5, 5)));
        Ok(())
    }

    #[test]
    fn test_double_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", Interval::from((-5, 5)));
        let e = ass("x", val(Interval::from(10)));
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(10));
        Ok(())
    }

    #[test]
    fn test_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new()
            .set("x", Interval::from((-5, 5))).set("y", Interval::from(12));
        let e = skip();
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", Interval::from((-5, 5)));
        let e = conc(skip(), skip());
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert!(r.state() == p.state());
        Ok(())
    }

    #[test]
    fn test_conc_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(Interval::from(2))),
            ass("y", val(Interval::from(3))));
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(2));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from(3));
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_val() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(ass("x", val(2)), ass("x", val(3)));
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(3));
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_same() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(ass("x", val(2)), ass("x", var("x")));
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(2));
        Ok(())
    }

    #[test]
    fn test_conc_ass_collision_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", val(Interval::from((-10, 10)))),
            ass("x", sum(var("x"), val(Interval::from(1)))),
        );
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((-9, 11)));
        Ok(())
    }

    #[test]
    fn test_ite_true_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            tru(),
            ass("x", val(Interval::from((-5, 5)))),
            ass("x", val(Interval::from(-3))),
        );
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((-5, 5)));
        Ok(())
    }

    #[test]
    fn test_ite_false_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = ite(
            fal(),
            ass("x", val(Interval::from((-5, 5)))),
            ass("x", val(Interval::from(-3))),
        );
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(-3));
        Ok(())
    }

    #[test]
    fn test_ite_range_multi_branch() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = conc(
            ass("x", range(val(0), val(1))),
            ite(
                eq(var("x"), range(val(-1), val(0))),
                ass("y", val(2)),
                ass("y", val(-2)),
            ),
        );
        debug!("{}", e);
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((0, 1)));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from((-2, 2)));
        Ok(())
    }

    #[test]
    fn test_ite_top_condition() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<Interval<i64>> = State::new().set("x", Interval::top());
        let e: SExpr<Interval<i64>> = ite(
            eq(var("x"), val(2)),
            ass("y", val(1)),
            ass("y", val(2)),
        ).into();
        let p = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::top());
        assert_eq!(r.state().get("y").unwrap(), &Interval::from((1, 2)));
        Ok(())
    }

    #[test]
    fn test_while_loop_false() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(fal(), ass("x", val(1)));
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x"), None);
        Ok(())
    }

    #[test]
    fn test_while_loop_true_same_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let e = wd(tru(), ass("x", val(1)));
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    #[ignore]
    fn test_while_loop_true_different_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0);
        let e = wd(tru(), ass("x", sum(var("x"), val(1))));
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::top());
        Ok(())
    }

    #[test]
    fn test_while_loop_true_different_ass_convergence() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", Interval::from((Val::V(0), Val::Inf)));
        let e = wd(tru(), ass("x", sum(var("x"), val(1)))).into();
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((Val::V(0), Val::Inf)));
        Ok(())
    }

    #[test]
    fn test_while_loop_interval() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        let e = wd(
            eq(var("x"), val(1)),
            ass("x", val(1)),
        );
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(1));
        Ok(())
    }

    #[test]
    fn test_while_loop_var_change() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1).set("y", 5);
        let e = wd(
            eq(var("x"), val(1)),
            ass("x", val(2)),
        );
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((Val::V(2), Val::Inf)));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from(5));
        Ok(())
    }

    #[test]
    fn test_while_loop_until() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            not(eq(var("x"), var("y"))),
            ass("x", sum(val(1), var("x"))),
        );
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(5));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from(5));
        Ok(())
    }

    #[test]
    fn while_join() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new()
            .set("x", Interval::from((-5, 5))).set("y", Interval::from(5));
        let e = wd(
            leq(var("x"), val(0)),
            conc(
                ass("x", sum(var("x"), val(1))),
                ass("y", sum(var("y"), val(1))),
            ),
        ).into();
        let p: AbstractConfig<Interval<i64>> = abstract_program(e, s);
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((1, 5)));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from((Val::V(5), Val::Inf)));
        Ok(())
    }

    #[test]
    fn test_while_nested() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            wd(
                eq(var("x"), val(1)),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from(0));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from(5));
        Ok(())
    }

    #[test]
    fn test_while_nested_finite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 0).set("y", 5);
        let e = wd(
            eq(var("x"), val(0)),
            conc(
                wd(
                    eq(var("x"), val(1)),
                    ass("x", var("y"))),
                ass("x", sum(var("x"), val(1))),
            ),
        );
        let p: AbstractConfig<Interval<i64>> = program(e, s).into();
        debug!("{}", p);
        let r = Interpreter::run(&p)?;
        debug!("{}", r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((Val::V(1), Val::Inf)));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from(5));
        Ok(())
    }
}

#[cfg(test)]
mod programs {
    use crate::interpreter::examples::*;

    use super::*;

    #[test]
    fn test_factorial() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Interval<i64>> = program_factorial_5().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}\n", &r);
        assert!(r.is_terminated());
        assert!(!r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((Val::NInf, Val::V(1))));
        assert_eq!(r.state().get("y").unwrap(), &Interval::from((Val::V(1), Val::Inf)));
        Ok(())
    }


    #[test]
    /// "while true do skip" test. It's run only if the environment variable "REPEAT" is set and it will
    /// loop forever.
    fn test_infinite_loop() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Interval<i64>> = infinite_loop().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        Ok(())
    }

    #[test]
    /// "while true ++i" test. It's run only if the environment variable "REPEAT" is set and it will
    /// loop forever.
    fn test_infinite_increment() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p: AbstractConfig<Interval<i64>> = infinite_increment().into();
        debug!("{}", &p);
        let r = Interpreter::run(&p)?;
        debug!("{}", &r);
        assert!(r.is_terminated());
        assert!(r.state().is_bottom());
        assert_eq!(r.state().get("x").unwrap(), &Interval::from((Val::V(1), Val::Inf)));
        Ok(())
    }
}
