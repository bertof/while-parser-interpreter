//! Intervals interpretation module
use crate::domains::expressions::stmt::arithmetic::AExpr;
use crate::domains::expressions::stmt::prelude::*;
use crate::interpreter::abstracts::Interpreter;
use crate::interpreter::traits::{AbstractArithmeticInterpreter, AbstractBooleanInterpreter, AbstractInterpreter};

use super::*;

impl<V> AbstractArithmeticInterpreter<Interval<V>> for Interpreter where V: Value + Ord + HasSteps {}

impl<V> AbstractBooleanInterpreter<Interval<V>> for Interpreter where V: Value + Ord + HasSteps {
    fn b_filter_eq(exp1: &AExpr<Interval<V>>, exp2: &AExpr<Interval<V>>, state: &State<Interval<V>>) -> Result<State<Interval<V>>, Box<dyn Error>> {
        let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        let intersection = Interval::glb(
            &Some(ev_exp1), &Some(ev_exp2));

        Ok(match intersection {
            Interval::Bottom => state.to_bottom(),
            _ => match (exp1, exp2) {
                (AExpr::Var(s1), AExpr::Var(s2)) =>
                    state.set(&s1, intersection.clone()).set(&s2, intersection),
                (AExpr::Var(s), _) | (_, AExpr::Var(s)) =>
                    state.set(&s, intersection),
                _ => state.clone()
            }
        })
    }

    fn b_filter_not_eq(exp1: &AExpr<Interval<V>>, exp2: &AExpr<Interval<V>>, state: &State<Interval<V>>) -> Result<State<Interval<V>>, Box<dyn Error>> {
        let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        Ok(match (ev_exp1.clone().try_into().ok(), ev_exp2.clone().try_into().ok()) {
            (Some((i1, s1)), Some((i2, s2))) => {
                let i_int_1 = Interval::glb(
                    &Some(match i1 {
                        Val::V(i1v) => Interval::from((Val::NInf, Val::V(i1v.prev()))),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp2.clone()),
                );
                let s_int_1 = Interval::glb(
                    &Some(match s1 {
                        Val::V(s1v) => Interval::from((Val::V(s1v.next()), Val::Inf)),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp2),
                );

                let i_int_2 = Interval::glb(
                    &Some(match i2 {
                        Val::V(i2v) => Interval::from((Val::NInf, Val::V(i2v.prev()))),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp1.clone()),
                );
                let s_int_2 = Interval::glb(
                    &Some(match s2 {
                        Val::V(s2v) => Interval::from((Val::V(s2v.next()), Val::Inf)),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp1),
                );

                let new_1 = Interval::lub(&Some(i_int_2), &Some(s_int_2));
                let new_2 = Interval::lub(&Some(i_int_1), &Some(s_int_1));

                match (exp1, exp2) {
                    (AExpr::Var(v1), AExpr::Var(v2)) =>
                        if
                        new_1.is_bottom() || new_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v1, new_1).set(v2, new_2) },
                    (AExpr::Var(v1), _) =>
                        if new_1.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v1, new_1) },
                    (_, AExpr::Var(v2)) =>
                        if new_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v2, new_2) },
                    _ if new_1.is_bottom() && new_2.is_bottom() => state.to_bottom(),
                    _ => state.clone()
                }
            }
            _ => state.to_bottom()
        })
    }

    fn b_filter_leq(exp1: &AExpr<Interval<V>>, exp2: &AExpr<Interval<V>>, state: &State<Interval<V>>) -> Result<State<Interval<V>>, Box<dyn Error>> {
        let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        Ok(match (ev_exp1.clone().try_into().ok(), ev_exp2.clone().try_into().ok()) {
            (Some((i1, _)), Some((_, s2))) => {
                let int_1 = Interval::glb(
                    &Some(match s2 {
                        Val::V(s2v) => Interval::from((Val::NInf, Val::V(s2v))),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp1),
                );
                let int_2 = Interval::glb(
                    &Some(match i1 {
                        Val::V(i1v) => Interval::from((Val::V(i1v), Val::Inf)),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp2),
                );

                match (exp1, exp2) {
                    _ if int_1.is_bottom() && int_2.is_bottom() => state.to_bottom(),
                    (AExpr::Var(v1), AExpr::Var(v2)) =>
                        if int_1.is_bottom() || int_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v1, int_1).set(v2, int_2) },
                    (AExpr::Var(v), _) =>
                        if int_1.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v, int_1) },
                    (_, AExpr::Var(v)) =>
                        if int_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v, int_2) },
                    _ => state.clone()
                }
            }
            _ => state.to_bottom()
        })
    }

    fn b_filter_not_leq(exp1: &AExpr<Interval<V>>, exp2: &AExpr<Interval<V>>, state: &State<Interval<V>>) -> Result<State<Interval<V>>, Box<dyn Error>> {
        /*let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        Ok(match &ev_exp2 {
            Interval::Bottom => state.to_bottom(),
            Interval::Int(i_limit, _) => {
                let i_val: Val<V> = i_limit.clone().into();
                let i_inf = Interval::from((Val::NInf, i_val.clone()));
                let i_sup = Interval::from((i_val, Val::Inf));
                let intersect_inf = Interval::intersect(&Some(ev_exp1), &Some(i_inf.not()));
                let intersect_sup = Interval::intersect(&Some(ev_exp2), &Some(i_sup.not()));

                match intersect_inf {
                    Interval::Bottom => state.to_bottom(),
                    _ => match (exp1, exp2) {
                        (AExpr::Var(s1), AExpr::Var(s2)) =>
                            state.set(&s1, intersect_inf).set(&s2, intersect_sup),
                        (AExpr::Var(s), _) => state.set(&s, intersect_inf),
                        (_, AExpr::Var(s)) => state.set(&s, intersect_sup),
                        _ => state.clone()
                    }
                }
            }
        })*/

        let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        Ok(match (ev_exp1.clone().try_into().ok(), ev_exp2.clone().try_into().ok()) {
            (Some((i1, _)), Some((_, s2))) => {
                let int_1 = Interval::glb(
                    &Some(match s2 {
                        Val::V(s2v) => Interval::from((Val::V(s2v.next()), Val::Inf)),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp1),
                );
                let int_2 = Interval::glb(
                    &Some(match i1 {
                        Val::V(i1v) => Interval::from((Val::NInf, Val::V(i1v.prev()))),
                        _ => Interval::Bottom,
                    }),
                    &Some(ev_exp2),
                );

                match (exp1, exp2) {
                    _ if int_1.is_bottom() && int_2.is_bottom() => state.to_bottom(),
                    (AExpr::Var(v1), AExpr::Var(v2)) =>
                        if int_1.is_bottom() || int_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v1, int_1).set(v2, int_2) },
                    (AExpr::Var(v), _) =>
                        if int_1.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v, int_1) },
                    (_, AExpr::Var(v)) =>
                        if int_2.is_bottom() {
                            state.to_bottom()
                        } else { state.set(v, int_2) },
                    _ => state.clone()
                }
            }
            _ => state.to_bottom()
        })
    }
}

impl<V> AbstractInterpreter<Interval<V>> for Interpreter where V: Value + Ord + HasSteps {}