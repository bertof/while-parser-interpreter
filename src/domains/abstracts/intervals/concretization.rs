//! Intervals concretization module
use crate::domains::expressions::stmt::prelude::*;
use crate::domains::traits::{Abstractable, Concretizable, Concretization, GaloisConnection};

use super::*;

impl Concretizable<i64> for Interval<i64> {}

impl<V> Abstractable<Interval<V>> for V where V: Value + Ord + HasSteps {}

impl GaloisConnection<Interval<i64>, i64> for Interval<i64> {}

impl Into<Concretization<i64>> for Interval<i64> {
    fn into(self) -> Concretization<i64> {
        match self {
            Interval::Int(InfLimit::Inf, SupLimit::Inf) => Concretization::Top,
            Interval::Int(InfLimit::Inf, SupLimit::Val(r)) =>
                Concretization::Val(Box::new((-r..).map(|v| -v))),
            Interval::Int(InfLimit::Val(l), SupLimit::Inf) =>
                Concretization::Val(Box::new(l..)),
            Interval::Int(InfLimit::Val(l), SupLimit::Val(r)) =>
                Concretization::Val(Box::new(l..=r)),
            Interval::Bottom => Concretization::Bottom,
        }
    }
}

impl<V> From<V> for InfLimit<V> where V: Value + Ord + HasSteps {
    fn from(val: V) -> Self {
        InfLimit::Val(val)
    }
}

impl<V> From<V> for SupLimit<V> where V: Value + Ord + HasSteps {
    fn from(val: V) -> Self {
        SupLimit::Val(val)
    }
}

impl<V> From<V> for Interval<V> where V: Value + Ord + HasSteps {
    fn from(val: V) -> Self {
        Interval::from((Val::V(val.clone()), Val::V(val)))
    }
}

impl<V> From<(V, V)> for Interval<V> where V: Value + Ord + HasSteps {
    fn from(values: (V, V)) -> Self {
        Interval::from((Val::V(values.0), Val::V(values.1)))
    }
}

impl<V> Into<AExpr<Interval<V>>> for AExpr<V> where V: Value + Ord + HasSteps {
    fn into(self) -> AExpr<Interval<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<BExpr<Interval<V>>> for BExpr<V> where V: Value + Ord + HasSteps {
    fn into(self) -> BExpr<Interval<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<SExpr<Interval<V>>> for SExpr<V> where V: Value + Ord + HasSteps {
    fn into(self) -> SExpr<Interval<V>> {
        self.apply(&|v| v.clone().into())
    }
}

impl<V> Into<State<Interval<V>>> for State<V> where V: Value + Ord + HasSteps {
    fn into(self) -> State<Interval<V>> {
        self.to_abstract(&|v| v.clone().into())
    }
}

impl<V> Into<AbstractConfig<Interval<V>>> for Config<V> where V: Value + Ord + HasSteps {
    fn into(self) -> AbstractConfig<Interval<V>> {
        let e = self.expression().cloned().map(|e| e.into());
        let s = self.state().keys()
            .iter()
            .cloned()
            .filter_map(|k| self.state().get(&k).cloned().map(|v| (k, v.into())))
            .collect_vec();
        if let Some(e) = e {
            AbstractConfig::Program(e, State::from_value_vec(s))
        } else {
            AbstractConfig::Ts(State::from_value_vec(s))
        }
    }
}