//! Intervals abstract domain module
use std::{cmp::Ordering, convert::TryInto, fmt, ops};

use itertools::{iproduct, Itertools};

use crate::data::value::{AbstractValue, Value};
use crate::domains::expressions::stmt::prelude::Error;

/// Trait to represent a digital generic domain.
///
/// This trait is required to easily define outer ranges from intervals and to get if a value
/// is positive or negative.
pub trait HasSteps: Value {
    /// Return the value that nullifies the multiplication and splits between positive and negative
    /// values.
    fn zero() -> Self;
    /// Returns the value immediately after.
    fn next(&self) -> Self;
    /// Returns the value immediately before.
    fn prev(&self) -> Self;
}

impl HasSteps for i64 {
    fn zero() -> Self {
        0
    }

    fn next(&self) -> Self {
        self + 1
    }

    fn prev(&self) -> Self {
        *self - 1
    }
}

/// Error during conversion between a generic value to a limit.
#[derive(Debug)]
struct IntervalConversionError {
    details: String
}

impl IntervalConversionError {
    fn new(msg: &str) -> IntervalConversionError {
        IntervalConversionError { details: msg.to_string() }
    }
}

impl fmt::Display for IntervalConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for IntervalConversionError {
    fn description(&self) -> &str {
        &self.details
    }
}

/// Value that represent a generic value domain united to minus infinite.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum InfLimit<V> where V: Value {
    Inf,
    Val(V),
}

impl<V> fmt::Display for InfLimit<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            InfLimit::Inf => write!(f, "-∞"),
            InfLimit::Val(v) => write!(f, "{}", v),
        }
    }
}

impl<V> ops::Neg for InfLimit<V> where V: Value {
    type Output = SupLimit<V>;

    fn neg(self) -> Self::Output {
        match self {
            InfLimit::Inf => SupLimit::Inf,
            InfLimit::Val(v) => SupLimit::Val(-v),
        }
    }
}

impl<V> PartialOrd for InfLimit<V> where V: Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (InfLimit::Inf, InfLimit::Inf) => Some(Ordering::Equal),
            (InfLimit::Inf, InfLimit::Val(_)) => Some(Ordering::Less),
            (InfLimit::Val(_), InfLimit::Inf) => Some(Ordering::Greater),
            (InfLimit::Val(v1), InfLimit::Val(v2)) => v1.partial_cmp(v2)
        }
    }
}

/// Value that represent a generic value domain united to plus infinite.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum SupLimit<V> where V: Value {
    Inf,
    Val(V),
}

impl<V> fmt::Display for SupLimit<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SupLimit::Inf => write!(f, "∞"),
            SupLimit::Val(v) => write!(f, "{}", v),
        }
    }
}

impl<V> ops::Neg for SupLimit<V> where V: Value {
    type Output = InfLimit<V>;

    fn neg(self) -> Self::Output {
        match self {
            SupLimit::Inf => InfLimit::Inf,
            SupLimit::Val(v) => InfLimit::Val(-v),
        }
    }
}

impl<V> PartialOrd for SupLimit<V> where V: Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (SupLimit::Inf, SupLimit::Inf) => Some(Ordering::Equal),
            (SupLimit::Inf, SupLimit::Val(_)) => Some(Ordering::Greater),
            (SupLimit::Val(_), SupLimit::Inf) => Some(Ordering::Less),
            (SupLimit::Val(v1), SupLimit::Val(v2)) => v1.partial_cmp(v2)
        }
    }
}

/// Value that represent a generic value domain united to plus and minus infinite.
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub enum Val<V> where V: Value + Ord {
    NInf,
    V(V),
    Inf,
}

impl<V> From<InfLimit<V>> for Val<V> where V: Value + Ord {
    fn from(v: InfLimit<V>) -> Self {
        match v {
            InfLimit::Inf => Val::NInf,
            InfLimit::Val(v) => Val::V(v),
        }
    }
}

impl<V> From<SupLimit<V>> for Val<V> where V: Value + Ord {
    fn from(v: SupLimit<V>) -> Self {
        match v {
            SupLimit::Inf => Val::Inf,
            SupLimit::Val(v) => Val::V(v),
        }
    }
}

impl<V> From<(Val<V>, Val<V>)> for Interval<V> where V: Value + Ord {
    fn from(value: (Val<V>, Val<V>)) -> Self {
        let i = value.0.clone().min(value.1.clone());
        let s = value.0.max(value.1);
        match (i, s) {
            (Val::V(iv), Val::V(sv)) if iv > sv => Interval::Bottom,
            (i, s) => {
                let l = match i {
                    Val::NInf => InfLimit::Inf,
                    Val::V(lv) => InfLimit::Val(lv),
                    Val::Inf => { return Interval::Bottom; }
                };
                let r = match s {
                    Val::NInf => { return Interval::Bottom; }
                    Val::V(rv) => SupLimit::Val(rv),
                    Val::Inf => SupLimit::Inf,
                };
                Interval::Int(l, r)
            }
        }
    }
}

impl<V> TryInto<(Val<V>, Val<V>)> for Interval<V> where V: Value + Ord {
    type Error = Box<dyn Error>;

    fn try_into(self) -> Result<(Val<V>, Val<V>), Self::Error> {
        match self {
            Interval::Int(a, b) => Ok((a.into(), b.into())),
            Interval::Bottom => Err(Box::new(IntervalConversionError::new("Invalid interval"))),
        }
    }
}

impl<V> fmt::Display for Val<V> where V: Value + Ord {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Val::NInf => write!(f, "-∞"),
            Val::V(v) => write!(f, "{}", v),
            Val::Inf => write!(f, "∞")
        }
    }
}

impl<V> ops::Neg for Val<V> where V: Value + Ord {
    type Output = Self;

    fn neg(self) -> Self::Output {
        match self {
            Val::NInf => Val::Inf,
            Val::V(v) => Val::V(-v),
            Val::Inf => Val::NInf,
        }
    }
}

impl<V> ops::Add for Val<V> where V: Value + Ord {
    type Output = Option<Self>;

    fn add(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Val::Inf, Val::NInf) | (Val::NInf, Val::Inf) => None,
            (Val::Inf, _) | (_, Val::Inf) => Some(Val::Inf),
            (Val::NInf, _) | (_, Val::NInf) => Some(Val::NInf),
            (Val::V(a), Val::V(b)) => Some(Val::V(a + b))
        }
    }
}

impl<V> ops::Sub for Val<V> where V: Value + Ord {
    type Output = Option<Self>;

    fn sub(self, rhs: Self) -> Self::Output {
        self + (-rhs)
    }
}

impl<V> ops::Mul for Val<V> where V: Value + Ord + HasSteps {
    type Output = Option<Self>;

    fn mul(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Val::Inf, Val::NInf) | (Val::NInf, Val::Inf) => None,
            (Val::Inf, Val::Inf) | (Val::NInf, Val::NInf) => Some(Val::Inf),
            (Val::Inf, Val::V(v)) | (Val::V(v), Val::Inf) => if v < V::zero() {
                Some(Val::NInf)
            } else if v == V::zero() {
                Some(Val::V(V::zero()))
            } else {
                Some(Val::Inf)
            }
            (Val::NInf, Val::V(v)) | (Val::V(v), Val::NInf) => if v < V::zero() {
                Some(Val::Inf)
            } else if v == V::zero() {
                Some(Val::V(V::zero()))
            } else {
                Some(Val::NInf)
            }
            (Val::V(v1), Val::V(v2)) => Some(Val::V(v1 * v2))
        }
    }
}

impl<V> ops::Div for Val<V> where V: Value + Ord + HasSteps {
    type Output = Option<Self>;

    fn div(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Val::Inf, Val::NInf) | (Val::NInf, Val::Inf) => None,
            (Val::Inf, Val::Inf) | (Val::NInf, Val::NInf) => Some(Val::Inf),
            (Val::Inf, Val::V(v)) => if v < V::zero() {
                Some(Val::NInf)
            } else if v == V::zero() {
                None
            } else {
                Some(Val::Inf)
            }
            (Val::V(v), Val::Inf) => if v < V::zero() {
                Some(Val::NInf)
            } else if v == V::zero() {
                Some(Val::V(V::zero()))
            } else {
                Some(Val::Inf)
            }
            (Val::NInf, Val::V(v)) => if v < V::zero() {
                Some(Val::Inf)
            } else if v == V::zero() {
                None
            } else {
                Some(Val::NInf)
            }
            (Val::V(v), Val::NInf) => if v < V::zero() {
                Some(Val::Inf)
            } else if v == V::zero() {
                Some(Val::V(V::zero()))
            } else {
                Some(Val::NInf)
            }
            (Val::V(v1), Val::V(v2)) => if v2 == V::zero() {
                None
            } else if v1 == V::zero() {
                Some(Val::V(V::zero()))
            } else {
                Some(Val::V(v1 / v2))
            }
        }
    }
}

/// Interval abstract domain.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Interval<V> where V: Value {
    Int(InfLimit<V>, SupLimit<V>),
    Bottom,
}

impl<V> fmt::Display for Interval<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Interval::Int(l, r) => match (l, r) {
                (InfLimit::Inf, SupLimit::Inf) => write!(f, "⊤"),
                _ => write!(f, "[{},{}]", l, r)
            }
            Interval::Bottom => write!(f, "⊥")
        }
    }
}

impl<V> ops::Neg for Interval<V> where V: Value {
    type Output = Self;

    fn neg(self) -> Self::Output {
        match self {
            Interval::Bottom => Interval::Bottom,
            Interval::Int(l1, r1) => Interval::Int(r1.neg(), l1.neg())
        }
    }
}

impl<V> ops::Not for Interval<V> where V: Value + Ord + HasSteps {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            Interval::Int(InfLimit::Inf, SupLimit::Val(s)) =>
                Interval::Int(InfLimit::Val(s.next()), SupLimit::Inf),
            Interval::Int(InfLimit::Val(i), SupLimit::Inf) =>
                Interval::Int(InfLimit::Inf, SupLimit::Val(i.prev())),
            Interval::Int(InfLimit::Inf, SupLimit::Inf) => Interval::bottom(),
            Interval::Int(InfLimit::Val(_), SupLimit::Val(_)) |
            Interval::Bottom => Interval::top(),
        }
    }
}

impl<V> ops::Add for Interval<V> where V: Value + Ord {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        match (self.try_into(), rhs.try_into()) {
            (Err(_), _) | (_, Err(_)) => Interval::Bottom,
            (Ok((l1, r1)), Ok((l2, r2))) => {
                let (it1, it2) =
                    iproduct!(vec![l1,r1], vec![l2,r2]).filter_map(|(a, b)| a + b).tee();
                let vi = it1.min().unwrap();
                let vs = it2.max().unwrap();
                Interval::from((vi, vs))
            }
        }
    }
}

impl<V> ops::Sub for Interval<V> where V: Value + Ord {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        match (self.try_into(), rhs.try_into()) {
            (Err(_), _) | (_, Err(_)) => Interval::Bottom,
            (Ok((l1, r1)), Ok((l2, r2))) => {
                let (it1, it2) =
                    iproduct!(vec![l1,r1], vec![l2,r2]).filter_map(|(a, b)| a - b).tee();
                let vi = it1.min().unwrap();
                let vs = it2.max().unwrap();
                Interval::from((vi, vs))
            }
        }
    }
}

impl<V> ops::Mul for Interval<V> where V: Value + Ord + HasSteps {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        match (self.try_into(), rhs.try_into()) {
            (Err(_), _) | (_, Err(_)) => Interval::Bottom,
            (Ok((l1, r1)), Ok((l2, r2))) => {
                let (it1, it2) =
                    iproduct!(vec![l1,r1], vec![l2,r2]).filter_map(|(a, b)| a * b).tee();
                let vi = it1.min().unwrap();
                let vs = it2.max().unwrap();
                Interval::from((vi, vs))
            }
        }
    }
}

impl<V> ops::Div for Interval<V> where V: Value + Ord + HasSteps {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        match (self.try_into(), rhs.try_into()) {
            (Err(_), _) | (_, Err(_)) => Interval::Bottom,
            (Ok((l1, r1)), Ok((l2, r2))) => {
                let (it1, it2) =
                    iproduct!(vec![l1,r1], vec![l2,r2]).filter_map(|(a, b)| a / b).tee();
                let vi = match it1.min() {
                    Some(v) => v,
                    None => { return Interval::Bottom; }
                };
                let vs = it2.max().unwrap();
                Interval::from((vi, vs))
            }
        }
    }
}

impl<V> PartialOrd for Interval<V> where V: Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Interval::Bottom, Interval::Bottom) => Some(Ordering::Equal),
            (Interval::Int(InfLimit::Val(l1), SupLimit::Val(r1)), Interval::Int(InfLimit::Val(l2), SupLimit::Val(r2))) if l1 == l2 && l2 == r1 && r1 == r2 => Some(Ordering::Equal),

            (Interval::Int(_, SupLimit::Val(r)), Interval::Int(InfLimit::Val(l), _)) if r < l => Some(Ordering::Less),
            (Interval::Int(InfLimit::Val(l), _), Interval::Int(_, SupLimit::Val(r))) if l > r => Some(Ordering::Greater),
            _ => None
        }
    }
}

impl<V> Value for Interval<V> where V: Value + Ord + HasSteps {
    fn rnd_range(min: Self, max: Self) -> Self {
        if min > max {
            panic!("Invalid values, min should be smaller or equal to max. Got min = {} and max = {}", min, max)
        }
        Self::lub(&Some(min), &Some(max))
    }
}

impl<V> AbstractValue for Interval<V> where V: Value + Ord + HasSteps {
    fn top() -> Self {
        Interval::Int(InfLimit::Inf, SupLimit::Inf)
    }

    fn bottom() -> Self {
        Interval::Bottom
    }

    fn lub(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (
            s1.clone().and_then(|v| v.try_into().ok()),
            s2.clone().and_then(|v| v.try_into().ok())
        ) {
            (None, None) => Interval::Bottom,
            (None, Some((l, r))) |
            (Some((l, r)), None) =>
                Interval::from((l, r)),
            (Some((l1, r1)), Some((l2, r2))) => {
                Interval::from((l1.min(l2), r1.max(r2)))
            }
        }
    }

    fn glb(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (
            s1.clone().and_then(|v| v.try_into().ok()),
            s2.clone().and_then(|v| v.try_into().ok())
        ) {
            (None, None) | (None, Some(_)) | (Some(_), None) =>
                Interval::Bottom,
            (Some((l1, r1)), Some((l2, r2))) => {
                let l_max = l1.max(l2);
                let r_min = r1.min(r2);
                if l_max <= r_min { Interval::from((l_max, r_min)) } else { Interval::Bottom }
            }
        }
    }

    fn widen(s1: &Option<Self>, s2: &Option<Self>) -> Self {
        match (s1, s2) {
            (None, None) => Interval::Bottom,
            (Some(x), None) | (None, Some(x)) => x.clone(),
            (Some(v1), Some(v2)) => match (v1, v2) {
                (Interval::Bottom, Interval::Bottom) => Interval::Bottom,
                (Interval::Bottom, x) | (x, Interval::Bottom) => x.clone(),
                (v1, v2) => {
                    let ((i1, s1), (i2, s2)) =
                        (v1.clone().try_into().unwrap(), v2.clone().try_into().unwrap());
                    let i = if i1 <= i2 { i1 } else { Val::NInf };
                    let s = if s1 >= s2 { s1 } else { Val::Inf };
                    Interval::from((i, s))
                }
            }
        }
    }
}

pub mod concretization;
pub mod interpretation;

#[cfg(test)]
mod tests;