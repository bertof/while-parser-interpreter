//! Utility module for syntactic_categories
//!
//! Contains utility functions to simplify the writing of code containing objects of those classes.

pub use std::error::Error;

pub use crate::data::{
    config::*,
    state::*,
    value::*,
};
pub use crate::domains::expressions::stmt::{
    arithmetic::{AExpr, short::*},
    boolean::{BExpr, short::*},
    semantic::{SExpr, short::*},
};
pub use crate::utilities::logging::*;

/// Returns a program from its expression and an initial state
pub fn program<V: Value>(e: SExpr<V>, s: State<V>) -> Config<V> {
    Config::from((e, s))
}

/// Returns an abstract program from its expression and an initial abstract state
#[allow(dead_code)]
pub fn abstract_program<V: AbstractValue>(e: SExpr<V>, s: State<V>) -> AbstractConfig<V> {
    AbstractConfig::from((e, s))
}