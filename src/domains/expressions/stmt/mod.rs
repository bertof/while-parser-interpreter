//! Concrete semantic categories module for the While (stmt) language
//!
//! This module contains the components necessary for describing arithmetic and boolean expression
//! and statements used in the language While (stmt).

pub mod arithmetic;
pub mod boolean;
pub mod semantic;
pub mod prelude;