//! While language boolean syntactic categories

use std::fmt::{self, Debug, Display, Formatter};

pub use BExpr::*;
use short::*;

use crate::data::value::Value;
use crate::domains::expressions::stmt::arithmetic::AExpr;

pub mod short {
    use crate::data::value::Value;

    use super::*;

    /// Returns a True value
    pub fn tru<V: Value>() -> BExpr<V> { True }

    /// Returns a False value
    pub fn fal<V: Value>() -> BExpr<V> { False }

    /// Returns the negation of the given boolean expression
    pub fn not<V: Value>(e: BExpr<V>) -> BExpr<V> { Not(e.into()) }

    /// Returns the and of the given boolean expressions
    pub fn and<V: Value>(l: BExpr<V>, r: BExpr<V>) -> BExpr<V> { And(l.into(), r.into()) }

    /// Returns the or of the given boolean expressions
    pub fn or<V: Value>(l: BExpr<V>, r: BExpr<V>) -> BExpr<V> { Or(l.into(), r.into()) }

    /// Returns the equality check of the given arithmetic expressions
    pub fn eq<V: Value>(l: AExpr<V>, r: AExpr<V>) -> BExpr<V> { Eq(l.into(), r.into()) }

    /// Returns the less-equality check of the given arithmetic expressions
    pub fn leq<V: Value>(l: AExpr<V>, r: AExpr<V>) -> BExpr<V> { LEq(l.into(), r.into()) }

    /// Takes a boolean expression and returns the AParens containing it
    pub fn b_paren<V: Value>(e: BExpr<V>) -> BExpr<V> { BParen(e.into()) }
}

/// BExp describes the boolean expressions.
///
/// While lang defines them like the following:
///
/// *b* in Bexp
/// *b* ::= true | false | a1 = a2 | a1 <= a2 | not b | b1 and b2 | b1 or b2
///
/// where a1,a2 are arithmetic expressions.
/// BParen represents the round parenthesis in boolean expressions.
#[derive(Clone, Eq, PartialEq)]
pub enum BExpr<V> where V: Value {
    True,
    False,
    Not(Box<BExpr<V>>),
    BParen(Box<BExpr<V>>),
    And(Box<BExpr<V>>, Box<BExpr<V>>),
    Or(Box<BExpr<V>>, Box<BExpr<V>>),
    Eq(Box<AExpr<V>>, Box<AExpr<V>>),
    LEq(Box<AExpr<V>>, Box<AExpr<V>>),
}

impl<V> Display for BExpr<V> where V: Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use self::BExpr::*;
        match *self {
            True => write!(f, "tt"),
            False => write!(f, "ff"),
            Not(ref be) =>
                write!(f, "¬({})", be),
            And(ref be1, ref be2) =>
                write!(f, "{} ∧ {}", be1, be2),
            Or(ref be1, ref be2) =>
                write!(f, "{} ∨ {}", be1, be2),
            Eq(ref be1, ref be2) =>
                write!(f, "{} = {}", be1, be2),
            LEq(ref be1, ref be2) =>
                write!(f, "{} ≤ {}", be1, be2),
            BParen(ref expr) =>
                write!(f, "({})", expr),
        }
    }
}

impl<V> Debug for BExpr<V> where V: Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use self::BExpr::*;
        match self {
            True =>
                write!(f, "tt"),
            False =>
                write!(f, "ff"),
            Not(ref be) =>
                write!(f, "¬({:?})", be),
            And(ref be1, ref be2) =>
                write!(f, "({:?} ∧ {:?})", be1, be2),
            Or(ref be1, ref be2) =>
                write!(f, "({:?} ∨ {:?})", be1, be2),
            Eq(ref be1, ref be2) =>
                write!(f, "({:?} = {:?})", be1, be2),
            LEq(ref be1, ref be2) =>
                write!(f, "({:?} ≤ {:?})", be1, be2),
            BParen(ref expr) =>
                write!(f, "({:?})", expr),
        }
    }
}

impl<V> BExpr<V> where V: Value {
    pub fn apply<W>(&self, f: &dyn Fn(&V) -> W) -> BExpr<W> where W: Value {
        match self {
            BExpr::True => BExpr::True,
            BExpr::False => BExpr::False,
            BExpr::Not(v) => not(v.apply(f)),
            BExpr::BParen(e) => b_paren(e.apply(f)),
            BExpr::And(l, r) =>
                and(l.apply(f), r.apply(f)),
            BExpr::Or(l, r) =>
                or(l.apply(f), r.apply(f)),
            BExpr::Eq(l, r) =>
                eq(l.apply(f), r.apply(f)),
            BExpr::LEq(l, r) =>
                leq(l.apply(f), r.apply(f)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::domains::expressions::stmt::prelude::*;
    use crate::utilities::logging::*;

    #[test]
    /// Test Display and Debug implementation of BExpr
    fn test_bexp_print() {
        init_log_test();
        debug!("Print TRUE:\t\t{}\t\t{:?}", tru::<i64>(), tru::<i64>());
        debug!("Print FALSE:\t{}\t\t{:?}", fal::<i64>(), fal::<i64>());
        debug!("Print NOT:\t\t{}\t\t{:?}", not(tru::<i64>()), not(tru::<i64>()));
        debug!("Print AND:\t\t{}\t\t{:?}", and(tru::<i64>(), tru::<i64>()), and(tru::<i64>(), tru::<i64>()));
        debug!("Print EQ:\t\t{}\t{:?}", eq(val(1), val(1)), eq(val(1), val(1)));
        debug!("Print LEQ:\t\t{}\t{:?}", leq(val(1), val(1)), leq(val(1), val(1)));
    }

    #[test]
    fn test_bexp_apply() {
        init_log_test();
        assert_eq!(tru::<i64>(), tru::<i64>()
            .apply(&|v| v * 2));
        assert_eq!(fal::<i64>(), fal::<i64>()
            .apply(&|v| v * 2));
        assert_eq!(not(tru::<i64>()), not(tru::<i64>())
            .apply(&|v| v * 2));
        assert_eq!(and(tru::<i64>(), tru::<i64>()), and(tru::<i64>(), tru::<i64>())
            .apply(&|v| v * 2));
        assert_eq!(eq(val(2), val(4)), eq(val(1), val(2))
            .apply(&|v| v * 2));
        assert_eq!(leq(val(2), val(4)), leq(val(1), val(2))
            .apply(&|v| v * 2));
    }
}