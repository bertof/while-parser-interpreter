//! While language arithmetic syntactic categories

use std::cmp::Ordering;
use std::fmt::{self, Debug, Display, Formatter};

pub use AExpr::*;
use short::*;

use crate::data::value::Value;

pub mod short {
    use super::*;

    /// Takes a i64 and returns a Val with the given number
    pub fn val<V>(v: V) -> AExpr<V> where V: Value { Val(v) }

    /// Takes a str reference and returns a Var
    pub fn var<V: Value>(s: &str) -> AExpr<V> { Var(s.to_string()) }

    /// Takes two arithmetic expressions and returns the range of them
    pub fn range<V: Value>(l: AExpr<V>, r: AExpr<V>) -> AExpr<V> { Range(l.into(), r.into()) }

    /// Takes two arithmetic expressions and returns the sum of them
    pub fn sum<V: Value>(l: AExpr<V>, r: AExpr<V>) -> AExpr<V> { Sum(l.into(), r.into()) }

    /// Takes two arithmetic expressions and returns the difference of them
    pub fn dif<V: Value>(l: AExpr<V>, r: AExpr<V>) -> AExpr<V> { Dif(l.into(), r.into()) }

    /// Takes two arithmetic expressions and returns the product of them
    pub fn mul<V: Value>(l: AExpr<V>, r: AExpr<V>) -> AExpr<V> { Mul(l.into(), r.into()) }

    /// Takes two arithmetic expressions and returns the division of them
    pub fn div<V: Value>(l: AExpr<V>, r: AExpr<V>) -> AExpr<V> { Div(l.into(), r.into()) }

    /// Takes a arithmetic expression and returns the AParens containing it
    pub fn a_paren<V: Value>(exp: AExpr<V>) -> AExpr<V> { AParen(exp.into()) }

    /// Takes a arithmetic expression and returns the Neg containing it
    pub fn neg<V: Value>(exp: AExpr<V>) -> AExpr<V> { Neg(exp.into()) }
}

/// AExp describes the arithmetic expressions.
///
/// While lang defines them like the following:
///
/// *a* in Aexp
/// *a* ::= n | x | a1 + a2 | a1 - a2 | a1 * a2 | a1 / a2
///
/// where n is a Numeral and x a Variable.
/// Numerals map a numeric word to an arithmetic value.
/// Variables are used as keys in a State object to save a value.
/// AParen represents the round parenthesis in the expressions.
#[derive(Clone, Eq, PartialEq)]
pub enum AExpr<V> where V: Value {
    Val(V),
    Var(String),
    AParen(Box<AExpr<V>>),
    Range(Box<AExpr<V>>, Box<AExpr<V>>),
    Neg(Box<AExpr<V>>),
    Sum(Box<AExpr<V>>, Box<AExpr<V>>),
    Dif(Box<AExpr<V>>, Box<AExpr<V>>),
    Mul(Box<AExpr<V>>, Box<AExpr<V>>),
    Div(Box<AExpr<V>>, Box<AExpr<V>>),
}

impl<V> PartialOrd for AExpr<V> where V: Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if let Val(sx) = &self {
            if let Val(rx) = &other {
                sx.partial_cmp(rx)
            } else { None }
        } else { None }
    }
}

impl<V> Display for AExpr<V> where V: Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Val(v) => write!(f, "{}", v),
            Range(min, max) => write!(f, "[{}, {}]", min, max),
            Var(s) => write!(f, "{}", s),
            Sum(ae1, ae2) => write!(f, "{} + {}", ae1, ae2),
            Dif(ae1, ae2) => write!(f, "{} - {}", ae1, ae2),
            Mul(ae1, ae2) => write!(f, "{} * {}", ae1, ae2),
            Div(ae1, ae2) => write!(f, "{} / {}", ae1, ae2),
            AParen(expr) => write!(f, "({})", expr),
            Neg(expr) => write!(f, "-{}", expr)
        }
    }
}

impl<V> Debug for AExpr<V> where V: Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Val(v) => write!(f, "{:?}", v),
            Range(min, max) => write!(f, "[{:?}, {:?}]", min, max),
            Var(s) => write!(f, "v({:?})", s),
            Sum(ae1, ae2) => write!(f, "({:?} + {:?})", ae1, ae2),
            Dif(ae1, ae2) => write!(f, "({:?} - {:?})", ae1, ae2),
            Mul(ae1, ae2) => write!(f, "({:?} * {:?})", ae1, ae2),
            Div(ae1, ae2) => write!(f, "({:?} / {:?})", ae1, ae2),
            AParen(expr) => write!(f, "({:?})", expr),
            Neg(expr) => write!(f, "-({:?})", expr)
        }
    }
}

impl<V> AExpr<V> where V: Value {
    pub fn apply<W>(&self, f: &dyn Fn(&V) -> W) -> AExpr<W> where W: Value {
        match self {
            Val(v) => Val(f(v)),
            Var(s) => Var(s.clone()),
            AParen(e) => a_paren(e.apply(f)),
            Range(min, max) =>
                range(min.apply(f), max.apply(f)),
            Sum(l, r) =>
                sum(l.apply(f), r.apply(f)),
            Dif(l, r) =>
                dif(l.apply(f), r.apply(f)),
            Mul(l, r) =>
                mul(l.apply(f), r.apply(f)),
            Div(l, r) =>
                div(l.apply(f), r.apply(f)),
            Neg(e) => neg(e.apply(f))
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::domains::expressions::stmt::prelude::*;
    use crate::utilities::logging::*;

    #[test]
    /// Test Display and Debug implementation of AExpr
    fn test_aexpr_print() {
        init_log_test();
        debug!("Print NUM:\t\t{}\t\t{:?}", val(1), val(1));
        debug!("Print VAR:\t\t{}\t\t{:?}", var::<i64>("x"), var::<i64>("x"));
        debug!("Print SUM:\t\t{}\t{:?}", sum(val(1), val(5)), sum(val(1), val(5)));
        debug!("Print DIF:\t\t{}\t{:?}", dif(val(1), val(5)), dif(val(1), val(5)));
        debug!("Print MUL:\t\t{}\t{:?}", mul(val(1), val(5)), mul(val(1), val(5)));
        debug!("Print DIV:\t\t{}\t{:?}", div(val(1), val(5)), div(val(1), val(5)));
    }

    #[test]
    fn test_aexpr_apply() {
        init_log_test();
        assert_eq!(val(2), val(1)
            .apply(&|v| v * 2));
        assert_eq!(var::<i64>("x"), var::<i64>("x")
            .apply(&|v| v * 2));
        assert_eq!(a_paren(val(2)), a_paren(val(1))
            .apply(&|v| v * 2));
        assert_eq!(range(val(2), val(4)), range(val(1), val(2))
            .apply(&|v| v * 2));
        assert_eq!(sum(val(2), val(10)), sum(val(1), val(5))
            .apply(&|v| v * 2));
        assert_eq!(dif(val(2), val(10)), dif(val(1), val(5))
            .apply(&|v| v * 2));
        assert_eq!(mul(val(2), val(10)), mul(val(1), val(5))
            .apply(&|v| v * 2));
        assert_eq!(div(val(2), val(10)), div(val(1), val(5))
            .apply(&|v| v * 2));
    }
}