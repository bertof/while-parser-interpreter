//! While language stmt syntactic categories

use std::fmt::{self, Display, Formatter};

pub use SExpr::*;
use short::*;

use crate::data::value::Value;
use crate::domains::expressions::stmt::arithmetic::AExpr;
use crate::domains::expressions::stmt::boolean::BExpr;

pub mod short {
    use super::*;

    /// Returns a Skip expression
    pub fn skip<V: Value>() -> SExpr<V> { Skip }

    /// Returns a assignment expression
    pub fn ass<V: Value>(s: &str, e: AExpr<V>) -> SExpr<V> {
        Ass(s.to_string(), e.into())
    }

    /// Returns a concatenation expression of the two given stmt expressions
    pub fn conc<V: Value>(e1: SExpr<V>, e2: SExpr<V>) -> SExpr<V> { Conc(e1.into(), e2.into()) }

    /// Returns a if condition
    pub fn ite<V: Value>(eg: BExpr<V>, et: SExpr<V>, ef: SExpr<V>) -> SExpr<V> { ITE(eg.into(), et.into(), ef.into()) }

    /// Returns a while condition
    pub fn wd<V: Value>(eg: BExpr<V>, eb: SExpr<V>) -> SExpr<V> { WD(eg.into(), eb.into()) }

    /// Returns a stmt parens stmt for the given stmt expression
    pub fn s_paren<V: Value>(e: SExpr<V>) -> SExpr<V> { SParen(e.into()) }
}

/// SExp describes the stmt expressions.
///
/// While lang defines them like the following:
///
/// S in Stm
/// S ::= x := a | skip | S1 ; S2 | if b then S1 else S2 | while b do S
///
/// where a is an arithmetic expressions and b a boolean expression.
/// SParen represents the round parenthesis in boolean expressions.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum SExpr<V> where V: Value {
    Skip,
    Ass(String, Box<AExpr<V>>),
    Conc(Box<SExpr<V>>, Box<SExpr<V>>),
    ITE(Box<BExpr<V>>, Box<SExpr<V>>, Box<SExpr<V>>),
    WD(Box<BExpr<V>>, Box<SExpr<V>>),
    SParen(Box<SExpr<V>>),
}

impl<V> Display for SExpr<V> where V: Value {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use self::SExpr::*;
        match self {
            Ass(var, val) => write!(f, "{} := {}", var, val),
            Skip => write!(f, "SKIP"),
            Conc(se1, se2) => write!(f, "{}; {}", se1, se2),
            ITE(be, se1, se2) => {
                write!(f, "if {} then ", be)?;
                match &**se1 {
                    Conc(_e1, _e2) => write!(f, "{} else ", SParen(se1.clone())),
                    _ => write!(f, "{} else ", se1)
                }?;
                match &**se2 {
                    Conc(ref _e1, ref _e2) => write!(f, "{}", SParen(se2.clone())),
                    _ => write!(f, "{}", se2)
                }
            }
            WD(ref be, ref se) => {
                write!(f, "while {} do ", be)?;
                match se.as_ref() {
                    Conc(ref _e1, ref _e2) => write!(f, "{}", SParen(se.clone())),
                    _ => write!(f, "{}", se)
                }
            }
            SParen(ref expr) => write!(f, "{{{}}}", expr),
        }
    }
}

impl<V> SExpr<V> where V: Value {
    pub fn apply<W>(&self, f: &dyn Fn(&V) -> W) -> SExpr<W> where W: Value {
        match self {
            Skip => Skip,
            Ass(s, v) =>
                ass(s.as_ref(), v.apply(f)),
            Conc(e1, e2) =>
                conc(e1.apply(f), e2.apply(f)),
            ITE(b, e1, e2) =>
                ite(b.apply(f), e1.apply(f), e2.apply(f)),
            WD(b, e) =>
                wd(b.apply(f), e.apply(f)),
            SParen(e) => s_paren(e.apply(f))
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::domains::expressions::stmt::prelude::*;
    use crate::utilities::logging::*;

    #[test]
    /// Test Display and Debug implementation of SExpr
    fn test_sexpr_print_skip() {
        init_log_test();
        debug!("Print SKIP:\t\t{}\t\t\t\t\t\t\t\t{:?}", skip::<i64>(), skip::<i64>());
    }

    #[test]
    fn test_sexpr_print_ass() {
        init_log_test();
        debug!("Print ASS:\t\t{}\t\t\t\t\t\t\t\t{:?}", ass("x", val(1)), ass("x", val(1)));
    }

    #[test]
    fn test_sexpr_print_conc() {
        init_log_test();
        debug!("Print CONC:\t\t{}\t\t\t\t\t\t\t{:?}", conc(skip::<i64>(), skip::<i64>()), conc(skip::<i64>(), skip::<i64>()));
    }

    #[test]
    fn test_sexpr_print_ite() {
        init_log_test();
        debug!("Print ITE:\t\t{}\t\t\t{:?}", ite(tru(), skip::<i64>(), skip::<i64>()), ite(tru(), skip::<i64>(), skip::<i64>()));
        debug!("Print ITE CONC:\t{}\t{:?}\n\t\t\t\t{}\t{:?}",
               ite(tru(), conc(skip::<i64>(), skip::<i64>()), skip::<i64>()),
               ite(tru(), conc(skip::<i64>(), skip::<i64>()), skip::<i64>()),
               ite(tru(), skip::<i64>(), conc(skip::<i64>(), skip::<i64>())),
               ite(tru(), skip::<i64>(), conc(skip::<i64>(), skip::<i64>()))
        );
    }

    #[test]
    fn test_sexpr_print_wd() {
        init_log_test();
        debug!("Print WD:\t\t{}\t\t\t\t\t{:?}",
               wd(tru(), skip::<i64>()),
               wd(tru(), skip::<i64>())
        );
        debug!("Print WD CONC:\t{}\t\t\t{:?}",
               wd(tru(), conc(skip::<i64>(), skip::<i64>())),
               wd(tru(), conc(skip::<i64>(), skip::<i64>()))
        );
    }

    #[test]
    fn test_sexpr_apply() {
        init_log_test();
        assert_eq!(skip::<i64>(),
                   skip::<i64>()
                       .apply(&|v| v * 2));
        assert_eq!(ass("x", val(2)),
                   ass("x", val(1))
                       .apply(&|v| v * 2));
        assert_eq!(conc(ass("x", val(2)), skip::<i64>()),
                   conc(ass("x", val(1)), skip::<i64>())
                       .apply(&|v| v * 2));
        assert_eq!(ite(tru(), ass("x", val(2)), skip::<i64>()),
                   ite(tru(), ass("x", val(1)), skip::<i64>())
                       .apply(&|v| v * 2));
        assert_eq!(ite(tru(), conc(
            ass("x", val(2)),
            skip::<i64>()), skip::<i64>()),
                   ite(tru(), conc(
                       ass("x", val(1)),
                       skip::<i64>()), skip::<i64>())
                       .apply(&|v| v * 2)
        );
        assert_eq!(wd(tru(), ass("x", val(2))),
                   wd(tru(), ass("x", val(1)))
                       .apply(&|v| v * 2)
        );
        assert_eq!(wd(tru(), conc(
            ass("x", val(2)),
            skip::<i64>())),
                   wd(tru(), conc(
                       ass("x", val(1)),
                       skip::<i64>()))
                       .apply(&|v| v * 2)
        );
    }
}