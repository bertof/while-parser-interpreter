//! Module containing the tests for the parser components

/// Tests for arithmetic expressions parser
#[cfg(test)]
mod p_a_expr {
    pub use crate::utilities::logging::*;

    use super::super::*;

    #[test]
    /// Test integer parser
    fn test_p_s_i64() {
        init_log_test();
        assert_eq!(p_i64("15".into()),
                   Ok(("".into(), 15)));
        assert_eq!(p_i64("-15".into()),
                   Ok(("".into(), -15)));
        assert_eq!(p_i64("- 15".into()),
                   Ok(("".into(), -15)));
    }

    #[test]
    /// Test value parser
    fn test_p_a_val() {
        init_log_test();
        assert_eq!(p_a_val("15".into()),
                   Ok(("".into(), val(15))));
    }

    #[test]
    /// Test variable parser
    fn test_p_a_var() {
        init_log_test();
        assert_eq!(p_a_var("x".into()),
                   Ok(("".into(), var("x"))));
    }

    #[test]
    /// Test parenthesis parser
    fn test_p_a_parens() {
        init_log_test();
        assert_eq!(p_a_parens("(15)".into()),
                   Ok(("".into(), a_paren(val(15)))));
        assert_eq!(p_a_parens("( 15)".into()),
                   Ok(("".into(), a_paren(val(15)))));
        assert_eq!(p_a_parens("(15 )".into()),
                   Ok(("".into(), a_paren(val(15)))));
        assert_eq!(p_a_parens("(15+1)".into()),
                   Ok(("".into(), a_paren(sum(val(15), val(1))))));
        assert_eq!(p_a_parens("((15))".into()),
                   Ok(("".into(), a_paren(a_paren(val(15))))));
    }

    #[test]
    /// Test factor parser
    fn test_p_a_factor() {
        init_log_test();
        assert_eq!(p_a_factor("15".into()),
                   Ok(("".into(), val(15))));
        assert_eq!(p_a_factor("x".into()),
                   Ok(("".into(), var("x"))));
        assert_eq!(p_a_factor("(15)".into()),
                   Ok(("".into(), a_paren(val(15)))));
    }

    #[test]
    /// Test arithmetic term parser
    fn test_p_a_term() {
        init_log_test();
        assert_eq!(p_a_term("15".into()),
                   Ok(("".into(), val(15))));
        assert_eq!(p_a_term("15+15".into()),
                   Ok(("+15".into(), val(15))));
        assert_eq!(p_a_term("15-15".into()),
                   Ok(("-15".into(), val(15))));
        assert_eq!(p_a_term("15*15".into()),
                   Ok(("".into(), mul(val(15), val(15)))));
        assert_eq!(p_a_term("15/15".into()),
                   Ok(("".into(), div(val(15), val(15)))));
        assert_eq!(p_a_term("15*15*15".into()),
                   Ok(("".into(), mul(mul(val(15), val(15)), val(15)))));
        assert_eq!(p_a_term("15*15/15".into()),
                   Ok(("".into(), div(mul(val(15), val(15)), val(15)))));
        assert_eq!(p_a_term("15/15*15".into()),
                   Ok(("".into(), mul(div(val(15), val(15)), val(15)))));
        assert_eq!(p_a_term("15*(15-15)".into()),
                   Ok(("".into(), mul(val(15), a_paren(dif(val(15), val(15)))))));
    }

    #[test]
    /// Test arithmetic expressions parser
    fn test_p_a_expr() {
        init_log_test();
        assert_eq!(p_a_expr("15".into()),
                   Ok(("".into(), val(15))));
        assert_eq!(p_a_expr("15+15".into()),
                   Ok(("".into(), sum(val(15), val(15)))));
        assert_eq!(p_a_expr("15-15".into()),
                   Ok(("".into(), dif(val(15), val(15)))));
        assert_eq!(p_a_expr("15+15*15".into()),
                   Ok(("".into(), sum(val(15), mul(val(15), val(15))))));
        assert_eq!(p_a_expr("(15+15)*15".into()),
                   Ok(("".into(), mul(a_paren(sum(val(15), val(15))), val(15)))));
    }
}

/// Tests for boolean expressions parser
#[cfg(test)]
mod p_b_expr {
    pub use crate::utilities::logging::*;

    use super::super::*;

    #[test]
    /// Test boolean value parser
    fn test_p_b_val() {
        init_log_test();
        assert_eq!(p_b_val("tt".into()),
                   Ok(("".into(), tru())));
        assert_eq!(p_b_val("ff".into()),
                   Ok(("".into(), fal())));
    }

    #[test]
    /// Test binary boolean operator parser
    fn test_p_b_bin() {
        init_log_test();
        assert_eq!(p_b_bin("tt&&ff".into()),
                   Ok(("".into(), and(tru(), fal()))));
        assert_eq!(p_b_bin("tt &&ff".into()),
                   Ok(("".into(), and(tru(), fal()))));
        assert_eq!(p_b_bin("tt&& ff".into()),
                   Ok(("".into(), and(tru(), fal()))));
        assert_eq!(p_b_bin("tt&&ff".into()),
                   Ok(("".into(), and(tru(), fal()))));
        assert_eq!(p_b_bin("tt  &&  ff".into()),
                   Ok(("".into(), and(tru(), fal()))));
        assert_eq!(p_b_bin("tt && ff && tt".into()),
                   Ok(("".into(), and(and(tru(), fal()), tru()))));
        assert_eq!(p_b_bin("tt && (tt)".into()),
                   Ok(("".into(), and(tru(), b_paren(tru())))));
        assert_eq!(p_b_bin("tt && (1=1)".into()),
                   Ok(("".into(), and(tru(), b_paren(eq(val(1), val(1)))))));
        assert_eq!(p_b_bin("tt && 1=1".into()),
                   Ok(("".into(), and(tru(), eq(val(1), val(1))))));
    }

    #[test]
    /// Test negation operator parser
    fn test_p_b_not() {
        init_log_test();
        assert_eq!(p_b_not("!tt".into()),
                   Ok(("".into(), not(tru()))));
        assert_eq!(p_b_not("! tt".into()),
                   Ok(("".into(), not(tru()))));
        assert_eq!(p_b_not("!!tt".into()),
                   Ok(("".into(), not(not(tru())))));
        assert_eq!(p_b_not("!tt&&ff".into()),
                   Ok(("&&ff".into(), not(tru()))));
    }

    #[test]
    /// Test equal operator parser
    fn test_p_b_eq() {
        init_log_test();
        assert_eq!(p_b_eq("1=2".into()),
                   Ok(("".into(), eq(val(1), val(2)))));
        assert_eq!(p_b_eq("1 =2".into()),
                   Ok(("".into(), eq(val(1), val(2)))));
        assert_eq!(p_b_eq("1= 2".into()),
                   Ok(("".into(), eq(val(1), val(2)))));
        assert_eq!(p_b_eq("1=x".into()),
                   Ok(("".into(), eq(val(1), var("x")))));
        assert_eq!(p_b_eq("1=0+1".into()),
                   Ok(("".into(), eq(val(1), sum(val(0), val(1))))));
    }

    #[test]
    /// Test less or equal operator parser
    fn test_p_b_leq() {
        init_log_test();
        assert_eq!(p_b_leq("1<=2".into()),
                   Ok(("".into(), leq(val(1), val(2)))));
        assert_eq!(p_b_leq("1 <=2".into()),
                   Ok(("".into(), leq(val(1), val(2)))));
        assert_eq!(p_b_leq("1<= 2".into()),
                   Ok(("".into(), leq(val(1), val(2)))));
        assert_eq!(p_b_leq("1<=x".into()),
                   Ok(("".into(), leq(val(1), var("x")))));
        assert_eq!(p_b_leq("1<=0+1".into()),
                   Ok(("".into(), leq(val(1), sum(val(0), val(1))))));
    }

    #[test]
    /// Test boolean expressions parser
    fn test_p_b_expr() {
        init_log_test();
        assert_eq!(p_b_expr("1<=2".into()),
                   Ok(("".into(), leq(val(1), val(2)))));
        assert_eq!(p_b_expr("((1)<=(2))".into()),
                   Ok(("".into(), b_paren(leq(a_paren(val(1)), a_paren(val(2)))))));
        assert_eq!(p_b_expr("!1=2".into()),
                   Ok(("".into(), not(eq(val(1), val(2)))))
        );
        assert_eq!(p_b_expr("!!!!tt".into()),
                   Ok(("".into(), not(not(not(not(tru()))))))
        );
        assert_eq!(p_b_expr("!x=1".into()),
                   Ok(("".into(), not(eq(var("x"), val(1)))))
        );
        assert_eq!(p_b_expr("ff && !x=1".into()),
                   Ok(("".into(), and(fal(), not(eq(var("x"), val(1))))))
        );
        assert_eq!(p_b_expr("x<=4 && !x=1".into()),
                   Ok(("".into(), and(
                       leq(var("x"), val(4)),
                       not(eq(var("x"), val(1))),
                   )))
        );
        assert_eq!(p_b_expr("x<=4 && !x=1 && 0 <= x".into()),
                   Ok(("".into(), and(
                       and(
                           leq(var("x"), val(4)),
                           not(eq(var("x"), val(1))),
                       ),
                       leq(val(0), var("x")),
                   ))),
        );
    }
}

/// Tests for stmt expressions parser
#[cfg(test)]
mod p_s_expr {
    pub use crate::utilities::logging::*;

    use super::super::*;

    #[test]
    /// Test assignment statements parser
    fn test_p_s_ass() {
        init_log_test();
        assert_eq!(p_s_ass("x:=1".into()),
                   Ok(("".into(), ass("x", val(1)))));
        assert_eq!(p_s_ass("x :=1".into()),
                   Ok(("".into(), ass("x", val(1)))));
        assert_eq!(p_s_ass("x:= 1".into()),
                   Ok(("".into(), ass("x", val(1)))));
        assert_eq!(p_s_ass("_as123 := 1".into()),
                   Ok(("".into(), ass("_as123", val(1)))));
        assert_eq!(p_s_ass("x := 1+12".into()),
                   Ok(("".into(), ass("x", sum(val(1), val(12)))))
        );
        assert_eq!(p_s_ass("_xd := (10*8)".into()),
                   Ok(("".into(), ass("_xd", a_paren(mul(val(10), val(8))))))
        );
        assert_eq!(p_s_ass("y := y * x".into()),
                   Ok(("".into(), ass("y", mul(var("y"), var("x")))))
        );
    }

    #[test]
    /// Test skip statements parser
    fn test_p_s_skip() {
        init_log_test();
        assert_eq!(p_s_skip("skip".into()),
                   Ok(("".into(), skip())));
        assert_eq!(p_s_expr(" skip".into()),
                   Ok(("".into(), skip())));
    }

    #[test]
    /// Test concatenation of statements parser
    fn test_p_s_conc() {
        init_log_test();
        assert_eq!(p_s_conc("skip;skip".into()),
                   Ok(("".into(), conc(skip(), skip()))));
        assert_eq!(p_s_conc("skip ;skip".into()),
                   Ok(("".into(), conc(skip(), skip()))));
        assert_eq!(p_s_conc("skip; skip".into()),
                   Ok(("".into(), conc(skip(), skip()))));
        assert_eq!(p_s_conc("x:=1; skip".into()),
                   Ok(("".into(), conc(ass("x", val(1)), skip())))
        );
    }

    #[test]
    /// Test stmt parenthesis parser
    fn test_p_s_paren() {
        init_log_test();
        assert_eq!(p_s_paren("{skip}".into()),
                   Ok(("".into(), s_paren(skip())))
        );
        assert_eq!(p_s_paren("{ skip}".into()),
                   Ok(("".into(), s_paren(skip())))
        );
        assert_eq!(p_s_paren("{skip }".into()),
                   Ok(("".into(), s_paren(skip())))
        );
        assert_eq!(p_s_paren("{skip;skip}".into()),
                   Ok(("".into(), s_paren(conc(skip(), skip()))))
        );
        assert_eq!(p_s_paren("{x:=1}".into()),
                   Ok(("".into(), s_paren(ass("x", val(1)))))
        );
        assert_eq!(p_s_paren("{x:=1+2}".into()),
                   Ok(("".into(), s_paren(ass("x", sum(val(1), val(2))))))
        );
        assert_eq!(p_s_paren("{while tt do skip}".into()),
                   Ok(("".into(), s_paren(wd(tru(), skip()))))
        );
        assert_eq!(p_s_expr("{y := y * x}".into()),
                   Ok(("".into(), s_paren(ass("y", mul(var("y"), var("x"))))))
        )
    }

    #[test]
    /// Test if then else statements parser
    fn test_p_s_ite() {
        init_log_test();
        assert_eq!(p_s_ite("if tt then skip else skip".into()),
                   Ok(("".into(), ite(tru(), skip(), skip()))));
        assert_eq!(p_s_ite("if 1<=2 then x:=1 else skip".into()),
                   Ok(("".into(), ite(
                       leq(val(1), val(2)),
                       ass("x", val(1)),
                       skip(),
                   ))));
        assert_eq!(p_s_ite("if tt && 1=2 then skip else x:=1+2; skip".into()),
                   Ok(("; skip".into(), ite(
                       and(tru(), eq(val(1), val(2))),
                       skip(),
                       ass("x", sum(val(1), val(2))),
                   ))));
        assert_eq!(p_s_ite("if x <= 15 then {x:=x-1; skip} else {skip; skip}".into()),
                   Ok(("".into(), ite(
                       leq(var("x"), val(15)),
                       s_paren(conc(ass("x", dif(var("x"), val(1))), skip())),
                       s_paren(conc(skip(), skip())),
                   ))));
        assert_eq!(p_s_ite("if x <= 15 then while tt do skip else {skip}".into()),
                   Ok(("".into(), ite(
                       leq(var("x"), val(15)),
                       wd(tru(), skip()),
                       s_paren(skip()),
                   ))));
    }

    #[test]
    /// Test while do statements parser
    fn test_p_s_wd() {
        init_log_test();
        assert_eq!(p_s_wd("while tt do skip".into()),
                   Ok(("".into(), wd(
                       tru(),
                       skip(),
                   ))));
        assert_eq!(p_s_wd("while x<=2 do x:=x-1".into()),
                   Ok(("".into(), wd(
                       leq(var("x"), val(2)),
                       ass("x", dif(var("x"), val(1))),
                   ))));
        assert_eq!(p_s_wd("while 1<=x do {y:=y*x; x:=x-1}".into()),
                   Ok(("".into(), wd(
                       leq(val(1), var("x")),
                       s_paren(conc(
                           ass("y", mul(var("y"), var("x"))),
                           ass("x", dif(var("x"), val(1))),
                       )),
                   ))));
    }

    #[test]
    /// Test repeat until statements parser
    fn test_p_s_ru() {
        init_log_test();
        assert_eq!(p_s_ru("repeat skip until tt".into()),
                   Ok(("".into(), conc(
                       skip(),
                       wd(not(tru()), skip()),
                   ))));
        assert_eq!(p_s_ru("repeat x:=x-1 until x<=2".into()),
                   Ok(("".into(), conc(
                       ass("x", dif(var("x"), val(1))),
                       wd(
                           not(leq(var("x"), val(2))),
                           ass("x", dif(var("x"), val(1))),
                       ),
                   ))));
        assert_eq!(p_s_ru("repeat {y:=y*x; x:=x-1} until 1<=x".into()),
                   Ok(("".into(), conc(
                       s_paren(conc(
                           ass("y", mul(var("y"), var("x"))),
                           ass("x", dif(var("x"), val(1))),
                       )),
                       wd(
                           not(leq(val(1), var("x"))),
                           s_paren(conc(
                               ass("y", mul(var("y"), var("x"))),
                               ass("x", dif(var("x"), val(1))),
                           )),
                       ),
                   ))));
    }

    #[test]
    /// Test stmt expressions parser
    fn test_p_s_expr() {
        init_log_test();
        assert_eq!(parse_s_expr("x := 5".into()),
                   Ok(("".into(), ass("x", val(5)))));

        assert_eq!(parse_s_expr("x := 5; y := 1".into()),
                   Ok(("".into(), conc(
                       ass("x", val(5)),
                       ass("y", val(1)),
                   ))));

        assert_eq!(parse_s_expr("x := 5; y := 1; while 1 <= x do skip".into()),
                   Ok(("".into(), conc(
                       ass("x", val(5)),
                       conc(ass("y", val(1)),
                            wd(
                                leq(val(1), var("x")),
                                skip(),
                            ),
                       ),
                   ))));

        assert_eq!(parse_s_expr("x := 5; y := 1; while 1 <= x do {skip}".into()),
                   Ok(("".into(), conc(
                       ass("x", val(5)),
                       conc(ass("y", val(1)),
                            wd(
                                leq(val(1), var("x")),
                                s_paren(skip()),
                            ),
                       ),
                   ))));

        assert_eq!(parse_s_expr("x := 100; while 1<=x+1 do x:=x-1".into()),
                   Ok(("".into(), conc(
                       ass("x", val(100)),
                       wd(
                           leq(val(1), sum(var("x"), val(1))),
                           ass("x", dif(var("x"), val(1)))),
                   ))));

        assert_eq!(parse_s_expr("x := 5; y := 1; while 1<=x do {y := y * x}".into()),
                   Ok(("".into(), conc(
                       ass("x", val(5)),
                       conc(
                           ass("y", val(1)),
                           wd(
                               leq(val(1), var("x")),
                               s_paren(ass("y", mul(var("y"), var("x")))),
                           ),
                       ),
                   ))));

        assert_eq!(parse_s_expr("\
            x := 1;\
            y := 0;\
            while x <= 100 do { \
                y := y+1;\
                x := x+1\
            }".into()),
                   Ok(("".into(), conc(
                       ass("x", val(1)),
                       conc(ass("y", val(0)),
                            wd(
                                leq(var("x"), val(100)),
                                s_paren(conc(
                                    ass("y", sum(var("y"), val(1))),
                                    ass("x", sum(var("x"), val(1))),
                                )),
                            ),
                       ),
                   ))));
    }
}