//! Parser for the While (Stm) language.
//!
//! Contains the functions to parse arithmetic, boolean and stmt
//! expression for the language While (Stm).

use nom::{
    alpha1,
    alphanumeric0,
    alt,
    complete,
    delimited,
    digit1,
    do_parse,
    many0,
    map,
    map_res,
    multispace0,
    multispace1,
    named_attr,
    opt,
    preceded,
    tag,
    terminated,
    tuple,
    types::CompleteStr,
};

use crate::domains::expressions::stmt::prelude::*;

/// Arithmetic operations markers.
#[derive(Debug)]
enum AOps {
    Mul,
    Sum,
    Dif,
    Div,
}

/// Boolean operations markers.
#[derive(Debug)]
enum BOps {
    And,
    Or,
}

/// Method to fold arithmetic expression vectors.
fn fold_p_a_exprs<V: Value>(initial: AExpr<V>, remainder: Vec<(AOps, AExpr<V>)>) -> AExpr<V> {
    remainder.into_iter().fold(initial, |acc, pair| {
        let (op, a_expr) = pair;
        match op {
            AOps::Mul => mul(acc, a_expr),
            AOps::Div => div(acc, a_expr),
            AOps::Sum => sum(acc, a_expr),
            AOps::Dif => dif(acc, a_expr),
        }
    })
}

/// Method to fold boolean expression vectors.
fn fold_p_b_exprs<V: Value>(initial: BExpr<V>, remainder: Vec<(BOps, BExpr<V>)>) -> BExpr<V> {
    remainder.into_iter().fold(initial, |acc, pair| {
        let (op, b_expr) = pair;
        match op {
            BOps::And => and(acc, b_expr),
            BOps::Or => or(acc, b_expr),
        }
    })
}

/// Method to fold stmt expression vectors.
///
/// Foldr implementation for concatenation statements; generates the concatenation tree of a
/// stmt expression.
fn fold_p_s_exprs<V: Value>(initial: SExpr<V>, remainder: Vec<SExpr<V>>) -> SExpr<V> {
    match remainder.len() {
        0 => initial,
        1 => {
            let s_expr = remainder.first().unwrap();
            SExpr::Conc(Box::from(initial), Box::from(s_expr.clone()))
        }
        _ => {
            let (l, r) = remainder.split_at(1);
            let s_expr = l.get(0).unwrap();
            SExpr::Conc(Box::from(initial), Box::from(fold_p_s_exprs(s_expr.clone(), Vec::from(r))))
        }
    }
}

named_attr!( # [doc=
"64 bit integer parser from a `CompleteStr`."
], pub p_i64(CompleteStr) -> i64, do_parse!(
    m: opt!(terminated!(
        tag!("-"),
        multispace0
    )) >>
    v: map_res!(
        digit1,
        |s: CompleteStr| (m.unwrap_or_else(||"".into()).0.to_owned()+s.0).parse::<i64>()) >>
    (v)
));

named_attr!(#[doc=
"Arithmetic value parser from `CompleteStr`.

Parse an integer in an arithmetic value."
], pub p_a_val(CompleteStr) -> AExpr<i64>, do_parse!(
    v: p_i64 >> (val(v))
));

named_attr!(#[doc=
"Variable name parser from `CompleteStr`.

Valid variable names begin with `_` or an alphabetic character followed by alphanumeric character."
], pub p_var_name(CompleteStr) -> String, do_parse!(
    init: alt!(tag!("_") | alpha1) >>
    next: alphanumeric0 >>
    ((String::from(init.0) + next.0))
));

named_attr!(#[doc=
"Arithmetic variable parser from `CompleteStr`.

Parse a string as an arithmetic variable."
], pub p_a_var(CompleteStr) -> AExpr<i64>, map!(
    p_var_name,
    |n| (var(&n))
));

named_attr!(#[doc=
"Arithmetic range parser from `CompleteStr.`

Parse a string searching for square parenthesis containing two arithmetic expressions separated by a comma.
The valid characters for the parenthesis are `[` and `]`."
], pub p_a_range(CompleteStr) -> AExpr<i64>, do_parse!(
    tag!("[") >>
    multispace0 >>
    min: p_a_expr >>
    multispace0 >>
    tag!(",") >>
    multispace0 >>
    max: p_a_expr >>
    multispace0 >>
    tag!("]") >>
    (range(min, max))
));

named_attr!(#[doc=
"Arithmetic parenthesis parser from `CompleteStr`.

Parse a string searching for parenthesis containing an arithmetic expression.
The valid characters for the parenthesis are `(` and `)`."
], pub p_a_parens(CompleteStr) -> AExpr<i64>, map!(
    delimited!(
        terminated!(tag!("("), multispace0),
        p_a_expr,
        preceded!(multispace0, tag!(")"))
    ),
    |e| (a_paren(e))
));

named_attr!(#[doc=
"Arithmetic negation parser from `CompleteStr`.

Parse a string searching for minus sign followed by an arithmetic expression."
], pub p_a_neg(CompleteStr) -> AExpr<i64>, map!(
    preceded!(
        tag!("-"),
        preceded!(
            multispace0,
            p_a_expr
        )
    ),
    |e| (neg(e))
));

named_attr!(#[doc=
"Arithmetic factors parser from `CompleteStr`.

Parse a string searching for a factor.
Valid factors are values, variables and parenthesised arithmetic expressions."
], pub p_a_factor(CompleteStr) -> AExpr<i64>, alt!(
    p_a_val | p_a_var | p_a_range | p_a_neg | p_a_parens
));

named_attr!(#[doc=
"Arithmetic terms parser from `CompleteStr`.

Parse a string searching for a term. Valid terms are product of at least two factors."
], pub p_a_term(CompleteStr) -> AExpr<i64>, do_parse!(
    initial: p_a_factor >>
    remainder: many0!(
        tuple!(
            map!(preceded!(multispace0, alt!(
                tag!("*") | tag!("/")
            )), |s| match s.0 {
                "*" => AOps::Mul,
                _ => AOps::Div,
            }),
            preceded!(multispace0, p_a_factor)
        )
    ) >>
    (fold_p_a_exprs(initial, remainder))
));

named_attr!(#[doc=
"Arithmetic expressions parser from `CompleteStr`.

Parse a string searching for an arithmetic expression.
Valid arithmetic expressions are summation of at least two terms."
], pub p_a_expr(CompleteStr) -> AExpr<i64>, do_parse!(
    initial: p_a_term >>
    remainder: many0!(
        tuple!(
            map!(preceded!(multispace0, alt!(
                tag!("+") | tag!("-")
            )), |s| match s.0 {
                "+" => AOps::Sum,
                _ => AOps::Dif
            }),
            preceded!(multispace0, p_a_term)
        )
    ) >>
    (fold_p_a_exprs(initial, remainder))
));

named_attr!(#[doc=
"Boolean value parser from `CompleteStr`.

Parse a string searching for a boolean value.
Valid boolean values are `tt` which means `true` and `ff` which means `false`."
], pub p_b_val(CompleteStr) -> BExpr<i64>, map!(
    alt!(tag!("tt") | tag!("ff")),
    |v| match v.0 {
        "tt" => tru(),
        _ => fal(),
    }
));

named_attr!(#[doc=
"Binary boolean operator parser from `CompleteStr`.

Parse a string searching for boolean binary operators.
The only valid operator is *and* in the form of the string `&&`.
Multiple concatenated binary operators are folded into one."
], pub p_b_bin(CompleteStr) -> BExpr<i64>, do_parse!(
    initial: p_b_factor >>
    remainder: many0!(
        tuple!(
            map!(preceded!(multispace0, alt!(
                tag!("&&") | tag!("||")
            )), |s| match s.0 {
                "&&" => BOps::And,
                _ => BOps::Or
            }),
            preceded!(multispace0, p_b_factor)
        )
    ) >>
    (fold_p_b_exprs(initial, remainder))
));

named_attr!(#[doc=
"Boolean expression parser from `CompleteStr`.

Parse a string searching for a boolean expression.
Valid boolean expressions are folds of binary boolean operations."
], pub p_b_expr(CompleteStr) -> BExpr<i64>, alt!(
    p_b_bin
));

named_attr!(#[doc=
"Boolean factor parser from `CompleteStr`.

Parse a string searching for a boolean factor.
Valid boolean factors are boolean terms and equality and less-equality comparison."
], pub p_b_factor(CompleteStr) -> BExpr<i64>, alt!(
    p_b_term | p_b_eq | p_b_leq
));

named_attr!(#[doc=
"Boolean term parser from `CompleteStr`.

Parse a string searching for a boolean term. Valid boolean terms are boolean values, not operators and boolean parenthesised expressions.
The valid characters for the parenthesis are `(` and `)`."
], pub p_b_term(CompleteStr) -> BExpr<i64>, alt!(
    p_b_val | p_b_not | p_b_parens
));

named_attr!(#[doc=
"Boolean not operator parser from `CompleteStr`.

Parse a string searching for a negation operator.
Negation operator is `!` and applies to the closest next boolean factor."
], pub p_b_not(CompleteStr) -> BExpr<i64>, map!(
    preceded!(
        preceded!( tag!("!"), multispace0),
        p_b_factor
    ),
    |f| not(f)
));

named_attr!(#[doc=
"Boolean parenthesis parser from `CompleteStr`.

Parse a string searching for a parenthesized boolean expression."
], pub p_b_parens(CompleteStr) -> BExpr<i64>, map!(
    delimited!(
        terminated!(tag!("("), multispace0),
        p_b_expr,
        preceded!(multispace0, tag!(")"))
    ),
    |e| (b_paren(e))
));

named_attr!(#[doc=
"Boolean equal operator parser from `CompleteStr`.

Parse a string searching for an equality comparison.
Valid equality comparison are arithmetic expressions followed by `=` and by another arithmetic expression."
], pub p_b_eq(CompleteStr) -> BExpr<i64>, do_parse!(
    l: p_a_expr >>
    delimited!(
        multispace0,
        tag!("="),
        multispace0
    ) >>
    r: p_a_expr >>
    (eq(l, r))
));

named_attr!(#[doc=
"Boolean less or equal operator parser from `CompleteStr`.

Parse a string searching for an equality comparison.
Valid equality comparison are arithmetic expressions followed by `<=` and by another arithmetic expression."
], pub p_b_leq(CompleteStr) -> BExpr<i64>, do_parse!(
    l: p_a_expr >>
    delimited!(
        multispace0,
        tag!("<="),
        multispace0
    ) >>
    r: p_a_expr >>
    (leq(l, r))
));

named_attr!(#[doc=
"Statement expression parser from `CompleteStr`.

Parse a string searching for a stmt expression. Valid stmt expressions are folds of concatenations of statements."
], pub p_s_expr(CompleteStr) -> SExpr<i64>, delimited!(
    multispace0,
    p_s_conc,
    multispace0
));

named_attr!(#[doc=
"Statement term parser from `CompleteStr`.

Parse a string searching for a stmt term.
Valid stmt terms are assignments, skips, if then else statements,
while do statements and parenthesized stmt expressions."
], pub p_s_term(CompleteStr) -> SExpr<i64>, preceded!(
    multispace0,
    alt!(p_s_ass | p_s_skip | p_s_ite | p_s_wd | p_s_ru | p_s_for | p_s_paren)
));

named_attr!(#[doc=
"Assignment stmt parser from `CompleteStr`.

Parse a string searching for an assignment stmt.
Valid assignment statements begin with a variable name, followed by `:=` and an arithmetic expression."
], pub p_s_ass(CompleteStr) -> SExpr<i64>, do_parse!(
    name: p_var_name >>
    preceded!(
        multispace0,
        tag!(":=")
    ) >>
    exp: preceded!(
        multispace0,
        p_a_expr
    ) >>
    (ass(&name, exp))
));

named_attr!(#[doc=
"Skip stmt parser from `CompleteStr`.

Parse a string searching for a skip stmt. Valid skip stmt are identified by the string `skip`."
], pub p_s_skip(CompleteStr) -> SExpr<i64>, do_parse!(
    tag!("skip") >>
    (skip())
));

named_attr!(#[doc=
"If then else stmt parser from `CompleteStr`.

Parse a string searching for an if then else stmt.
Valid if then else statements begin with the keyword `if`
followed by a boolean expression, then the keyword `then` and a
stmt expression, finally the keyword `else` and a stmt expression."
], pub p_s_ite(CompleteStr) -> SExpr<i64>, do_parse!(
    tag!("if") >>
    guard: preceded!(
        multispace1,
        p_b_expr
    ) >>
    preceded!(
        multispace1,
        tag!("then")
    ) >>
    st: preceded!(
        multispace1,
        p_s_term
    ) >>
    preceded!(
        multispace1,
        tag!("else")
    ) >>
    sf: preceded!(
        multispace1,
        p_s_term
    ) >>
    (SExpr::ITE(guard.into(), st.into(), sf.into()))
));

named_attr!(#[doc=
"While do stmt parser from `CompleteStr`.

Parse a string searching for a while do stmt.
Valid while do statements begin with the keyword `while` and a
boolean expression, then the keyword `do` and a stmt expression."
], pub p_s_wd(CompleteStr) -> SExpr<i64>, do_parse!(
    tag!("while") >>
    guard: preceded!(
        multispace1,
        p_b_expr
    ) >>
    preceded!(
        multispace1,
        tag!("do")
    ) >>
    s: preceded!(
        multispace1,
        p_s_term
    ) >>
    (SExpr::WD(guard.into(), s.into()))
));

named_attr!(#[doc=
"Repeat until stmt parser from `CompleteStr`.

Parse a string searching for a repeat until stmt.
Valid repeat until statements begin with the keyword `repeat`
and a stmt expression, followed by the keyword `until` and a boolean expression.
Repeat until is syntactic sugar for the repetition of the stmt `s`
for at least one time and until the guard is false."
], pub p_s_ru(CompleteStr) -> SExpr<i64>, do_parse!(
    tag!("repeat") >>
    s: preceded!(
        multispace1,
        p_s_term
    ) >>
    preceded!(
        multispace1,
        tag!("until")
    ) >>
    guard: preceded!(
        multispace1,
        p_b_expr
    ) >>
    (SExpr::Conc(s.clone().into(), wd(not(guard), s).into()))
));

named_attr!(#[doc=
"For stmt parser from `CompleteStr`

Parse a string searching for a for stmt.
Valid for stmt begin with the keyword `for` and a stmt as starter,
a boolean expression as guard and a stmt as step, enclosed between
round parenthesis and separated by `;`, finally a stmt as the body."
], pub p_s_for(CompleteStr) -> SExpr<i64>, do_parse!(
    tag!("for") >>
    preceded!(
        multispace0,
        tag!("(")
    ) >>
    start: p_s_term >>
    preceded!(
        multispace0,
        tag!(";")
    ) >>
    guard: preceded!(
        multispace0,
        p_b_expr
    ) >>
    preceded!(
        multispace0,
        tag!(";")
    ) >>
    step: preceded!(
        multispace0,
        p_s_term
    ) >>
    preceded!(
        multispace0,
        tag!(")")
    ) >>
    body: preceded!(
        multispace0,
        p_s_term
    ) >>
    (SExpr::Conc(start.into(), wd(guard, conc(body, step)).into()))
));

named_attr!(#[doc=
"Concatenated statements parser from `CompleteStr`.

Parse a string searching for concatenated stmt terms.
Valid concatenated stmt terms are concatenated by the character `;`."
], pub p_s_conc(CompleteStr) -> SExpr<i64>, do_parse!(
    initial: p_s_term >>
    remainder: many0!(
        preceded!(
            delimited!(
                multispace0,
                tag!(";"),
                multispace0
            ),
            p_s_term
        )
    ) >> (fold_p_s_exprs(initial, remainder))
));

named_attr!(#[doc=
"Parenthesized stmt expression parser from `CompleteStr`.

Parse a string searching for a parenthesized stmt expression.
The valid characters for the parenthesis are `{` and `}`."
], pub p_s_paren(CompleteStr) -> SExpr<i64>, map!(
    delimited!(
        delimited!(multispace0, tag!("{"), multispace0),
        p_s_conc,
        preceded!(multispace0, tag!("}"))
    ),
    |e| SExpr::SParen(e.into())
));

named_attr!(#[doc=
"Parser for stmt expressions.

Parses a `CompleteStr` and searches for a stmt expression. It returns an `IResult` with the
parsed contents and any extra not parsed input."
], pub parse_s_expr(CompleteStr)->SExpr<i64>, complete!(p_s_expr));

/// Module containing tests for the parsers
mod tests;
