//! Example programs module

use crate::domains::expressions::stmt::prelude::*;

/// Simple skip program
#[allow(dead_code)]
pub fn program_skip() -> Config<i64> {
    program(skip(), State::new())
}

/// Simple assignment program
#[allow(dead_code)]
pub fn program_assignment() -> Config<i64> {
    program(ass("x", val(15)), State::new())
}

/// Simple concatenated program
#[allow(dead_code)]
pub fn program_concatenation() -> Config<i64> {
    program(conc(skip(), ass("x", val(15))), State::new())
}

/// Double assignment program
#[allow(dead_code)]
pub fn program_conc_assignments() -> Config<i64> {
    program(conc(
        ass("x", val(10)),
        ass("y", val(15)),
    ), State::new())
}

/// Simple ITE test over true branch
#[allow(dead_code)]
pub fn program_ite_example_true_branch() -> Config<i64> {
    program(ite(
        eq(val(1), val(1)),
        ass("x", val(2)),
        skip(),
    ), State::new())
}

/// Simple ITE test over false branch
#[allow(dead_code)]
pub fn program_ite_example_false_branch() -> Config<i64> {
    program(
        ite(
            eq(val(1), val(-2)),
            ass("x", val(1)),
            ass("x", val(-2)), ),
        State::new())
}

/// Calculate factorial of 5
#[allow(dead_code)]
pub fn program_factorial_5() -> Config<i64> {
    program(conc(
        ass("x", val(5)),
        conc(
            ass("y", val(1)),
            wd(
                leq(val(2), var("x")),
                conc(
                    ass("y", mul(var("x"), var("y"))),
                    ass("x", dif(var("x"), val(1))),
                ),
            )),
    ), State::new())
}

/// While true do skip
#[allow(dead_code)]
pub fn infinite_loop() -> Config<i64> {
    program(wd(tru(), skip()), State::new() as State<i64>)
}

/// While true do x++
#[allow(dead_code)]
pub fn infinite_increment() -> Config<i64> {
    program(conc(
        ass("x", val(1)),
        wd(tru(), ass("x", sum(var("x"), val(1))))),
            State::new() as State<i64>)
}