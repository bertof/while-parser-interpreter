//! Concrete interpreters module

pub mod operational;
pub mod denotational;