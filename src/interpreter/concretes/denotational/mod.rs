//! Interpreter for the While language using the denotational-style semantics

use crate::data::config::Config::{Program, Ts};
use crate::domains::expressions::stmt::prelude::*;
use crate::interpreter::traits::*;
use crate::utilities::fix_point::fp;
use crate::utilities::logging::*;

use super::operational;

/// Denotational-style semantics interpreter
pub struct Interpreter;

impl<V> ConcreteArithmeticInterpreter<V> for Interpreter where V: Value {
    fn a_eval(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        operational::Interpreter::a_eval(exp, state)
    }
}

impl<V> ConcreteBooleanInterpreter<V> for Interpreter where V: Value {
    fn b_eval(exp: &BExpr<V>, state: &State<V>) -> Result<bool, Box<dyn Error>> {
        operational::Interpreter::b_eval(exp, state)
    }
}

impl<V> ConcreteInterpreter<V> for Interpreter where V: Value {
    fn s_eval(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>> {
        operational::Interpreter::s_eval(conf)
    }

    fn run(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>> {
        let r = match &conf {
            Config::Ts(_) => conf.clone(),
            Config::Program(e, s) => match e {
                SExpr::Skip => {
                    let r = Ts(s.clone());
                    trace!("{} => {}", &conf, &r);
                    r
                }
                SExpr::Ass(key, val) => {
                    let r = Ts(s.set(key.as_ref(), Self::a_eval(&val, s)?));
                    trace!("{} => {}", &conf, &r);
                    r
                }
                SExpr::Conc(e1, e2) => {
                    match Self::run(&Program(*e1.clone(), s.clone()))? {
                        Ts(s1) => Self::run(&Program(*e2.clone(), s1))?,
                        r => panic!("Unfinished run: {}", &r),
                    }
                }
                SExpr::ITE(guard, se1, se2) => {
                    let truth_val = Self::b_eval(&guard, s)?;
                    let p = if truth_val {
                        Program(*se1.clone(), s.clone())
                    } else {
                        Program(*se2.clone(), s.clone())
                    };
                    let r = Self::run(&p)?;
                    trace!("B[{}]{} == {} => {} => {}", &guard, &s, &truth_val, &p, &r);
                    r
                }
                SExpr::WD(_, _) => {
                    let res =
                        fp(conf.clone(), &|c| {
                            if let Program(SExpr::WD(g, e), s) = c {
                                if Self::b_eval(&g, s)? {
                                    let p = Program(*e.clone(), s.clone());
                                    let c1 = Self::run(&p)?;
                                    debug_assert!(c1.is_terminated());
                                    Ok(Program(
                                        SExpr::WD(g.clone(), e.clone()),
                                        c1.state().clone()))
                                } else {
                                    Ok(c.clone())
                                }
                            } else {
                                // Is Ts
                                Ok(c.clone())
                            }
                        })?;
                    Config::Ts(res.state().clone())
                }
                SExpr::SParen(se) => {
                    Self::run(&Program(*se.clone(), s.clone()))?
                }
            }
        };
        debug!("{} => {:?}", &conf, &r);
        Ok(r)
    }
}

#[cfg(test)]
mod tests;
