//! Interpreter for the While language using the operative semantics
use crate::data::config::Config::{Program, Ts};
use crate::domains::expressions::stmt::prelude::*;
use crate::interpreter::traits::*;

/// Operational semantics interpreter
pub struct Interpreter;

impl<V> ConcreteArithmeticInterpreter<V> for Interpreter where V: Value {
    fn a_eval(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        match exp {
            AExpr::Val(v) => Ok(v.clone()),
            AExpr::Var(name) => {
                match state.get(name.as_str()) {
                    Some(v) => Ok(v.clone()),
                    None => Err(format!("Variable {} not found in state {}", name, state).into()),
                }
            }
            AExpr::AParen(e) => Self::a_eval(e, state),
            AExpr::Range(min, max) => {
                let min_v = Self::a_eval(min, state)?;
                let max_v = Self::a_eval(max, state)?;
                Ok(V::rnd_range(min_v, max_v))
            }
            AExpr::Sum(sx, dx) => {
                Ok(Self::a_eval(sx, state)? + Self::a_eval(dx, state)?)
            }
            AExpr::Dif(sx, dx) => {
                Ok(Self::a_eval(sx, state)? - Self::a_eval(dx, state)?)
            }
            AExpr::Mul(sx, dx) => {
                Ok(Self::a_eval(sx, state)? * Self::a_eval(dx, state)?)
            }
            AExpr::Div(sx, dx) => {
                Ok(Self::a_eval(sx, state)? / Self::a_eval(dx, state)?)
            }
            AExpr::Neg(e) => Ok(-Self::a_eval(e, state)?)
        }
    }
}

impl<V> ConcreteBooleanInterpreter<V> for Interpreter where V: Value {
    fn b_eval(exp: &BExpr<V>, state: &State<V>) -> Result<bool, Box<dyn Error>> {
        match exp {
            BExpr::True => Ok(true),
            BExpr::False => Ok(false),
            BExpr::Not(be) => Ok(!Self::b_eval(be, state)?),
            BExpr::BParen(ref expr) => Self::b_eval(expr, state),
            BExpr::And(l, r) =>
                Ok(Self::b_eval(l, state)? & Self::b_eval(r, state)?),
            BExpr::Or(l, r) =>
                Ok(Self::b_eval(l, state)? | Self::b_eval(r, state)?),
            BExpr::Eq(l, r) =>
                Ok(Self::a_eval(l, state)? == Self::a_eval(r, state)?),
            BExpr::LEq(l, r) =>
                Ok(Self::a_eval(l, state)? <= Self::a_eval(r, state)?),
        }
    }
}

impl<V> ConcreteInterpreter<V> for Interpreter where V: Value {
    fn s_eval(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>> {
        match conf {
            Config::Ts(state) => Ok(Ts(state.clone())),
            Config::Program(s_expr, state) => match s_expr {
                SExpr::Skip => { Ok(Ts(state.clone())) }

                SExpr::Ass(key, val) =>
                    Ok(Ts(state.set(&key, Self::a_eval(&*val, state)?))),

                SExpr::Conc(se1, se2) => {
                    let p1 = program(*se1.clone(), state.clone());

                    match Self::s_eval(&p1)? {
                        Program(se1_1, state_1) => Ok(program(
                            conc(se1_1, *se2.clone()), state_1)
                        ),
                        Ts(ref s) => Ok(program(*se2.clone(), s.clone())),
                    }
                }

                SExpr::ITE(guard, se1, se2) => {
                    if Self::b_eval(&*guard, state)? {
                        Ok(program(*se1.clone(), state.clone()))
                    } else {
                        Ok(program(*se2.clone(), state.clone()))
                    }
                }

                SExpr::WD(guard, se) => if Self::b_eval(&guard, state)? {
                    Ok(program(
                        conc(
                            ite(*guard.clone(), *se.clone(), skip()),
                            wd(*guard.clone(), *se.clone()),
                        ), state.clone(),
                    ))
                } else {
                    Ok(program(skip(), state.clone()))
                },

                SExpr::SParen(se) => {
                    let p = Program(*se.clone(), state.clone());
                    Self::s_eval(&p)
                }
            }
        }
    }

    fn run(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>> {
        let mut conf = conf.clone();
        loop {
            match &conf {
                Config::Ts(s) => return Ok(Ts(s.clone())),
                Config::Program(_, _) => conf = Self::s_eval(&conf)?
            }
        }
    }
}

/// Module containing all the tests for the interpreter and the data structure it uses
#[cfg(test)]
mod tests;
