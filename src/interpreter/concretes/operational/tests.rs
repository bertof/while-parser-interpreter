//! Module containing all the tests for the operational style interpreter and the data structure it uses

use crate::interpreter::examples::{infinite_increment, infinite_loop};
use crate::utilities::logging::*;

use super::*;

#[cfg(test)]
mod a_expr {
    use super::*;

    #[test]
    fn test_eval_num() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::a_eval(&val(1), &s)?, 1);
        Ok(())
    }

    #[test]
    fn test_eval_sum() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::a_eval(&sum(val(1), val(0)), &s)?, 1);
        Ok(())
    }

    #[test]
    fn test_eval_mul() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::a_eval(&dif(val(1), val(2)), &s)?, -1);
        Ok(())
    }

    #[test]
    fn test_eval_div() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::a_eval(&div(val(1), val(2)), &s)?, 0);
        Ok(())
    }

    #[test]
    /// Test range number generation
    fn test_range() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        let r = range(val(15), val(30));
        let res = Interpreter::a_eval(&r, &s)?;
        assert!(res < Interpreter::a_eval(&val(30), &s)?);
        assert!(res >= Interpreter::a_eval(&val(15), &s)?);
        Ok(())
    }
}

#[cfg(test)]
mod b_expr {
    use crate::data::state::State;

    use super::*;

    #[test]
    fn test_eval_tru() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&tru(), &s)?, true);
        Ok(())
    }

    #[test]
    fn test_eval_fal() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&fal(), &s)?, false);
        Ok(())
    }

    #[test]
    fn test_eval_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&not(tru()), &s)?, false);
        Ok(())
    }

    #[test]
    fn test_eval_not_not() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&not(not(tru())), &s)?, true);
        Ok(())
    }

    #[test]
    fn test_eval_and() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&and(tru(), fal()), &s)?, false);
        Ok(())
    }

    #[test]
    fn test_eval_or() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s: State<i64> = State::new();
        assert_eq!(Interpreter::b_eval(&or(tru(), fal()), &s)?, true);
        Ok(())
    }

    #[test]
    fn test_eval_eq() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::b_eval(&eq(val(1), val(2)), &s)?, false);
        Ok(())
    }

    #[test]
    fn test_eval_leq() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        assert_eq!(Interpreter::b_eval(&leq(val(1), val(2)), &s)?, true);
        Ok(())
    }
}

#[cfg(test)]
mod s_exp {
    use crate::data::state::State;

    use super::*;

    #[test]
    fn test_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        match Interpreter::s_eval(&program(ass("x", val(3)), State::new()))? {
            Ts(s) => assert_eq!(Interpreter::a_eval(&var("x"), &s)?, 3),
            _ => panic!("Wrong return type")
        };
        Ok(())
    }

    #[test]
    fn test_skip() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        match Interpreter::s_eval(&program(
            skip(),
            s.clone(),
        ))? {
            Ts(s1) => assert_eq!(s1, s),
            _ => panic!("Wrong return type")
        };
        Ok(())
    }

    #[test]
    fn test_conc() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new().set("x", 1);
        match Interpreter::s_eval(&program(
            conc(skip(), skip()),
            s.clone(),
        ))? {
            Program(e, s1) => {
                assert_eq!(e, skip());
                assert_eq!(s1, s)
            }
            _ => panic!("Wrong return type")
        };
        Ok(())
    }

    #[test]
    fn test_conc_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        match Interpreter::s_eval(&program(
            conc(
                ass("x", val(1)),
                ass("x", sum(var("x"), val(1))),
            ), s,
        ))? {
            Program(e, s1) => {
                assert_eq!(e, ass("x", sum(var("x"), val(1))));
                assert_eq!(Interpreter::a_eval(&var("x"), &s1)?, 1)
            }
            _ => panic!("Wrong return type")
        }
        Ok(())
    }

    #[test]
    fn test_ite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        match Interpreter::s_eval(&program(
            ite(tru(), ass("x", val(1)), ass("x", val(2))),
            State::new(),
        ))? {
            Program(e, s1) => {
                assert_eq!(e, ass("x", val(1)));
                assert_eq!(s1, s);
            }
            _ => panic!("Wrong return type")
        }
        Ok(())
    }

    #[test]
    fn test_ite_ass() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        match Interpreter::s_eval(&program(
            ite(fal(), ass("x", val(1)), ass("x", val(2))),
            State::new(),
        ))? {
            Program(e, s1) => {
                assert_eq!(e, ass("x", val(2)));
                assert_eq!(s1, s);
            }
            _ => panic!("Wrong return type")
        }
        Ok(())
    }

    #[test]
    fn test_wd() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        match Interpreter::s_eval(&program(
            wd(fal(), ass("x", val(1))),
            s.clone(),
        ))? {
            Program(e, s1) => {
                assert_eq!(e, skip());
                assert_eq!(s1, s);
            }
            _ => panic!("Wrong return type")
        }
        Ok(())
    }

    #[test]
    fn test_wd_conc() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let s = State::new();
        match Interpreter::s_eval(
            &program(wd(tru(), ass("x", val(1))), s)
        )? {
            Program(e, s1) => {
                assert_eq!(e,
                           conc(
                               ite(tru(), ass("x", val(1)), skip()),
                               wd(tru(), ass("x", val(1))),
                           )
                );
                assert_eq!(s1, State::new());
            }
            _ => panic!("Wrong return type")
        }
        Ok(())
    }
}

#[cfg(test)]
mod program {
    use crate::data::state::State;
    use crate::interpreter::examples::*;

    use super::*;

    #[test]
    fn test_p1() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p1 = program_skip();
        assert_eq!(Interpreter::s_eval(&p1)?.state(), &State::new());
        let res = Interpreter::run(&p1)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new());
        Ok(())
    }

    #[test]
    fn test_p2() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p2 = program_assignment();
        assert_eq!(Interpreter::s_eval(&p2)?.state(), &State::new().set("x", 15));
        let res = Interpreter::run(&p2)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new().set("x", 15));
        Ok(())
    }

    #[test]
    fn test_p3() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p3 = program_concatenation();
        assert_eq!(Interpreter::s_eval(&p3)?.state(), &State::new());
        let res = Interpreter::run(&p3)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new().set("x", 15));
        Ok(())
    }

    #[test]
    fn test_p4() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p4 = program_conc_assignments();
        assert_eq!(Interpreter::s_eval(&p4)?.state(), &State::new().set("x", 10));
        let res = Interpreter::run(&p4)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new()
            .set("x", 10)
            .set("y", 15));
        Ok(())
    }

    #[test]
    fn test_p5() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p5 = program_ite_example_true_branch();
        assert_eq!(Interpreter::s_eval(&p5)?.state(), &State::new());
        let res = Interpreter::run(&p5)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new()
            .set("x", 2));
        Ok(())
    }

    #[test]
    fn test_p6() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p6 = program_ite_example_false_branch();
        assert_eq!(Interpreter::s_eval(&p6)?.state(), &State::new());
        let res = Interpreter::run(&p6)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new().set("x", -2));
        Ok(())
    }

    #[test]
    fn test_p7() -> Result<(), Box<dyn Error>> {
        init_log_test();
        let p7 = program_factorial_5();
        assert_eq!(Interpreter::s_eval(&p7)?.state(), &State::new().set("x", 5));
        let res = Interpreter::run(&p7)?;
        debug!("{}\n", &res);
        assert_eq!(res.state(), &State::new()
            .set("x", 1)
            .set("y", 120));
        Ok(())
    }
}

#[test]
#[ignore]
/// "while true do skip" test. It's run only if the environment variable "REPEAT" is set and it will
/// loop forever.
fn test_infinite_loop() -> Result<(), Box<dyn Error>> {
    init_log_test();
    let res = Interpreter::run(&infinite_loop())?;
    debug!("{}\n", &res);
    panic!("Should never end")
}

#[test]
#[ignore]
fn test_infinite_increment() -> Result<(), Box<dyn Error>> {
    init_log_test();
    let res = Interpreter::run(&infinite_increment())?;
    debug!("{}\n", &res);
    panic!("Should never end")
}