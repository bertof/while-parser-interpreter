//! Interpreter generic traits
use std::cmp::Ordering;

use crate::domains::expressions::stmt::prelude::*;
use crate::utilities::fix_point::fp;

/// Trait for an arithmetic interpreter.
pub trait ConcreteArithmeticInterpreter<V> where V: Value {
    /// Arithmetic evaluation.
    ///
    /// Try to evaluate an arithmetic expression on a state.
    /// If the evaluation terminates a integer is returned, otherwise an error is returned.
    fn a_eval(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>>;
}

/// Trait for a boolean interpreter.
pub trait ConcreteBooleanInterpreter<V>: ConcreteArithmeticInterpreter<V> where V: Value {
    /// Boolean evaluation.
    ///
    /// Try to evaluate a boolean expression on a state.
    /// If the evaluation terminates a boolean is returned, otherwise an error is returned.
    fn b_eval(exp: &BExpr<V>, state: &State<V>) -> Result<bool, Box<dyn Error>>;
}

/// Trait for an interpreter.
pub trait ConcreteInterpreter<V> where V: Value {
    /// Evaluate a stmt expression on a state and returns its successor.
    ///
    /// This method takes in input a configuration and returns a configuration if the evaluation of the stmt expression terminates correctly, otherwise it returns an an error.
    /// The method is based on the operational semantics of the While language.
    fn s_eval(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>>;
    /// Evaluate a stmt expression on a state and returns its result.
    ///
    /// This method takes in input a configuration and returns a configuration if the evaluation of the stmt expression terminates correctly, otherwise it returns a string containing an error.
    /// The method is based on the denotational semantics of the While language.
    fn run(conf: &Config<V>) -> Result<Config<V>, Box<dyn Error>>;
}

/// Trait for an abstract arithmetic interpreter.
pub trait AbstractArithmeticInterpreter<V> where V: AbstractValue {
    /// Arithmetic evaluation.
    ///
    /// Try to evaluate an arithmetic expression on a state.
    /// If the evaluation terminates a value is returned, otherwise an error is returned.
    fn a_eval(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        match exp {
            AExpr::Val(v) => Self::a_eval_val(v, state),
            AExpr::Var(name) => Self::a_eval_var(name, state),
            AExpr::AParen(exp) => Self::a_eval_paren(&exp, state),
            AExpr::Range(min, max) => Self::a_eval_range(min, max, state),
            AExpr::Neg(e) => Self::a_eval_neg(e, state),
            AExpr::Sum(sx, dx) => Self::a_eval_sum(sx, dx, state),
            AExpr::Dif(sx, dx) => Self::a_eval_dif(sx, dx, state),
            AExpr::Mul(sx, dx) => Self::a_eval_mul(sx, dx, state),
            AExpr::Div(sx, dx) => Self::a_eval_div(sx, dx, state),
        }
    }

    /// Evaluate a value.
    fn a_eval_val(v: &V, _state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(v.clone())
    }

    /// Evaluate a variable.
    fn a_eval_var(name: &str, state: &State<V>) -> Result<V, Box<dyn Error>> {
        match state.get(&name) {
            Some(v) => Ok(v.clone()),
            None => Err(format!("Variable {} not found in state {}", name, state).into()),
        }
    }

    /// Evaluate a parenthesized arithmetic expression.
    fn a_eval_paren(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Self::a_eval(exp, state)
    }

    /// Evaluate a range of values.
    fn a_eval_range(min: &AExpr<V>, max: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        let r_min = Self::a_eval(&min, &state.clone())?;
        let r_max = Self::a_eval(&max, &state.clone())?;
        Ok(V::rnd_range(r_min, r_max))
    }

    /// Evaluate a negation of an arithmetic expression.
    fn a_eval_neg(exp: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(-Self::a_eval(exp, state)?)
    }

    /// Evaluate the sum of rwo arithmetic expressions.
    fn a_eval_sum(sx: &AExpr<V>, dx: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(Self::a_eval(&sx, &state.clone())? +
            Self::a_eval(&dx, &state.clone())?)
    }

    /// Evaluate the difference of two arithmetic expressions.
    fn a_eval_dif(sx: &AExpr<V>, dx: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(Self::a_eval(&sx, &state.clone())? -
            Self::a_eval(&dx, &state.clone())?)
    }

    /// Evaluate the multiplication of two arithmetic expressions.
    fn a_eval_mul(sx: &AExpr<V>, dx: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(Self::a_eval(&sx, &state.clone())? *
            Self::a_eval(&dx, &state.clone())?)
    }

    /// Evaluate the division of two arithmetic expressions.
    fn a_eval_div(sx: &AExpr<V>, dx: &AExpr<V>, state: &State<V>) -> Result<V, Box<dyn Error>> {
        Ok(Self::a_eval(&sx, &state.clone())? /
            Self::a_eval(&dx, &state.clone())?)
    }
}

/// Trait for an abstract boolean interpreter.
pub trait AbstractBooleanInterpreter<V>: AbstractArithmeticInterpreter<V> where V: AbstractValue {
    /// Boolean comparison between two AExpr.
    ///
    /// `operator` must return `true` when the expected output is the same state given as input and
    /// `false` when the expected output is a bottom representation.
    ///
    /// Example operator:
    ///
    /// - `ev_var1 == ev_var2` for EQ
    /// - `ev_var1.partial_cmp(&ev_var2) != Some(Ordering::Greater)` for LEQ
    fn boolean_simple_aexpr_comparison(
        exp1: &AExpr<V>, exp2: &AExpr<V>, state: &State<V>,
        operator: fn(&V, &V) -> bool) -> Result<State<V>, Box<dyn Error>> {
        // Evaluate the expressions
        let ev_exp1 = Self::a_eval(exp1, state)?;
        let ev_exp2 = Self::a_eval(exp2, state)?;

        Ok(match (&exp1, &exp2, &ev_exp1, &ev_exp2, ) {
            _ if ev_exp1.is_bottom() || ev_exp2.is_bottom() => state.to_bottom(),
            _ if ev_exp1.is_top() || ev_exp2.is_top() => state.clone(),
            _ if operator(&ev_exp1, &ev_exp2) => state.clone(),
            _ => state.to_bottom()
        })
    }

    /// Filter a state based on a boolean expression
    fn b_filter(exp: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        match exp {
            BExpr::True => Self::b_filter_true(state),
            BExpr::False => Self::b_filter_false(state),
            BExpr::Not(not_e) => match &**not_e {
                BExpr::True => Self::b_filter_not_true(state),
                BExpr::False => Self::b_filter_not_false(state),
                BExpr::Not(exp) => Self::b_filter_not_not(exp, state),
                BExpr::BParen(exp) => Self::b_filter_not_paren(exp, state),
                BExpr::And(exp1, exp2) =>
                    Self::b_filter_not_and(exp1, exp2, state),
                BExpr::Or(exp1, exp2) =>
                    Self::b_filter_not_or(exp1, exp2, state),
                BExpr::Eq(exp1, exp2) =>
                    Self::b_filter_not_eq(exp1, exp2, state),
                BExpr::LEq(exp1, exp2) =>
                    Self::b_filter_not_leq(exp1, exp2, state)
            },
            BExpr::BParen(exp) => Self::b_filter_paren(exp, state),
            BExpr::And(exp1, exp2) => Self::b_filter_and(exp1, exp2, state),
            BExpr::Or(exp1, exp2) => Self::b_filter_or(exp1, exp2, state),
            BExpr::Eq(exp1, exp2) => Self::b_filter_eq(exp1, exp2, state),
            BExpr::LEq(exp1, exp2) => Self::b_filter_leq(exp1, exp2, state),
        }
    }

    /// Filter using a true expression
    fn b_filter_true(state: &State<V>) -> Result<State<V>, Box<dyn Error>> { Ok(state.clone()) }
    /// Filter using a false expression
    fn b_filter_false(state: &State<V>) -> Result<State<V>, Box<dyn Error>> { Ok(state.to_bottom()) }
    /// Filter using a not(false) expression
    fn b_filter_not_true(state: &State<V>) -> Result<State<V>, Box<dyn Error>> { Self::b_filter_false(state) }
    /// Filter using a not(true) expression
    fn b_filter_not_false(state: &State<V>) -> Result<State<V>, Box<dyn Error>> { Self::b_filter_true(state) }
    /// Filter using a not(not(something)) expression
    fn b_filter_not_not(exp: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> { Self::b_filter(exp, state) }
    /// Filter using a not((something)) expression
    fn b_filter_not_paren(exp: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        Self::b_filter(&not(exp.clone()), state)
    }
    /// Filter using a not and expression
    fn b_filter_not_and(exp1: &BExpr<V>, exp2: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        Self::b_filter(&or(not(exp1.clone()), not(exp2.clone())), state)
    }
    /// Filter using a not or expression
    fn b_filter_not_or(exp1: &BExpr<V>, exp2: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        Self::b_filter(&and(not(exp1.clone()), not(exp2.clone())), state)
    }
    /// Filter using a not equal expression
    fn b_filter_not_eq(exp1: &AExpr<V>, exp2: &AExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        match (&exp1, &exp2) {
            (&e1, &AExpr::Sum(e2, e3)) => {
                let s1 = Self::b_filter(&not(eq(
                    e1.clone(),
                    val(Self::a_eval(&sum(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?))), state)?;

                let s2 = Self::b_filter(&not(eq(
                    val(Self::a_eval(&dif(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Sum(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&not(eq(
                    val(Self::a_eval(&sum(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                )), state)?;

                let s2 = Self::b_filter(&not(eq(
                    *e1.clone(),
                    val(Self::a_eval(&dif(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&e1, &AExpr::Dif(e2, e3)) => {
                let s1 = Self::b_filter(&not(eq(
                    e1.clone(),
                    val(Self::a_eval(&dif(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?))), state)?;

                let s2 = Self::b_filter(&not(eq(
                    val(Self::a_eval(&sum(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Dif(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&not(eq(
                    val(Self::a_eval(&dif(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                )), state)?;

                let s2 = Self::b_filter(&not(eq(
                    *e1.clone(),
                    val(Self::a_eval(&sum(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            _ => Self::boolean_simple_aexpr_comparison(
                exp1, exp2, state, |a, b| {
                    match a.partial_cmp(b) {
                        Some(Ordering::Equal) => false,
                        _ => true,
                    }
                })
        }
    }
    /// Filter using a not(a <= b) expression
    fn b_filter_not_leq(exp1: &AExpr<V>, exp2: &AExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        match (&exp1, &exp2) {
            (&e1, &AExpr::Sum(e2, e3)) => {
                let s1 = Self::b_filter(&not(leq(
                    e1.clone(),
                    val(Self::a_eval(&sum(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?))), state)?;

                let s2 = Self::b_filter(&not(leq(
                    val(Self::a_eval(&dif(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Sum(e1, e2), &e3) => {
                let s1 = Self::b_filter(&not(leq(
                    val(Self::a_eval(&sum(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                )), state)?;

                let s2 = Self::b_filter(&not(leq(
                    *e1.clone(),
                    val(Self::a_eval(&dif(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&e1, &AExpr::Dif(e2, e3)) => {
                let s1 = Self::b_filter(&not(leq(
                    e1.clone(),
                    val(Self::a_eval(&dif(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?))), state)?;

                let s2 = Self::b_filter(&not(leq(
                    val(Self::a_eval(&sum(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Dif(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&not(leq(
                    val(Self::a_eval(&dif(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                )), state)?;

                let s2 = Self::b_filter(&not(leq(
                    *e1.clone(),
                    val(Self::a_eval(&sum(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                )), state)?;
                Ok(s1.intersect(&s2))
            }
            _ => Self::boolean_simple_aexpr_comparison(
                exp1, exp2, state, |a, b| {
                    match a.partial_cmp(b) {
                        Some(Ordering::Equal) | Some(Ordering::Less) => false,
                        _ => true,
                    }
                })
        }
    }
    /// Filter using a parenthesized expression
    fn b_filter_paren(exp: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        Self::b_filter(exp, state)
    }
    /// Filter using an and expression
    fn b_filter_and(exp1: &BExpr<V>, exp2: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        let s1 = Self::b_filter(exp1, state)?;
        let s2 = Self::b_filter(exp2, state)?;
        Ok(s1.intersect(&s2))
    }
    /// Filter using an or expression
    fn b_filter_or(exp1: &BExpr<V>, exp2: &BExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        let s1 = Self::b_filter(exp1, state)?;
        let s2 = Self::b_filter(exp2, state)?;
        Ok(s1.unite(&s2))
    }
    /// Filter using an equal comparison expression
    fn b_filter_eq(exp1: &AExpr<V>, exp2: &AExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        match (&exp1, &exp2) {
            (&e1, &AExpr::Sum(e2, e3)) => {
                let s1 = Self::b_filter(&eq(
                    e1.clone(),
                    val(Self::a_eval(&sum(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?)), state)?;

                let s2 = Self::b_filter(&eq(
                    val(Self::a_eval(&dif(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Sum(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&eq(
                    val(Self::a_eval(&sum(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                ), state)?;

                let s2 = Self::b_filter(&eq(
                    *e1.clone(),
                    val(Self::a_eval(&dif(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&e1, &AExpr::Dif(e2, e3)) => {
                let s1 = Self::b_filter(&eq(
                    e1.clone(),
                    val(Self::a_eval(&dif(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?)), state)?;

                let s2 = Self::b_filter(&eq(
                    val(Self::a_eval(&sum(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Dif(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&eq(
                    val(Self::a_eval(&dif(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                ), state)?;
                let s2 = Self::b_filter(&eq(
                    *e1.clone(),
                    val(Self::a_eval(&sum(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            _ => Self::boolean_simple_aexpr_comparison(exp1, exp2, state, |a, b| {
                match a.partial_cmp(b) {
                    None | Some(Ordering::Equal) => true,
                    _ => false,
                }
            })
        }
    }
    /// Filter using a less or equal comparison expression
    fn b_filter_leq(exp1: &AExpr<V>, exp2: &AExpr<V>, state: &State<V>) -> Result<State<V>, Box<dyn Error>> {
        match (&exp1, &exp2) {
            (&e1, &AExpr::Sum(e2, e3)) => {
                let s1 = Self::b_filter(&leq(
                    e1.clone(),
                    val(Self::a_eval(&sum(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?)), state)?;
                let s2 = Self::b_filter(&leq(
                    val(Self::a_eval(&dif(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Sum(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&leq(
                    val(Self::a_eval(&sum(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                ), state)?;
                let s2 = Self::b_filter(&leq(
                    *e1.clone(),
                    val(Self::a_eval(&dif(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&e1, &AExpr::Dif(e2, e3)) => {
                let s1 = Self::b_filter(&leq(
                    e1.clone(),
                    val(Self::a_eval(&dif(
                        *e2.clone(),
                        *e3.clone(),
                    ), state)?)), state)?;
                let s2 = Self::b_filter(&leq(
                    val(Self::a_eval(&sum(
                        e1.clone(),
                        *e3.clone(),
                    ), state)?),
                    *e2.clone(),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            (&AExpr::Dif(e1, e2), &e3, ) => {
                let s1 = Self::b_filter(&leq(
                    val(Self::a_eval(&dif(
                        *e1.clone(),
                        *e2.clone(),
                    ), state)?),
                    e3.clone(),
                ), state)?;
                let s2 = Self::b_filter(&leq(
                    *e1.clone(),
                    val(Self::a_eval(&sum(
                        e3.clone(),
                        *e2.clone(),
                    ), state)?),
                ), state)?;
                Ok(s1.intersect(&s2))
            }
            _ => Self::boolean_simple_aexpr_comparison(exp1, exp2, state, |a, b| {
                match a.partial_cmp(b) {
                    None | Some(Ordering::Equal) | Some(Ordering::Less) => true,
                    _ => false,
                }
            })
        }
    }
}

/// Trait for an abstract interpreter.
pub trait AbstractInterpreter<V>: AbstractBooleanInterpreter<V> where V: AbstractValue {
    /// Execute the configuration and return a final configuration or an error.
    fn run(conf: &AbstractConfig<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let r = match conf.expression() {
            None => Ok(conf.clone()),
            Some(e) => match e {
                SExpr::Skip => Self::run_skip(conf.state()),
                SExpr::Ass(var, val) => Self::run_ass(&var, &val, conf.state()),
                SExpr::Conc(e1, e2) => Self::run_conc(&e1, &e2, conf.state()),
                SExpr::ITE(g, e1, e2) => Self::run_ite(&g, &e1, &e2, conf.state()),
                SExpr::WD(g, e) => Self::run_wd(&g, &e, conf.state()),
                SExpr::SParen(e) => Self::run_paren(e, conf.state()),
            }
        }?;
        assert!(r.is_terminated());
        Ok(r)
    }

    /// Execute a skip statement.
    fn run_skip(s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let exp_span = debug_span!(
            "Skip",
            exp = format!("{}", &skip::<V>()).as_str(),
            state = format!("{}", &s).as_str()
        );
        let _enter = exp_span.enter();
        let r = AbstractConfig::from(s.clone());
        debug!("Skip => {}", &r);
        Ok(r)
    }
    /// Execute an assignment statement.
    fn run_ass(var: &str, val: &AExpr<V>, s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let exp_span = debug_span!(
            "Assign",
            exp = format!("{}", &ass(var, val.clone())).as_str(),
            state = format!("{}", &s).as_str()
        );
        let _enter = exp_span.enter();
        let new_val = Self::a_eval(&val, s)?;
        let r = AbstractConfig::from(s.set(
            var, V::update_state(&s.get(var).cloned(), &new_val)));
        debug!("Assign => {}", &r);
        Ok(r)
    }
    /// Execute a concatenation statement.
    fn run_conc(e1: &SExpr<V>, e2: &SExpr<V>, s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let exp_span = debug_span!(
            "Conc",
            exp = format!("{}", &conc(e1.clone(), e2.clone())).as_str(),
            state = format!("{}", &s).as_str()
        );
        let _enter = exp_span.enter();
        let p1 = AbstractConfig::from((e1.clone(), s.clone()));
        let r1 = Self::run(&p1)?;
        debug_assert!(r1.is_terminated());
        let p2 = AbstractConfig::from((e2.clone(), r1.state().clone()));
        Self::run(&p2)
    }
    /// Execute an if then else statement.
    fn run_ite(g: &BExpr<V>, e1: &SExpr<V>, e2: &SExpr<V>, s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let exp_span = debug_span!(
            "ITE",
            exp = format!("{}", &ite(g.clone(), e1.clone(), e2.clone())).as_str(),
            state = format!("{}", &s).as_str()
        );
        let _enter = exp_span.enter();
        let r1 = {
            let true_branch_span = debug_span!(
                "T FLT",
                guard = format!("{}", &*g).as_str(),
                state = format!("{}", &s).as_str()
            );
            let f1 = {
                let _enter = true_branch_span.enter();
                let s1 = Self::b_filter(&*g, s)?;
                debug!("T FLT => {}", &s1);
                s1
            };
            {
                let true_branch_exec_span = debug_span!(
                    "T EXC",
                    exp = format!("{}", &*e1).as_str(),
                    state = format!("{}", &f1).as_str()
                );
                let _enter = true_branch_exec_span.enter();
                if !f1.is_bottom() {
                    let p1 = AbstractConfig::from((e1.clone(), f1));
                    let r1 = Self::run(&p1)?;
                    debug_assert!(r1.is_terminated());
                    r1.state().clone()
                } else {
                    f1
                }
            }
        };
        let r2 = {
            let false_branch_filter_span = debug_span!(
                "F FLT",
                guard = format!("{}", not(g.clone())).as_str(),
                state = format!("{}", &s).as_str()
            );
            let f2 = {
                let _enter = false_branch_filter_span.enter();
                let s2 = Self::b_filter(&not(g.clone()), s)?;
                debug!("F FLT => {}", &s2);
                s2
            };
            {
                let false_branch_exec_span = debug_span!(
                    "F EXC",
                    exp = format!("{}", &*e2).as_str(),
                    state = format!("{}", &f2).as_str()
                );
                let _enter = false_branch_exec_span.enter();
                if !f2.is_bottom() {
                    let p2 = AbstractConfig::from((e2.clone(), f2));
                    let r2 = Self::run(&p2)?;
                    debug_assert!(r2.is_terminated());
                    r2.state().clone()
                } else {
                    f2
                }
            }
        };

        {
            let union_span = debug_span!(
                "UNION",
                rt = format!("{}", &r1).as_str(),
                rf = format!("{}", &r2).as_str(),
            );
            let _enter = union_span.enter();
            let ite_res = AbstractConfig::from(r1.unite(&r2));
            debug!("UNION => {}", &ite_res);
            Ok(ite_res)
        }
    }
    /// Execute a while do statement.
    fn run_wd(g: &BExpr<V>, e: &SExpr<V>, s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        let exp_span = debug_span!(
            "WD",
            exp = format!("{}", &e).as_str(),
            state = format!("{}", &s).as_str()
        );
        let _enter = exp_span.enter();

        // FP on guard boolean filter and body execution with widening
        let fp_state = fp(s.clone(), &|s| {
            let flt_state = {
                let guard_flt = debug_span!(
                    "G FLT",
                    guard = format!("{}", g).as_str(),
                    state = format!("{}", s).as_str());
                let _enter = guard_flt.enter();

                let flt_state = Self::b_filter(g, s)?;

                debug!("G FLT => {}", flt_state);
                flt_state
            };

            {
                let bot_check = debug_span!(
                    "B CHK",
                    state = format!("{}", s).as_str());
                let _enter = bot_check.enter();

                if flt_state.is_bottom() {
                    debug!("B CHK => {}", s);
                    return Ok(s.clone());
                }
            }

            let new_state = {
                let body_execution = debug_span!(
                    "EXC",
                    body = format!("{}", e).as_str(),
                    state = format!("{}", flt_state).as_str());
                let _enter = body_execution.enter();

                let c = AbstractConfig::from((e.clone(), flt_state));

                let r = Self::run(&c)?;

                r.state().clone()
            };

            let wdn_state = {
                let widening = debug_span!(
                    "WDN",
                    s1 = format!("{}", s).as_str(),
                    s2 = format!("{}", new_state).as_str());
                let _enter = widening.enter();

                let wdn_state = s.widen(&new_state);

                debug!("WDN => {}", wdn_state);
                wdn_state
            };

            Ok(wdn_state)
        })?;

        let exit_state = {
            let ng = not(g.clone());

            let neg_guard_flt = debug_span!(
                "NG FLT",
                guard = format!("{}", ng).as_str(),
                state = format!("{}", fp_state).as_str());
            let _enter = neg_guard_flt.enter();

            let exit_state = Self::b_filter(&ng, &fp_state)?;

            debug!("NG FLT => {}", exit_state);
            exit_state
        };

        let not_entered_state = {
            let ng = not(g.clone());
            let guard_flt = debug_span!(
                "NG FLT",
                guard = format!("{}", ng).as_str(),
                state = format!("{}", s).as_str());
            let _enter = guard_flt.enter();

            let not_entered_state = Self::b_filter(&ng, &s.clone())?;

            debug!("NG FLT => {}", not_entered_state);
            not_entered_state
        };

        let final_state = {
            let final_widening = debug_span!(
                "F WDN",
                s1 = format!("{}", exit_state).as_str(),
                s2 = format!("{}", not_entered_state).as_str());
            let _enter = final_widening.enter();

            let final_state = exit_state.widen(&not_entered_state);

            debug!("F WDN => {}", final_state);
            final_state
        };

        Ok(AbstractConfig::from(final_state))
    }
    /// Execute a parenthesized statement.
    fn run_paren(e: &SExpr<V>, s: &State<V>) -> Result<AbstractConfig<V>, Box<dyn Error>> {
        Self::run(&AbstractConfig::from((e.clone(), s.clone())))
    }
}