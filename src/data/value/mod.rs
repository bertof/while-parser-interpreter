//! Value definition module

use std::{fmt, ops};

/// Trait for a value definition
///
/// Allows to generalize over integers, floats, ecc.
pub trait Value: Clone + fmt::Debug + fmt::Display + PartialOrd + PartialEq +
ops::Add<Output=Self> + ops::Sub<Output=Self> +
ops::Mul<Output=Self> + ops::Div<Output=Self> +
ops::Neg<Output=Self> {
    /// Random value inside the range
    ///
    /// Generate a random value from a range. It's mandatory to check that min < max, otherwise panics.
    fn rnd_range(min: Self, max: Self) -> Self;
    /// Application of a function to a value
    fn apply<W>(&self, f: &dyn Fn(&Self) -> W) -> W where W: Value {
        f(&self)
    }
}

/// Trait for an abstract value definition
///
/// An `AbstractValue` is a `Value` with the additional functionality of  having least upper bound,
/// greatest upper bound and widening operators.
pub trait AbstractValue: Value {
    /// Returns the representation of top of the abstract domain
    fn top() -> Self;
    /// Returns the representation of bottom of the abstract domain
    fn bottom() -> Self;
    /// Returns true if the value is equal to bottom, false otherwise
    fn is_top(&self) -> bool {
        *self == Self::top()
    }
    /// Returns true if the vaule is equal to top, false otherwise
    fn is_bottom(&self) -> bool {
        *self == Self::bottom()
    }
    /// Least upper bound operator
    fn lub(s1: &Option<Self>, s2: &Option<Self>) -> Self;
    /// Greatest lower bound operator
    fn glb(s1: &Option<Self>, s2: &Option<Self>) -> Self;
    /// Widening operator
    fn widen(s1: &Option<Self>, s2: &Option<Self>) -> Self;
    /// Update state operator
    #[allow(unused_variables)]
    fn update_state(old_val: &Option<Self>, new_val: &Self) -> Self { new_val.clone() }
}