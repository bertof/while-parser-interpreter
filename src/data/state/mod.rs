//! Execution state data definitions

use std::collections::HashMap;
use std::fmt;

use itertools::Itertools;

use crate::data::value::{AbstractValue, Value};

/// Data structure used to represent a state of an execution.
///
/// It's supposed to be used as an immutable object. Contains an `HashMap<String, i64>` that stores
/// the states of each variable of the state.
///
/// It can either be defined, when its contents are valid, or undefined, when the state represented
/// has reached bottom in an abstract interpretation.
#[derive(Clone, Eq, PartialEq)]
pub enum State<V> {
    Defined(HashMap<String, V>),
    Bottom(HashMap<String, V>),
}

impl<V> State<V> {
    /// Creates a new empty State.
    pub fn new() -> Self {
        State::Defined(HashMap::new())
    }

    /// Returns a vector containing the names of the variables saved in the state.
    pub fn keys(&self) -> Vec<String> {
        self.unsafe_get_map().keys().cloned().collect()
    }

    /// Returns a borrow reference to the map of variable-value it represent if the state is well
    /// defined, otherwise it panics.
    fn get_map(&self) -> &HashMap<String, V> {
        match self {
            State::Defined(map) => map,
            State::Bottom(_) => panic!("Trying to interact with a state that reached bottom"),
        }
    }
    /// Returns a borrow reference to the map of variable-value it represent independently on whether
    /// the state has reached bottom or not.
    fn unsafe_get_map(&self) -> &HashMap<String, V> {
        match self {
            State::Defined(map) => map,
            State::Bottom(map) => map,
        }
    }

    /// Returns true if the state represented has reached a bottom state, false otherwise.
    pub fn is_bottom(&self) -> bool {
        match self {
            State::Defined(_) => false,
            State::Bottom(_) => true,
        }
    }
}

impl<V> State<V> where V: Clone {
    pub fn get(&self, var: &str) -> Option<&V> {
        self.unsafe_get_map().get(var)
    }

    pub fn set(&self, var: &str, val: V) -> Self {
        let mut new_map: HashMap<String, V> = self.get_map().clone();
        new_map.insert(var.to_string(), val);
        State::Defined(new_map)
    }

    #[allow(dead_code)]
    pub fn to_concrete<W>(&self, f: &dyn Fn(&V) -> W) -> State<W> where W: Value {
        let values = self.get_map().iter()
            .map(|(k, v)| (k.clone(), f(v)))
            .collect_vec();
        State::from_value_vec(values)
    }

    pub fn to_abstract<W>(&self, f: &dyn Fn(&V) -> W) -> State<W> where W: AbstractValue {
        let values = self.unsafe_get_map().iter()
            .map(|(k, v)| (k.clone(), f(v)))
            .collect_vec();
        State::from_abstract_vec(values)
    }
}

impl<V> State<V> where V: Value {
    pub fn from_value_vec(entries: Vec<(String, V)>) -> Self {
        State::Defined(entries.iter().cloned().collect())
    }
}

impl<V> State<V> where V: AbstractValue {
    pub fn to_bottom(&self) -> Self {
        match self {
            State::Defined(map) => State::Bottom(map.clone()),
            State::Bottom(map) => State::Bottom(map.clone()),
        }
    }

    pub fn from_abstract_vec(entries: Vec<(String, V)>) -> Self {
        if entries.iter().any(|(_k, v)| v.is_bottom()) {
            State::Bottom(entries.iter().cloned().collect())
        } else {
            State::Defined(entries.iter().cloned().collect())
        }
    }

    pub fn merge(&self, other: &Self, f: &dyn Fn(&Option<V>, &Option<V>) -> V) -> Self {
        let entries = self.keys()
            .iter()
            .chain(other.keys().iter())
            .unique()
            .map(|k| (
                k.clone(),
                f(&self.get(k).cloned(),
                  &other.get(k).cloned())
            ))
            .collect_vec();
        Self::from_value_vec(entries)
    }

    pub fn unite(&self, other: &Self) -> Self {
        let r = self.merge(other, &V::lub);
        if self.is_bottom() && other.is_bottom() {
            r.to_bottom()
        } else if self.is_bottom() {
            other.clone()
        } else if other.is_bottom() {
            self.clone()
        } else {
            r
        }
    }

    pub fn intersect(&self, other: &Self) -> Self {
        let r = self.merge(other, &V::glb);
        if self.is_bottom() || other.is_bottom() {
            r.to_bottom()
        } else {
            r
        }
    }

    pub fn widen(&self, other: &Self) -> Self {
        let r = self.merge(other, &V::widen);
        if self.is_bottom() && other.is_bottom() {
            r.to_bottom()
        } else if self.is_bottom() {
            other.clone()
        } else if other.is_bottom() {
            self.clone()
        } else {
            r
        }
    }
}

impl<V> fmt::Display for State<V> where V: fmt::Display + Clone {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let res = self.keys()
            .iter()
            .filter_map(|k| self.get(k).map(|v| (k, v)))
            .map(|(k, v)| format!("{} -> {}", k, v))
            .join(", ");
        if self.is_bottom() {
            write!(f, "⊥[{}]", res)
        } else {
            write!(f, "[{}]", res)
        }
    }
}

impl<V> fmt::Debug for State<V> where V: fmt::Debug + Clone {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let res = self.keys()
            .iter()
            .filter_map(|k| self.get(k).map(|v| (k, v)))
            .map(|(k, v)| format!("{:?} -> {:?}", k, v))
            .join(", ");
        if self.is_bottom() {
            write!(f, "⊥[{}]", res)
        } else {
            write!(f, "[{}]", res)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::utilities::logging::*;

    use super::*;

    #[test]
    fn test_state() {
        init_log_test();
        let s = State::new().set("x", 1);
        assert_eq!(s.get("x"), Some(&1));
        assert_eq!(s.get("y"), None);
    }
}