//! Data definitions module

pub mod config;
pub mod state;
pub mod value;