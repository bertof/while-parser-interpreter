//! Program configuration module

use std::fmt;

use crate::data::state::State;
use crate::data::value::{AbstractValue, Value};
use crate::domains::expressions::stmt::semantic::SExpr;

/// Configuration of a program.
///
/// Represents the possible inputs and outputs of the transition system:
///
/// - Program is the tuple (stmt expression, state)
/// - Ts is the terminal state, containing state
#[derive(Clone, Eq, PartialEq)]
pub enum Config<V> where V: Value {
    Program(SExpr<V>, State<V>),
    Ts(State<V>),
}

impl<V> Config<V> where V: Value {
    /// Returns true if the configuration is in a final state, false otherwise.
    pub fn is_terminated(&self) -> bool {
        match &self {
            Config::Program(_, _) => false,
            Config::Ts(_) => true
        }
    }

    /// Optionally returns the expression of the configuration, if it is present.
    pub fn expression(&self) -> Option<&SExpr<V>> {
        match &self {
            Config::Program(e, _) => Some(e),
            Config::Ts(_) => None,
        }
    }

    /// Returns the state of the configuration
    pub fn state(&self) -> &State<V> {
        match &self {
            Config::Program(_, s) => &s,
            Config::Ts(s) => &s,
        }
    }
}

impl<V> From<State<V>> for Config<V> where V: Value {
    fn from(state: State<V>) -> Self {
        Config::Ts(state)
    }
}

impl<V> From<(SExpr<V>, State<V>)> for Config<V> where V: Value {
    fn from((exp, state): (SExpr<V>, State<V>)) -> Self {
        Config::Program(exp, state)
    }
}

impl<V> fmt::Display for Config<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Config::Program(ref e, ref s) => write!(f, "<{}, {}>", e, s),
            Config::Ts(ref s) => write!(f, "{}", s),
        }
    }
}

impl<V> fmt::Debug for Config<V> where V: Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        (self as &dyn fmt::Display).fmt(f)
    }
}

/// Configuration of an abstract program.
///
/// Represents the possible inputs and outputs of the transition system:
///
/// - Program is the tuple (stmt expression, abstract state)
/// - Ts is the terminal state, containing state
#[derive(Clone, Eq, PartialEq)]
pub enum AbstractConfig<V> where V: AbstractValue {
    Program(SExpr<V>, State<V>),
    Ts(State<V>),
}


impl<V> AbstractConfig<V> where V: AbstractValue {
    /// Returns true if the configuration is in a final state, false otherwise.
    pub fn is_terminated(&self) -> bool {
        match &self {
            AbstractConfig::Program(_, _) => false,
            AbstractConfig::Ts(_) => true
        }
    }

    /// Optionally returns the expression of the configuration, if it is present.
    pub fn expression(&self) -> Option<&SExpr<V>> {
        match &self {
            AbstractConfig::Program(e, _) => Some(e),
            AbstractConfig::Ts(_) => None,
        }
    }

    /// Returns the state of the configuration
    pub fn state(&self) -> &State<V> {
        match &self {
            AbstractConfig::Program(_, s) => &s,
            AbstractConfig::Ts(s) => &s,
        }
    }
}

impl<V> From<State<V>> for AbstractConfig<V> where V: AbstractValue {
    fn from(state: State<V>) -> Self {
        AbstractConfig::Ts(state)
    }
}

impl<V> From<(SExpr<V>, State<V>)> for AbstractConfig<V> where V: AbstractValue {
    fn from((exp, state): (SExpr<V>, State<V>)) -> Self {
        AbstractConfig::Program(exp, state)
    }
}

impl<V> fmt::Display for AbstractConfig<V> where V: AbstractValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            AbstractConfig::Program(ref e, ref s) => write!(f, "<{}, {}>", e, s),
            AbstractConfig::Ts(ref s) => write!(f, "{}", s),
        }
    }
}

impl<V> fmt::Debug for AbstractConfig<V> where V: AbstractValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        (self as &dyn fmt::Display).fmt(f)
    }
}
