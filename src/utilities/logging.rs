//! Loggin module
//!
//! Contains all the necessary modules and helper functions to manage the logging of both the
//! application and the tests.

pub use tracing::{
    debug, debug_span,
    error, error_span,
    info, info_span,
    trace, trace_span,
    warn, warn_span,
};
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use crate::utilities::slogging_subscriber::SloggishSubscriber;

/// Initialize the default logger
pub fn init_log() {
    let subscriber = SloggishSubscriber::new(2);
    tracing::subscriber::set_global_default(subscriber).unwrap_or(());
}

/// Initialize the default logger for tests
#[allow(dead_code)]
pub fn init_log_test() {
    let subscriber = SloggishSubscriber::new(2);
    tracing::subscriber::set_global_default(subscriber).unwrap_or(());
}

/// Initialize the tracing logger
#[allow(dead_code)]
pub fn init_log_tracing() {
    FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .with_target(false)
        .without_time()
        .compact()
        .try_init().unwrap_or(())
}

/// Initialize the tracing logger for tests
#[allow(dead_code)]
pub fn init_log_test_tracing() {
    FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .with_target(false)
        .without_time()
        .try_init().unwrap_or(());
}