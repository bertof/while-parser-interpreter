//! Table generation module
//!
//! Contains helper functions to generate tables for single or binary operators.
use std::fmt;

use itertools::Itertools;
use prettytable::{Row, Table};

/// Single element function table generator
///
/// Given a list of values, it applies a function `operator` to each one of them and generates a
/// table with the output.
/// `operator_string` is a string representation of the operator, used to format the table.
#[allow(dead_code)]
pub fn generate_f_table<V, D>(
    values: &[V], operator: &dyn Fn(&V) -> D, operator_string: &str) -> Table
    where V: fmt::Display, D: fmt::Display {
    let mut table = Table::new();

    table.set_titles(Row::from(
        vec!["".to_string(), operator_string.to_string()]));

    for a in values {
        table.add_row(Row::from(
            vec![format!("{}", a), format!("{}", operator(a))]));
    }

    table
}

/// Single element function table generator
///
/// Given a list of values, it applies a function `operator` to each binary combination of them and
/// generates a table with the output.
/// `operator_string` is a string representation of the operator, used to format the table.
#[allow(dead_code)]
pub fn generate_ab_table<V, D>(
    values: &[V], operator: &dyn Fn(&V, &V) -> D, operator_string: &str) -> Table
    where V: fmt::Display, D: fmt::Display {
    let mut table = Table::new();

    let mut r = vec![operator_string.to_string()];
    r.extend(values.iter().map(|c| format!("{}", c)).collect_vec());
    table.set_titles(Row::from(r));

    for a in values {
        let mut r = vec![format!("{}", a)];
        r.extend(values.iter().map(|b| format!("{}", operator(a, b))).collect_vec());
        table.add_row(Row::from(r));
    }

    table
}