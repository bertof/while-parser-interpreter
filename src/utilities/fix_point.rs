//! Fix point module
//!
//! Contains a generic implementation of the fix point operation.

use std::error::Error;

/// Generic fix point operation
///
/// Given an initial value `init` it applies the function `step` to it until the result of the
/// function is equal to the value in input.
pub fn fp<T>(init: T, step: &dyn Fn(&T) -> Result<T, Box<dyn Error>>) -> Result<T, Box<dyn Error>>
    where T: PartialEq {
    let mut pred = init;
    loop {
        let next = step(&pred)?;
        if pred == next {
            return Ok(next);
        } else {
            pred = next
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::utilities::logging::*;

    use super::*;

    #[test]
    /// Test fix point function
    fn test_fix_point() -> Result<(), Box<dyn Error>> {
        init_log_test();
        assert_eq!(fp(0, &|&v| if v < 5 { Ok(v + 1) } else { Ok(v) })?, 5);
        Ok(())
    }

    #[test]
    /// Test for while true do skip
    fn test_potentially_infinite_loop() -> Result<(), Box<dyn Error>> {
        init_log_test();
        assert!(fp(true, &|&v| Ok(v))?);
        Ok(())
    }

    #[test]
    #[ignore]
    /// Test fix point function non terminating case
    fn test_fix_point_infinite() -> Result<(), Box<dyn Error>> {
        init_log_test();
        assert!(fp(true, &|&v| Ok(!v))?);
        Err(String::from("Should never return").into())
    }
}
