# While parser and interpreter written in Rust
[![pipeline status](https://gitlab.com/bertof/while-parser-interpreter/badges/master/pipeline.svg)](https://gitlab.com/bertof/while-parser-interpreter/commits/master)
[![coverage report](https://gitlab.com/bertof/while-parser-interpreter/badges/master/coverage.svg)](https://gitlab.com/bertof/while-parser-interpreter/commits/master)

A parser and an interpreter written in Rust for the toy language While, also known as Stm (statement).
The crate also includes a CLI tool that allows to execute while programs using the interpreter.

## Parser

The parser is written using the library [Nom](https://docs.rs/nom/~4/nom). It's modular so you can parse both complete programs and single arithmetic or boolean expressions. The language is easy to extend, for example adding statements like other arithmetic or boolean operators, parallelism and other data types, so the parser has been designed to be easily extended and composed.

## Interpreter

This project implements both a concrete interpreter and an abstract interpreter.
The concrete interpreter can use both the denotational semantic and the operational (small step) approach to evaluate the statements while arithmetic and boolean expressions are evaluated in a big step.
The abstract interpreter operates over an abstract interpretation of the concrete semantics.
The interpreter uses a pluggable default trait what can be adapted on the specific abstraction.
Three abstractions are implemented: constants, signs and intervals.

## CLI
The compiled executable `while-interpreter` includes a CLI tool to execute While programs dynamically.
The syntax to use is `while-interpreter interpret <INPUT>` to run the interpreter for the source code file given as `<INPUT>` and `while-interpreter abstract -a <abstraction> <INPUT>` to use an abstract interpretation.
The valid options for the `abstraction` flag are `signs`, `constants` and `intervals`.
```
while-interpreter 2.0.0
Filippo Berto <berto.f@protonmail.com>
Interpreter and static analyzer for While (Stm) language
USAGE:
    while-interpreter [SUBCOMMAND]
FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
SUBCOMMANDS:
    abstract     Use an abstract interpretation to run the source code
    help         Prints this message or the help of the given subcommand(s)
    interpret    Interpret the source code
```

## Generated documentation

You can find the crate documentation at [bertof.gitlab.io/while-parser-interpreter/while_interpreter](https://bertof.gitlab.io/while-parser-interpreter/while_interpreter). The docs are automatically generated on each new release of the repository using `cargo doc`.