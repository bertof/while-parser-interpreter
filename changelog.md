# Changelog

### v1.3.2

- Cleanup of some repetitions when handling results
- Update changelog
- Fix some typos
- Better test readability

### v1.3.1

Release 1.3

- Add syntactic sugar for repeat until and for statements
- Fix output of standard execution, now the string ends with the proper termination character
- Better format of tests output
- Add more example programs in While

### v1.2.1

- Add [pages support on Gitlab](https://bertof.gitlab.io/while-parser-interpreter/while_interpreter/index.html)
- CLI improvements
- Completed CI automation

## v1.2.0

Release 1.2

- Updated interpreter separating data from logic
- Enabled all tests
- Updated CLI to the new interpreter
- Readme and documentation updates

## v1.1.2

Add inline documentation

## v1.1.1

Fix issue with GitLab CI configuration

## v1.1.0

- Fix issues with spaces in the parser
- Integration with the cli
- Add more tests

## v1.0.0

First release!

- completed syntactic categories
- completed parser
- WIP: cli integration
